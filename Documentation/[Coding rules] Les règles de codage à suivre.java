*****************************************************************************
								Coding rules
*****************************************************************************
dimanche 15 juin 2014
dicotout




Bonjour, voici une petite liste de r�gle de codage � suivre religieusement lorsque vous codez pour le projet Cronos.

Sommaire :
I] R�gle d'or et de diamant ABSOLUE : Commentez
II] R�gle d'or n�2 : In ingliche plize
III] R�gle d'or n�3 : Nom des attributs
IV] R�gle d'or n�4 : Mise en page
V] R�gle d'or n�5 : Pas d'op�rateurs ternaires






I] R�gle d'or et de diamant ABSOLUE : Co-mmen-tez


Co-mmen-tez, Co-mmen-tez, Co-mmen-tez, Co-mmen-tez, Co-mmen-tez, Co-mmen-tez, Co-mmen-tez, Co-mmen-tez, Co-mmen-tez.
Et commentez en fran�ais.

Bien :
/**
 * Gestionnaire du graph de sc�ne.
 * Arborescence contenant tout les �l�ments d'une partie.
 */
public class SceneGraphManager {

PAS bien :
-rien-
public class SceneGraphManager {


Bien (pour un attribut, le commentaire est sur une seul ligne) :
/** Gestionnaire de l'interface. */
private UIManager mUIManager;

PAS bien :
- rien -
private UIManager mUIManager;

PAS bien (car cette pr�sentation sur trois ligne est r�serv�e aux m�thodes et aux classes) :
/**
 * Gestionnaire de l'interface.
 */
private UIManager mUIManager;


Bien :
/**
 * Permet de lib�rer la m�moire lorsque l'application est d�truite.
 */
public void dispose() {
	...
}

PAS bien : 
- rien -
public void dispose() {
	...
}


Bien :
/**
 * Boucle de rendu.
 */
public void render() {
	// Mets � jour tout les objets qu'il contient :
	gStage.act();
	// Dessine � l'�cran tout les objets qu'il contient :
	gStage.draw();
}

PAS bien :
// Boucle de rendu.
public void render() {
	/**
	 * Mets � jour tout les objets qu'il contient :
	 */
	gStage.act();
	/**
	 * Dessine � l'�cran tout les objets qu'il contient :
	 */
	gStage.draw();
}





II] R�gle d'or n�2 : In inglich plizzz


Codez en anglais.
C'est � dire le nom des attributs, des m�thodes, des classes, des packages ... doit �tre en anglais.
La seule chose qui est en fran�ais dans le code, ce sont les commentaires.

Bien :
/**
 * Gestion du redimensionnement de la fen�tre.
 */
public void resize(int pWidth, int pHeight) {
	mCameraManager.resize(pWidth, pHeight);
	mSceneGraphManager.resize(pWidth, pHeight);
}

PAS bien :
/**
 * Set the window configuration.
 */
public void redimensionnement(int largeur, int hauteur) {
	gestionnaireDeCamera.redimensionnement(largeur, hauteur);
	gestionnaireDeGraphe.redimensionnement(largeur, hauteur);
}





III] R�gle d'or n�3 : Nom des attributs


On utilise des pr�fixes pour les attributs :

lWidth est une variable locale � sa m�thode
mWidth est une variable membre de sa classe
pWidth est un param�tre
gWidth est une variable globale (public static en java) au projet

Bien :
/**
 * Gestion du redimensionnement de la fen�tre.
 */
public void resize(int pWidth, int pHeight) {
	mWidth = pWidth;
	mHeight = pHeight;
}

PAS bien : 
/**
 * Gestion du redimensionnement de la fen�tre.
 */
public void resize(int width, int height) {
	this.width = width;
	this.height = height;
}


Exception, ne faites pas :
for (int lJ = 0; lJ < toto; lJ++)

Faites :
for (int i = 0; i < toto; i++)





IV] R�gle d'or n�4 : Mise en page


Bien :
/**
 * Gestion du redimensionnement de la fen�tre.
 */
public void resize(int pWidth, int pHeight) {
	mCameraManager.resize(pWidth, pHeight);
	mSceneGraphManager.resize(pWidth, pHeight);
}

PAS bien :
/**
 * Gestion du redimensionnement de la fen�tre.
 */
public void resize(int pWidth,int pHeight) 
{
	mCameraManager.resize(pWidth,pHeight); mSceneGraphManager.resize(pWidth,pHeight);
}


Bien :
if (a == b) {
  toto
} else {
  tito
}

PAS bien :
if (a == b) {
  toto
} 
else {
  tito
}

PAS bien :
if (a == b) 
{
  toto
} 
else 
{
  tito
}

VRAIMENT PAS bien :
if 
	(a == b) 
{
  toto
} 
else 
{
  tito
}





V] R�gle d'or n�5 : Pas d'op�rateurs ternaires


Bien
if (a == b) {
  toto
} else {
  tito
}

PAS bien :
(a == b) ? toto : titi;







