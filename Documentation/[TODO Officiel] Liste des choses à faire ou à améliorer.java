﻿*****************************************************************************
			Choses à faire ou à améliorer
*****************************************************************************
Le 23 octobre 2014
dicotout


Bonjour à tous et tà toutes, ce fichier liste les choses à faire ou à améliorer pour le projet Le Maître des Titans : Cronos. 
Il s'agit ici de notes et de remarques concernant le code.
Il s'adresse aux développeur du projet.
Vous pouvez le compléter et le modifier à votre guise.


/**
 * Réutiliser les chemins calculés pour les personnages.
 * Dans AddCharacEvery, dans la méthode public boolean act(float delta).
 * 23 octobre 2014 - dicotout
 */
AddCharacEvery permet de créer à un point de départ un personnage et de l'envoyer vers un point d'arrivé, et ce, tous les x secondes.
Pour l'envoyer au point d'arrivé, le plus court chemin est calculé A CHAQUE FOIS. Donc pour tous les personnages créés, on lance le couteux calcul du plus court chemin.
Or, dans 99% des cas, le chemin est le même pour tous les personnages.
Il serait donc judicieux de stoquer le chemin calculé pour le premier perso, et, dans la mesure ou les tuiles de sol du chemin ne changent pas, réutiliser ce même chemin pour tous les perso qui suivront.


/**
 * Ne trier que les CityActors qui ont bougé.
 * Dans SceneGraphManager.java, dans la méthode public void render().
 * 23 octobre 2014 - dicotout
 */
Le role la méthode render() de SceneGraphManager est de trier les CityActors celon leur mDepth. Cela permet, au rendu, de positionner derrière les CityActors qui doivent etre derrière, et devant ceux qui doivent etre devant. Ce tri est effectué à chaque itération de rendu, soit toutes les 0.04s, ce qui est donc très couteux.
Pour gagner en perfs j'ai developpé ceci : on ne tri que les CityActors présent dans la zone vue par la caméra. Ceci est fais par la méthode updateActorsInView().
Mais on pourrait encore faire mieux : Il faudrait ne trier que les CityActors qui ont bougés depuis la dernière itération.
Concretement pendant une partie, les tuiles de sols et des batiments ne bouge quasi jamais. Cela permettrais d'économiser le recalcul inutil de leur mDepth.
Ainsi au final, la plus part du temps, on ne trirais que les personnages et les effets. Gain estimé : 50% au moins !