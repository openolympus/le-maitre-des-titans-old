\select@language {french}
\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {section}{\numberline {2}Premi\`ere conception du jeu}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Informations g\'en\'erales}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Choix techniques}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Biblioth\`eques utilis\'ees}{5}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}LibGDX et LWJGL}{5}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}TWL}{6}{subsubsection.2.3.2}
\contentsline {subsubsection}{\numberline {2.3.3}Biblioth\`eques tierces}{7}{subsubsection.2.3.3}
\contentsline {subsubsection}{\numberline {2.3.4}Cr\'eation d'un installateur}{7}{subsubsection.2.3.4}
\contentsline {subsection}{\numberline {2.4}Sc\'enarii}{7}{subsection.2.4}
\contentsline {subsubsection}{\numberline {2.4.1}Re-make du jeu}{7}{subsubsection.2.4.1}
\contentsline {subsubsection}{\numberline {2.4.2}Ajouts sp\'ecifiques}{7}{subsubsection.2.4.2}
\contentsline {paragraph}{\numberline {2.4.2.1}Modifications g\'en\'erales}{7}{paragraph.2.4.2.1}
\contentsline {paragraph}{\numberline {2.4.2.2}Modifications sur le gameplay}{9}{paragraph.2.4.2.2}
\contentsline {paragraph}{\numberline {2.4.2.3}Modifications sur la gestion des ressources}{10}{paragraph.2.4.2.3}
\contentsline {paragraph}{\numberline {2.4.2.4}Modifications sur l'aspect Mythologie}{11}{paragraph.2.4.2.4}
\contentsline {paragraph}{\numberline {2.4.2.5}Modifications sur l'aspect Arm\'ee}{13}{paragraph.2.4.2.5}
\contentsline {paragraph}{\numberline {2.4.2.6}Modifications sur l'aspect Monde - hors commerce}{14}{paragraph.2.4.2.6}
\contentsline {paragraph}{\numberline {2.4.2.7}Modifications sur l'aspect Commerce}{15}{paragraph.2.4.2.7}
\contentsline {paragraph}{\numberline {2.4.2.8}Modifications sur les aspects Administratif, Culture, Hygi\`ene et Esth\'etique}{16}{paragraph.2.4.2.8}
\contentsline {paragraph}{\numberline {2.4.2.9}Modifications sur l'\'editeur de sc\'enario}{17}{paragraph.2.4.2.9}
\contentsline {section}{\numberline {3}Analyse des diff\'erents modules}{19}{section.3}
\contentsline {subsection}{\numberline {3.1}Documentation des modules actuels}{19}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Pr\'esentation des principaux paquetages}{19}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Paquetage fenetre}{19}{subsubsection.3.1.2}
\contentsline {paragraph}{\numberline {3.1.2.1}Organisation}{19}{paragraph.3.1.2.1}
\contentsline {paragraph}{\numberline {3.1.2.2}Les actions de boutons}{20}{paragraph.3.1.2.2}
\contentsline {subparagraph}{\numberline {3.1.2.2.1}Actions courrantes}{20}{subparagraph.3.1.2.2.1}
\contentsline {subparagraph}{\numberline {3.1.2.2.2}Action personnalis\'ee}{20}{subparagraph.3.1.2.2.2}
\contentsline {paragraph}{\numberline {3.1.2.3}L'utilisation de twl}{20}{paragraph.3.1.2.3}
\contentsline {subsubsection}{\numberline {3.1.3}Paquetage jeu}{20}{subsubsection.3.1.3}
\contentsline {paragraph}{\numberline {3.1.3.1}Organisation}{20}{paragraph.3.1.3.1}
\contentsline {paragraph}{\numberline {3.1.3.2}Gestion de la cit\'e}{20}{paragraph.3.1.3.2}
\contentsline {subparagraph}{\numberline {3.1.3.2.1}Les batiments}{20}{subparagraph.3.1.3.2.1}
\contentsline {subparagraph}{\numberline {3.1.3.2.2}Les personnages}{20}{subparagraph.3.1.3.2.2}
\contentsline {paragraph}{\numberline {3.1.3.3}Gestion du monde}{20}{paragraph.3.1.3.3}
\contentsline {subsection}{\numberline {3.2}R\'eflexions sur les prochains modules}{20}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Organisation du d\'eveloppement}{21}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Pr\'esentation de l'\'equipe}{21}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}R\'epartition des t\^aches}{21}{subsubsection.3.3.2}
\contentsline {section}{\numberline {4}Conclusion}{22}{section.4}
