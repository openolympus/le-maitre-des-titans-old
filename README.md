Le projet « Le Maître des Titans : Cronos » est fait pour passer du rêve à la réalité. Nous refaçonnons le jeu Le Maitre de l'Olympe : Zeus et son extension Le Maitre de l'Atlantide : Poséidon pour vous proposer une extraordinaire et toute nouvelle expérience de jeu.

Ce projet est communautaire, vous pouvez aider en participant au forum, en apportant vos idées, et en rejoignant notre équipe (faites vous connaitre sur le forum !) : http://www.lemaitredelolympe.com/phpBB3/viewtopic.php?f=10&t=1308 

Site web officiel : http://www.lemaitredelolympe.com/ 

Blog officiel : http://www.lemaitredelolympe.com/projetcronos.php 

Forum : http://www.lemaitredelolympe.com/phpBB3/index.php 

Que Zeus vous protège, et à bientôt

Le Maitre des Titans Web Site 