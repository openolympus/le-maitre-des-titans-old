package Application;

import Screens.ScreenLoading;
import Sounds.SoundsManager;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

/**
 * Classe principale de l'application. Elle est chargée en premier et charge tout le reste.
 * Elle a, entre autre, le role de charger le premier screen du jeu.
 */

public class MainApplication extends Game {
	
	/** Screen de chargement. */
	private ScreenLoading mScreeLoading; 
	
	/** Gestionnaire de sons. */
	private SoundsManager mSoundsManager;
	
	/**
	 * Constructeur.
	 */
	public MainApplication() {

	}
	
	/**
	 * Semblable au contructeur. 
	 * Est appelé une fois à la création de la classe, après le "vrai" constructeur.
	 */
	public void create() {	
		// Chargement de la ScreenCity :
		mScreeLoading = new ScreenLoading(this);
		setScreen(mScreeLoading);
		
		// Gestionnaire de sons :
		mSoundsManager = new SoundsManager();
	}

	/**
	 * Boucle de rendu.
	 * Affiche les éléments à l'écran.
	 */
	public void render() {
		super.render(); 
		
		// Touche "echap" pour quitter l'application :
		if (Gdx.input.isKeyPressed(Keys.ESCAPE)) {
			Gdx.app.exit();
		}
		
		// Gestion des sons et musiques :
		mSoundsManager.render();
	}

	/**
	 * Gestion du redimensionnement de la fenêtre.
	 */
	public void resize(int pWidth, int pHeight) {
		super.resize(pWidth, pHeight);
	}

	/**
	 * Est appellée lorsque l'application est mise en pause.
	 */
	public void pause() {
		super.pause();
	}

	/**
	 * Est appellée lorsque l'application reprend (après avoir été mise en pause).
	 */
	public void resume() {
		super.resume();
	}
	
	/**
	 * Destructeur.
	 */
	public void dispose() {
		super.dispose();
		mScreeLoading.dispose();
		mSoundsManager.dispose();
	}
}
