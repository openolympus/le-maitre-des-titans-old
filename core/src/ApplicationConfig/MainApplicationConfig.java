package ApplicationConfig;

import java.util.Locale;

/**
 * Classe contenant tout les paramètres de l'applications, accessibles en public static.
 */

public class MainApplicationConfig {
	
	/** La largeur de l'écran. */
//	public static int gScreenWidth = (int) java.awt.Toolkit.getDefaultToolkit().getScreenSize().getWidth();
	public static int gScreenWidth = 1300;
	
	/** La hauteur de l'écran. */
//	public static int gScreenHeight  = (int) java.awt.Toolkit.getDefaultToolkit().getScreenSize().getHeight(); 
	public static int gScreenHeight  = 768; 
	
	/** Si l'on est ou non en plein écran. */
	public static boolean gIsFullScreen = false;
//	public static boolean gIsFullScreen = true;
	
	/** La langue utilisée. */
	public static String gLanguage = Locale.getDefault().getLanguage(); 
	
	/** Le nom de l'application. */
	public static String gApplicationName = "Le Maitre des Titans : Cronos - version 0.18";
	
	/** Le chemin du favicon de l'application. */
	public static String gFaviconPath = "favicon/icone32x32.png"; 
	
	/** Si l'on utilise la synchronisation verticale. */
	public static boolean gIsUsedVSync = true;
	
	/** La marge de détection de la sopuris sur le bord de l'écran, en pixels. Ne pas mettre 0. */
	public static int gBordersMarge = 0;
	
	/** La vitesse de défilement. Correspond au nombre de pixels par seconde. La vitesse par défaut est immuable. */
	public final static int gDefaultPanSpeed = 30;
	public static int gPanSpeed = gDefaultPanSpeed + 1;
	
	/** La vitesse de zoom. */
	public static float gZoomSpeed = 0.1f;
	
	/** Facteur multiplié à ce que l'on addition au temps pour chaque itération. */
	public static float gAnimationsSpeed = 1.0f;
	
	/** Facteur multiplié aux déplacements. */
	public static float gMovesSpeed = 3000.0f;
	
	/** Temps en secondes entre deux frames d'une animation.
	 *  Les animamtions ont été enregistrées en 25 images par secondes, soit 0.04s entre chaque frame. */
	public static float gAnimationStepTime = 0.04f;

	/** Niveau du volume des sons. Entre 0 et 1. */
	public static float gSoundVolume = 0.35f;
}
