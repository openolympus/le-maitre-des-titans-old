package CityActions;

import CityActors.CityActorBuilding;

import com.badlogic.gdx.scenes.scene2d.actions.TemporalAction;

/** 
 * Action qui rend visible l'animation d'un bâtiment pendant une durée donnée.
 */

public class ActivateAnimation extends TemporalAction {
	
	/** Bâtiment dont l'animation sera rendue visible. */
	private CityActorBuilding mTarget;

	/**
	 * Boucle de rendu.
	 */
	protected void update(float percent) {
		mTarget.setAnimationVisible(true);
	}
	
	/**
	 * Définis le bâtiment dont l'animation sera rendue visible.
	 */
	public void setTarget(CityActorBuilding pTarget) {
		mTarget = pTarget;
	}
	
	/**
	 * Réinitialise l'Action. Est appellée lorsque l'Action est réutilisé (dans un forever par exemple).
	 */
	protected void end() {
		mTarget.setAnimationVisible(false);
	}
}