package CityActions;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

import java.util.Iterator;
import java.util.LinkedList;

import ApplicationConfig.MainApplicationConfig;
import CityActors.CityActor;
import CityActors.CityActorBuilding;
import CityEngine.AIManager;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.utils.Pool;

/** 
 * Action qui obtient et place un personnage à un point de départ et l'envoi vers un point d'arrivé, ce, tous les x secondes.
 * Lorsque le personnage à atteint le point d'arrivé, il est libéré.
 */

public class AddCharacEvery extends Action {
	
	/** Le CityActor qui ajoute les personnages au point de départ et les envoi au point d'arrivé. */
	private CityActor mTarget;
	
	/** Un personnage sera créé tout les mDelay secondes. */
	private float mDelay;
	
	/** Type du personnage créé. */
	private Class<?> mCharacType;
	
	/** Point de départ des personnages. */
	private Vector2 mStart;
	
	/** Point d'arrivé des personnages. */
	private Vector2 mEnd;
	
	/** Bâtiment à animer lorsqu'un personnage atteint le point d'arrivé. */
	private CityActorBuilding mBuildingToBeAnimated;
	
	/** Compteur de temps. */
	private float mTime;
	
	/** Décalage pour placer correctement le personnage. */
	private Vector2 mCharacOffset;

	/**
	 * Boucle de rendu.
	 */
	public boolean act(float delta) {
		Pool<?> pool = getPool();
		setPool(null); // Ensure this action can't be returned to the pool while executing.
		try {
			// Toutes les mDelay secondes :
			if (mTime >= mDelay) { 
				mTime = 0.0f;
				
				// Trouve le chemin le plus court entre le départ et l'arrivée :
				LinkedList<Vector2> lPath = AIManager.gPathFinder.getPath(mStart, mEnd);
				
				// Si un chemin est trouvé :
				if (lPath.size() > 0) {
					// Action qui obtient un personnage et le place au point de départ :
					CreateCharac lActionCreate = Actions.action(CreateCharac.class);
					lActionCreate.setCharacType(mCharacType);
					lActionCreate.setPosition(mStart.x + mCharacOffset.x, mStart.y + mCharacOffset.y);
					lActionCreate.begin();
					
					// Liste qui stoquera toutes les Action de déplacements du chemin :
					WalkTo[] lAllWalkTo = new WalkTo[lPath.size()];
					int i = 0;
					
					// Départ pour la prochaine WalkTo Action :
					Vector2 lLocalStart = mStart;
	
					// Tant qu'il y a encore au moins un point du chemin à lire :
					Iterator<Vector2> lIterator = lPath.iterator();
					while (lIterator.hasNext()) { 
						// Arrivée pour la prochaine WalkTo Action :
						Vector2 lLocalEnd = lIterator.next();
				
						// Action qui envoie le personnage à sa destination:
						WalkTo lActionMove = Actions.action(WalkTo.class);
						lActionMove.setTarget(lActionCreate.getCharac());
						lActionMove.setDestination(lLocalEnd.x + mCharacOffset.x, lLocalEnd.y + mCharacOffset.y);
						lActionMove.setDuration(lLocalStart.dst(lLocalEnd) / (delta * MainApplicationConfig.gMovesSpeed));
						
						// On stoque cette Action :
						lAllWalkTo[i] = lActionMove;
						i++;
						
						// On met à jour le départ pour la prochaine WalkTo Action :
						lLocalStart = lLocalEnd;
					}
					
					// Sequence composée de toutes les Actions de déplacement créées précédemment : 
					SequenceAction lSequenceMove = sequence(lAllWalkTo);
					
					// Action qui active l'animation du bâtiment de destination pendant une durée donnée :
					ActivateAnimation lActionAnim = Actions.action(ActivateAnimation.class);
					lActionAnim.setTarget(mBuildingToBeAnimated);
					lActionAnim.setDuration(1.5f);
					
					// Action qui libère le personnage :
					RemoveCharac lActionRemove = Actions.action(RemoveCharac.class);
					lActionRemove.setCharac(lActionCreate.getCharac());
					lActionRemove.setDuration(0.1f); 
					
					// Affectation de l'Action au bâtiment qui ajoute les personnages :
					mTarget.addAction(sequence(lActionCreate, lSequenceMove, parallel(lActionAnim, lActionRemove)));
				}
			}
			mTime += delta;
			return false;
		} finally {
			setPool(pool);
		}
	}

	/** 
	 * Définis le CityActor qui ajoute les personnages au point de départ et les envoi au point d'arrivé. 
	 */
	public void setTarget(CityActor pTarget) {
		mTarget = pTarget;
	}
	
	/** 
	 * Un personnage sera créé tout les mDelay secondes définis ici. 
	 */
	public void setDelay(float pDelay) {
		mDelay = pDelay;
	}
	
	/** 
	 * Définis le type du personnage créé. 
	 */
	public void setCharacType(Class<?> pCharacType) {
		mCharacType = pCharacType;
	}
	
	/** 
	 * Définis le point de départ des personnages. 
	 */
	public void setStart(Vector2 pStart) {
		mStart = pStart;
	}
	
	/** 
	 * Définis le point d'arrivé des personnages. 
	 */
	public void setEnd(Vector2 pEnd) {
		mEnd = pEnd;
	}
	
	/** 
	 * Définis le bâtiment à animer lorsqu'un personnage atteint le point d'arrivé. 
	 */
	public void setBuildingToBeAnimated(CityActorBuilding pBuildingToBeAnimated) {
		mBuildingToBeAnimated = pBuildingToBeAnimated;
	}
	
	/** 
	 * Définis le décalage pour placer correctement le personnage. 
	 */
	public void setCharacOffset(Vector2 pCharacOffset) {
		mCharacOffset = pCharacOffset;
	}
	
}
