package CityActions;

import CityActors.CharacterActor;
import CityActors.CityActorCharacter;
import CityEngine.AIManager;

import com.badlogic.gdx.scenes.scene2d.Action;

/** 
 * Action qui obtient un personnage (le prend de sa réserve et le met en "en train d'être utilisé") 
 * et le place à un emplacement.
 */

public class CreateCharac extends Action {
	
	/** Type demandé pour le personnage. */
	private Class<?> mType;
	
	/** Emplacement du personnage. */
	private float mStartX, mStartY;
	
	/** Personnage qui sera créé. */
	private CityActorCharacter mCharac;
	
	/** Vrai si le personnage a déjà été obtenu et placé. */
	private boolean mIsBegan = false;
	
	/**
	 * Semblable au constructeur.
	 * Est appellée une seule fois, à la création de l'Action.
	 */
	protected void begin() {
		if (!mIsBegan) {
			if (CharacterActor.class.isAssignableFrom(mType)) {
				mCharac = AIManager.gPoolCharacterActors.obtain();
				mCharac.setX(mStartX);
				mCharac.setY(mStartY);
				mCharac.updateDepth();
				
				// Lorsqu'ils sont libérés les personnages sont mis en invisibles, 
				// il faut donc les remettre en visible lorsque l'on les reprend (système de Pool):
				mCharac.setVisible(true); 
			}
			mIsBegan = true;
		}
	}

	/**
	 * Boucle de rendu. Si retourne vrai, l'Action est considéré comme terminée.
	 */
	public boolean act(float delta) {
		return true;
	}
	
	/**
	 * Définis le type du personnage à créer.
	 */
	public void setCharacType(Class<?> pType) {
		mType = pType;
	}
	
	/**
	 * Définis l'emplacement où le personnage sera créé.
	 */
	public void setPosition(float pStartX, float pStartY) {
		mStartX = pStartX;
		mStartY = pStartY;
	}
	
	/**
	 * Retourne le personnage créé.
	 */
	public CityActorCharacter getCharac() {
		return mCharac;
	}
	
	/**
	 * Réinitialise l'Action. Est appellée lorsque l'Action est réutilisé (dans un forever par exemple).
	 */
	public void restart () {
		mIsBegan = false;
	}
}
