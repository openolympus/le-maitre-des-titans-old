package CityActions;

import CityActors.CharacterActor;
import CityActors.CityActorCharacter;
import CityEngine.AIManager;

import com.badlogic.gdx.scenes.scene2d.actions.TemporalAction;

/** 
 * Action qui libère le personnage (le remet en libre service dans sa réserve).
 */

public class RemoveCharac extends TemporalAction {
	
	/** Personnage à libérer. */
	private CityActorCharacter mCharac;
	
	/** Vrai si le personnage a déjà été libéré. */
	private boolean mIsBegan = false;

	/**
	 * Boucle de rendu.
	 */
	protected void update(float percent) {
		if (!mIsBegan) {
			if (CharacterActor.class.isAssignableFrom(mCharac.getClass())) {
				// Pour éviter de voir les personnage libérés, le temps qu'ils ne sont pas réutilisé :
				mCharac.setVisible(false);
				AIManager.gPoolCharacterActors.free((CharacterActor) mCharac);
			}
			mIsBegan = true;
		} 
	}
	
	/**
	 * Définis le personnage à libérer.
	 */
	public void setCharac(CityActorCharacter pCharac) {
		mCharac = pCharac;
	}
	
	/**
	 * Réinitialise l'Action. Est appellée lorsque l'Action est réutilisé (dans un forever par exemple).
	 */
	protected void end() {
		mIsBegan = false;
	}
}