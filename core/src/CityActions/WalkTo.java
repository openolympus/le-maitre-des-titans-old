package CityActions;

import CityActors.CityActor;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.TemporalAction;

/** 
 * Action qui déplace un CityActor d'un point A à un point B, en mettant à jour son oriention.
 */

public class WalkTo extends TemporalAction {
	
	/** Le CityActor à déplacer. */
	private CityActor mTarget;
	
	/** Point de départ. */
	private float mStartX, mStartY;
	
	/** Point d'arrivé. */
	private float mEndX, mEndY;

	/**
	 * Semblable au constructeur.
	 */
	protected void begin() {
		mStartX = mTarget.getX();
		mStartY = mTarget.getY();
		Vector2 lStart = new Vector2(mStartX, mStartY);
		Vector2 lEnd = new Vector2(mEndX, mEndY);
		((CityActor) mTarget).setOrientation(lStart.sub(lEnd).angle());
	}

	/**
	 * Boucle de rendu.
	 */
	protected void update(float percent) {
		// Prochaine position :
		mTarget.setPosition(mStartX + (mEndX - mStartX) * percent, mStartY + (mEndY - mStartY) * percent);
		
		// Mise à jour du depth :
		((CityActor) mTarget).updateDepth();
	}

	/**
	 * Définis le point d'arrivé.
	 */
	public void setDestination(float x, float y) {
		mEndX = x;
		mEndY = y;
	}
	
	/**
	 * Définis le CityActor à déplacer.
	 */
	public void setTarget(CityActor pTarget) {
		mTarget = pTarget;
	}
}
