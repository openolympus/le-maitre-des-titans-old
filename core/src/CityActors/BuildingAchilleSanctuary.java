package CityActors;

/**
 * Le sanctuaire d'Achille. 
 * C'est un ActorGroup composé d'un temple, d'une petite statue
 * et d'une coupole emflammée.
 */

public class BuildingAchilleSanctuary extends CityActorBuilding {

	/** CityActor composant ce ActorGroup. */
	private CityActor mFire;
	
	/** CityActorGroups composants ce ActorGroup. */
	private CityActorGroup mTemple, mStatue;
	
	/**
	 * Constructeur.
	 */
	public BuildingAchilleSanctuary(int pXPx, int pYPx) {
		super(pXPx, pYPx);
		setName("Santuaire d'Achille");

		// Dimensions :
		mWidthTile = 4;
		mHeightTile = 4;
		
		// Replace correctement ce CityActor en respectant l'isométrie :
		replaceOnGivenTile();
		
		// Création de l'Actor pour le temple :
		mTemple = new BuildingHeroTemple(pXPx, pYPx);
		
		// Création de l'Actor pour la petite statue :
		mStatue = new BuildingAchilleSmallStatue(pXPx, pYPx, 71, 13, mTemple.getTextureRegion());
		
		// Création de l'Actor pour la coupe de feu :
		mFire = new BuildingGobletFire(pXPx, pYPx);
		mFire.setX(getX() + 112);
		mFire.setY(getY() + 22);
		mFire.setDepth(mFire.getDepth() - 1);;
		
		// Ajout des CityActors enfants à cet ActorGroup :
		mAllChildren.add(mTemple);
		mAllChildren.add(mStatue);
		mAllChildren.add(mFire);
		
		// Ajout de ce CityActor et de tout ses enfants à leur graphe de scène :
		addToGraph(this);
	}
}
