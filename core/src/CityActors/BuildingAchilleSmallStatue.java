package CityActors;

import Screens.ScreenLoading;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * La petite statue d'Achille. Pour son temple.
 */

public class BuildingAchilleSmallStatue extends CityActorBuilding {
	
	/**
	 * Constructeur. Pour placer ce CityActor bien au milieu d'une tuile.
	 */
	public BuildingAchilleSmallStatue(int pXPx, int pYPx) {
		super(pXPx, pYPx);
		setName("Petite statue d'Achille");
		setDescription("Petite statue d'Achille.");
		
		// Dimensions :
		mWidthTile = 1;
		mHeightTile = 1;
		
		// Mise à jour de la map d'infos sur les sols :
		updateMapFloorInfos();
		
		// Replace correctement ce CityActor en respectant l'isométrie :
		setX(getX() - 1);
		setY(getY() - 1);
		replaceOnGivenTile();
		
		// Visuel :
		mTextureRegion = new TextureRegion(ScreenLoading.gAssetManager.get("buildings/petite_statue_achille.png", Texture.class));
		addTilesFromTexture(mTextureRegion);
		
		// Ajout de ce CityActor et de tout ses enfants à leur graphe de scène :
		addToGraph(this);
	}
	
	/**
	 * Constructeur. Pour pouvoir placer ce CityActor entre deux tuiles.
	 * Une gestion particulière est appliqué pour calculer correctement le depth.
	 */
	public BuildingAchilleSmallStatue(int pXPx, int pYPx, int pOffsetX, int pOffsetY, TextureRegion pTextRegBackground) {
		super(pXPx, pYPx);
		setName("Petite statue d'Achille");
		setDescription("Petite statue d'Achille.");
		
		// Visuel :
		mTextureRegion = new TextureRegion(ScreenLoading.gAssetManager.get("buildings/petite_statue_achille.png", Texture.class));
		addTilesFromTextureExtended(mTextureRegion, pOffsetX, pOffsetY, pTextRegBackground);
		
		// Ajout de ce CityActor et de tout ses enfants à leur graphe de scène :
		addToGraph(this);
	}
};
