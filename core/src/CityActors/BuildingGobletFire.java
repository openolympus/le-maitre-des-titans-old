package CityActors;

import Screens.ScreenCity;
import Screens.ScreenLoading;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * La coupole emflammée d'un santuaire/temple.
 */

public class BuildingGobletFire extends CityActor {
	
	/** L'animation du feu. */
	private Animation mAnimFire; 
	
	/**
	 * Constructeur.
	 */
	public BuildingGobletFire(int pXPx, int pYPx) {
		super(pXPx, pYPx);
		setName("Coupole de feu");
		setDescription("Une coupole de feu.");
		
		// Positionnement :
		setX(getX() + 16);
		setY(getY() + 8);
		
		// Dimensions :
		mWidthTile = 1;
		mHeightTile = 1;
		
		// Replace correctement ce CityActor en respectant l'isométrie :
		replaceOnGivenTile();
		
		// Visuel :
		mAnimFire = createAnimation(ScreenLoading.gAssetManager.get("buildings/feu_deco2.png", Texture.class), 24, 45);
		
		// Ajout de ce CityActor à son graphe de scène :
		addToGraph(this);
	}
	
	/**
	 * Renvoie la texture à afficher.
	 */
	public TextureRegion getTextureRegion() {
		mTextureRegion = mAnimFire.getKeyFrame(ScreenCity.gTime);
		return mTextureRegion;
	}
};