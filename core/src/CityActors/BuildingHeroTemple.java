package CityActors;

import Screens.ScreenLoading;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Le temple d'un héros anonyme
 */

public class BuildingHeroTemple extends CityActorBuilding {
	
	/**
	 * Constructeur.
	 */
	public BuildingHeroTemple(int pXPx, int pYPx) {
		super(pXPx, pYPx);
		setName("Temple");
		setDescription("Le temple d'un héros.");
		
		// Positionnement :
		mWidthTile = 4;
		mHeightTile = 4;
		replaceOnGivenTile();
		
		// Visuel :
		mTextureRegion = new TextureRegion(ScreenLoading.gAssetManager.get("buildings/sanctuaire_heros.png", Texture.class));
		addTilesFromTexture(mTextureRegion);
		
		// Ajout de ce CityActor et de tout ses enfants à leur graphe de scène :
		addToGraph(this);
	}
};