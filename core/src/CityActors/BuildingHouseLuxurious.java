package CityActors;

import CityEngine.CityMapRenderer;
import Screens.ScreenCity;

/**
 * Une maison luxueuse.
 */

public class BuildingHouseLuxurious extends CityActorBuilding {
	
	/** Le block Nord d'une maison luxueuse. */
	private BuildingHouseLuxuriousSlotNorth mSlotNorth;
	
	/** Le block Ouest d'une maison luxueuse. */
	private BuildingHouseLuxuriousSlotWest mSlotWest;
	
	/** Le block Sud d'une maison luxueuse. */
	private BuildingHouseLuxuriousSlotSouth mSlotSouth;
	
	/** Le block Est d'une maison luxueuse. */
	private BuildingHouseLuxuriousSlotEast mSlotEast;
	
	/**
	 * Constructeur.
	 */
	public BuildingHouseLuxurious(int pXPx, int pYPx) {
		super(pXPx, pYPx);
		setName("Maison luxueuse");
		setDescription("Une maison luxueuse.");
		
		// Dimensions :
		mWidthTile = 4;
		mHeightTile = 4;
		
		// Mise à jour de la map d'infos sur les sols :
		updateMapFloorInfos();
		
		// Replace correctement ce CityActor en respectant l'isométrie :
		replaceOnGivenTile();
		
		// Création de l'Actor pour le coin Nord :
		mSlotNorth = new BuildingHouseLuxuriousSlotNorth(pXPx, pYPx + 2*CityMapRenderer.gTileHeightPx);
		
		// Création de l'Actor pour le coin Ouest :
		mSlotWest = new BuildingHouseLuxuriousSlotWest(pXPx - CityMapRenderer.gTileWidthPx, pYPx + CityMapRenderer.gTileHeightPx);
		
		// Création de l'Actor pour le coin Sud :
		mSlotSouth = new BuildingHouseLuxuriousSlotSouth(pXPx, pYPx);
		
		// Création de l'Actor pour le coin Est :
		mSlotEast = new BuildingHouseLuxuriousSlotEast(pXPx + CityMapRenderer.gTileWidthPx, pYPx + CityMapRenderer.gTileHeightPx);
		
		// Ajout des CityActors enfants à ce ActorGroup :
		mAllChildren.add(mSlotNorth);
		mAllChildren.add(mSlotWest);
		mAllChildren.add(mSlotSouth);
		mAllChildren.add(mSlotEast);
		
		// Ajout de ce CityActor et de tout ses enfants à leur graphe de scène :
		addToGraph(this);
		
		// Ajout de ce CityActor au fonctionnement de la partie :
		ScreenCity.gAIManager.addToCityProcess(getAllChildren().first());
	}
};