package CityActors;

import Screens.ScreenLoading;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Le block Sud d'une maison luxueuse.
 */

public class BuildingHouseLuxuriousSlotSouth extends CityActorBuilding {
	
	/**
	 * Constructeur.
	 */
	public BuildingHouseLuxuriousSlotSouth(int pXPx, int pYPx) {
		super(pXPx, pYPx);
		setName("Maison luxueuse block Sud");
		setDescription("Le block Sud d'une maison luxueuse.");
		
		// Dimensions :
		mWidthTile = 2;
		mHeightTile = 2;

		// Replace correctement ce CityActor en respectant l'isométrie :
		replaceOnGivenTile();
		
		// Visuel :
		mTextureRegion = new TextureRegion(ScreenLoading.gAssetManager.get("buildings/habitation_luxe_sud.png", Texture.class));
		addTilesFromTexture(mTextureRegion);
		
		// Ajout de ce CityActor et de tout ses enfants au graphe de scène :
		addToGraph(this);
	}
};