package CityActors;

import Screens.ScreenCity;
import Screens.ScreenLoading;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Une maison modeste.
 */

public class BuildingHouseModest extends CityActorBuilding {
	
	/**
	 * Constructeur.
	 */
	public BuildingHouseModest(int pXPx, int pYPx) {
		super(pXPx, pYPx);
		setName("Maison modeste");
		setDescription("Une maison modeste.");
		
		// Dimensions :
		mWidthTile = 2;
		mHeightTile = 2;
		
		// Mise à jour de la map d'infos sur les sols :
		updateMapFloorInfos();
		
		// Replace correctement ce CityActor en respectant l'isométrie :
		replaceOnGivenTile();
		
		// Visuel :
		mTextureRegion = new TextureRegion(ScreenLoading.gAssetManager.get("buildings/habitation_modeste.png", Texture.class));
		addTilesFromTexture(mTextureRegion);
		
		// Ajout de ce CityActor et de tout ses enfants au graphe de scène :
		addToGraph(this);
		
		// Ajout de ce CityActor au fonctionnement de la partie :
		ScreenCity.gAIManager.addToCityProcess(getAllChildren().first());
	}
};
