package CityActors;

import Screens.ScreenLoading;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Un bassin.
 * 4 x 4 tuiles.
 */

public class BuildingPool extends CityActorBuilding {
	
	/**
	 * Constructeur.
	 */
	public BuildingPool(int pXPx, int pYPx) {
		super(pXPx, pYPx);
		setName("Bassin");
		mWidthTile = 4;
		mHeightTile = 4;
		replaceOnGivenTile();
		mTextureRegion = new TextureRegion(ScreenLoading.gAssetManager.get("buildings/bassin.png", Texture.class));
		addTilesFromTexture(mTextureRegion);
		
		// Ajout de ce CityActor et de tout ses enfants au graphe de scène :
		addToGraph(this);
	}
};
