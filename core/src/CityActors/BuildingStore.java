package CityActors;

/**
 * Un entrepot. 
 * C'est un ActorGroup composé d'une tuile où est le personnage et 
 * de 8 tuiles emplacements à marchandises.
 */

public class BuildingStore extends CityActorBuilding {

	/** CityActors composants cet ActorGroup. */
	private CityActor mHome, mSlot1, mSlot2, mSlot3, mSlot4, mSlot5, mSlot6, mSlot7, mSlot8;
	
	/**
	 * Constructeur.
	 */
	public BuildingStore (int pXPx, int pYPx) {
		super(pXPx, pYPx);
		setName("Entrepot");
		mWidthTile = 3;
		mHeightTile = 3;
		replaceOnGivenTile();
		
		// Création de l'Actor de l'emplacement du personnage :
		mHome = new BuildingStoreHome(pXPx, pYPx);
		
		// Création des CityActors des emplacements à marchandises  :
		mSlot1 = new BuildingStoreSlot(pXPx + 43, pYPx + 25);
		mSlot1.setDepth(mSlot1.getDepth() - 25);
		mSlot2 = new BuildingStoreSlot(pXPx + 43, pYPx + 25);
		mSlot2.setDepth(mSlot1.getDepth() - 25);
		mSlot3 = new BuildingStoreSlot(pXPx + 43, pYPx + 25);
		mSlot3.setDepth(mSlot1.getDepth() - 25);
		mSlot4 = new BuildingStoreSlot(pXPx + 43, pYPx + 25);
		mSlot4.setDepth(mSlot1.getDepth() - 25);
		mSlot5 = new BuildingStoreSlot(pXPx + 43, pYPx + 25);
		mSlot5.setDepth(mSlot1.getDepth() - 25);
		mSlot6 = new BuildingStoreSlot(pXPx + 43, pYPx + 25);
		mSlot6.setDepth(mSlot1.getDepth() - 25);
		mSlot7 = new BuildingStoreSlot(pXPx + 43, pYPx + 25);
		mSlot7.setDepth(mSlot1.getDepth() - 25);
		mSlot8 = new BuildingStoreSlot(pXPx + 43, pYPx + 25);
		mSlot8.setDepth(mSlot1.getDepth() - 25);
		
		// Ajout des CityActors enfants à cet ActorGroup :
		mAllChildren.add(mHome);
		mAllChildren.add(mSlot1);
		mAllChildren.add(mSlot2);
		mAllChildren.add(mSlot3);
		mAllChildren.add(mSlot4);
		mAllChildren.add(mSlot5);
		mAllChildren.add(mSlot6);
		mAllChildren.add(mSlot7);
		mAllChildren.add(mSlot8);
		
		// Ajout de ce CityActor et de tout ses enfants au graphe de scène :
		addToGraph(this);
	}
}

