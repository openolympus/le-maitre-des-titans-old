package CityActors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import Screens.ScreenLoading;

/**
 * L'accueil d'un entrepot, emplacement où se tient le personnage de l'entrepot.
 */

public class BuildingStoreHome extends CityActorBuilding {
	
	/**
	 * Constructeur.
	 */
	public BuildingStoreHome(int pXPx, int pYPx) {
		super(pXPx, pYPx);
		setName("Accueil d'un entrepot");
		mWidthTile = 1;
		mHeightTile = 1;
		replaceOnGivenTile();
		mTextureRegion = new TextureRegion(ScreenLoading.gAssetManager.get("buildings/entrepot_accueil.png", Texture.class));
		
		// Ajout de ce CityActor au graphe de scène :
		addToGraph(this);
	}
};