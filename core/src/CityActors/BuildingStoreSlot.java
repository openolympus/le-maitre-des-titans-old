package CityActors;

import Screens.ScreenLoading;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Emplacement pour stoquer de la marchandise sur un entrepot.
 */

public class BuildingStoreSlot extends CityActorBuilding {
	
	/**
	 * Constructeur.
	 */
	public BuildingStoreSlot(int pXPx, int pYPx) {
		super(pXPx, pYPx);
		setName("Emplacement à marchandise");
		mWidthTile = 1;
		mHeightTile = 1;
		replaceOnGivenTile();
		mTextureRegion = new TextureRegion(ScreenLoading.gAssetManager.get("buildings/entrepot_emplacement.png", Texture.class));
		
		// Ajout de ce CityActor au graphe de scène :
		addToGraph(this);
	}
};
