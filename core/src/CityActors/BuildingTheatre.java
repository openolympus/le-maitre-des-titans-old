package CityActors;

import Screens.ScreenLoading;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Un théatre.
 */

public class BuildingTheatre extends CityActorBuilding {
	
	/**
	 * Constructeur.
	 */
	public BuildingTheatre(int pXPx, int pYPx) {
		super(pXPx, pYPx);
		setName("Théâtre");
		setDescription("Un théâtre, c'est bien.");
		mTargetCharacType = BuildingHouseModest.class;
		
		// Positionnement :
		mWidthTile = 5;
		mHeightTile = 5;
		mEnterExit = FindExit();
		
		// Mise à jour de la map d'infos sur les sols :
		updateMapFloorInfos();
		
		// Replace correctement ce CityActor en respectant l'isométrie :
		replaceOnGivenTile();

		// Visuel :
		mTextureRegion = new TextureRegion(ScreenLoading.gAssetManager.get("buildings/theatre.png", Texture.class));
		addTilesFromTexture(mTextureRegion);
		Texture lTextureAnimation = ScreenLoading.gAssetManager.get("buildings/theatreAnim1.png", Texture.class);
		Animation lAnimation = createAnimation(lTextureAnimation, mTextureRegion.getRegionWidth(), lTextureAnimation.getHeight());
		addTilesAnimatedFromAnimation(lAnimation);
		
		// Ajout de ce CityActor et de tout ses enfants à leur graphe de scène :
		addToGraph(this);
	}
};
