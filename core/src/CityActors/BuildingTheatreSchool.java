package CityActors;

import Screens.ScreenCity;
import Screens.ScreenLoading;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Une école de théâtre.
 */

public class BuildingTheatreSchool extends CityActorBuilding {
	
	/**
	 * Constructeur.
	 */
	public BuildingTheatreSchool(int pXPx, int pYPx) {
		super(pXPx, pYPx);
		setName("École de théâtre");
		setDescription("Une école de théâtre.");
		
		// Dimensions :
		mWidthTile = 3;
		mHeightTile = 3;
		mEnterExit = FindExit();

		// Mise à jour de la map d'infos sur les sols :
		updateMapFloorInfos();
		
		// Replace correctement ce CityActor en respectant l'isométrie :
		replaceOnGivenTile();
		
		// Visuel non animé :
		mTextureRegion = new TextureRegion(ScreenLoading.gAssetManager.get("buildings/ecole_theatre.png", Texture.class));
		addTilesFromTexture(mTextureRegion);
		
		// Visuel animé :
		Texture lTextureAnimation = ScreenLoading.gAssetManager.get("buildings/ecole_theatre_animation.png", Texture.class);
		Animation lAnimation = createAnimation(lTextureAnimation, mTextureRegion.getRegionWidth(), lTextureAnimation.getHeight());
		addTilesAnimatedFromAnimation(lAnimation);
		setAnimationVisible(false);
		
		// Ajout de ce CityActor et de tout ses enfants à leur graphe de scène :
		addToGraph(this);
		
		// Ajout de ce CityActor au fonctionnement de la partie :
		// (On ajoute en réalité la premiere tuile enfant de ce CityActor car le CityActor n'est pas dans les graphes de scène)
		ScreenCity.gAIManager.addToCityProcess(getAllChildren().first());
	}
};
