package CityActors;

import Screens.ScreenLoading;

import com.badlogic.gdx.graphics.Texture;

/**
 * Un acteur de théâtre.
 */

public class CharacterActor extends CityActorCharacter {
	
	/**
	 * Constructeur.
	 */
	public CharacterActor(int pXPx, int pYPx) {
		super(pXPx, pYPx);
		setName("Acteur");
		setDescription("Un acteur de théâtre.");
		
		// Positionnement :
		setX(getX() + 5);
		updateDepth();
		
		// Visuel :
		mAnimWalkN = createAnimation(ScreenLoading.gAssetManager.get("characters/ActeurDeplaceN.png", Texture.class), 46, 45); 
		mAnimWalkNE = createAnimation(ScreenLoading.gAssetManager.get("characters/ActeurDeplaceNE.png", Texture.class), 46, 45); 
		mAnimWalkE = createAnimation(ScreenLoading.gAssetManager.get("characters/ActeurDeplaceE.png", Texture.class), 46, 45);  
		mAnimWalkSE = createAnimation(ScreenLoading.gAssetManager.get("characters/ActeurDeplaceSE.png", Texture.class), 46, 45); 
		mAnimWalkS = createAnimation(ScreenLoading.gAssetManager.get("characters/ActeurDeplaceS.png", Texture.class), 46, 45); 
		mAnimWalkSW = createAnimation(ScreenLoading.gAssetManager.get("characters/ActeurDeplaceSO.png", Texture.class), 46, 45);
		mAnimWalkW = createAnimation(ScreenLoading.gAssetManager.get("characters/ActeurDeplaceO.png", Texture.class), 46, 45); 
		mAnimWalkNW = createAnimation(ScreenLoading.gAssetManager.get("characters/ActeurDeplaceNO.png", Texture.class), 46, 45); 
		mAnimDie = createAnimation(ScreenLoading.gAssetManager.get("characters/ActeurMort.png", Texture.class), 46, 45); 
		
		// Ajout de ce CityActor et de tout ses enfants à son graphe de scène :
		addToGraph(this);
	}
}
