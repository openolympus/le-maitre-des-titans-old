package CityActors;

import Screens.ScreenLoading;

import com.badlogic.gdx.graphics.Texture;



/**
 * Arès, dieu de la Guerre et de la Destruction.
 */

public class CharacterAres extends CityActorCharacter {
	
	/**
	 * Constructeur.
	 */
	public CharacterAres(int pXPx, int pYPx) {
		super(pXPx, pYPx);
		setName("Arès");
		setDescription("Arès, dieu de la Guerre et de la Destruction.");
		
		// Positionnement :
		setX(getX() - 45);
		setY(getY() - 25);
		mWidthTile = 1;
		mHeightTile = 1;
		replaceOnGivenTile();
		
		// Visuel :
		mAnimWalkN = createAnimation(ScreenLoading.gAssetManager.get("characters/AresMarcheN.png", Texture.class), 155, 167); 
		mAnimWalkNE = createAnimation(ScreenLoading.gAssetManager.get("characters/AresMarcheNE.png", Texture.class), 155, 167); 
		mAnimWalkE = createAnimation(ScreenLoading.gAssetManager.get("characters/AresMarcheE.png", Texture.class), 155, 167);  
		mAnimWalkSE = createAnimation(ScreenLoading.gAssetManager.get("characters/AresMarcheSE.png", Texture.class), 155, 167); 
		mAnimWalkS = createAnimation(ScreenLoading.gAssetManager.get("characters/AresMarcheS.png", Texture.class), 155, 167); 
		mAnimWalkSW = createAnimation(ScreenLoading.gAssetManager.get("characters/AresMarcheSO.png", Texture.class), 155, 167);
		mAnimWalkW = createAnimation(ScreenLoading.gAssetManager.get("characters/AresMarcheO.png", Texture.class), 155, 167); 
		mAnimWalkNW = createAnimation(ScreenLoading.gAssetManager.get("characters/AresMarcheNO.png", Texture.class), 155, 167); 
		
		// Ajout de ce CityActor et de tout ses enfants à son graphe de scène :
		addToGraph(this);
	}
}
