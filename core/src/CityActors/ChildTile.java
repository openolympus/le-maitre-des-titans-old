package CityActors;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Tile isométrique d'une map de cité.
 * Elle ne sert qu'à découper des CityActors de plusieurs tuiles de coté.
 * /!\ Attention ce CityActor ne peut pas être appellé dans une sauvegarde XML,
 *     car les CityActors d'une sauvegarde ne doivent obligatoirement prendre en 
 *     arguments que deux int, ce qui n'est pas le cas ici. 
 */

public class ChildTile extends CityActor {
	
	/** Le CityActor dont est issue cette tuile. */
	private CityActor mFather;
	
	/**
	 * Constructeur.
	 */
	public ChildTile(TextureRegion pTextureRegion, int pXPx, int pYPx, CityActor pFather) {
		super(pXPx, pYPx);
		mFather = pFather;
		setName("Tuile isométrique");
		
		// Positionnement :
		mWidthTile = 1;
		mHeightTile = 1;
		replaceOnGivenTile();
		
		// Visuel :
		mTextureRegion = pTextureRegion;
		
		// Ajout de ce CityActor à son graphe de scène :
		addToGraph(this);
	}
	
	/**
	 * Retourne le type du CityActor dont est issue cette tuile.
	 */
	public CityActor getFather() {
		return mFather;
	}
}
