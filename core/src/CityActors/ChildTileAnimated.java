package CityActors;

import Screens.ScreenCity;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Tile isométrique animée d'une map de cité.
 * /!\ Attention ce CityActor ne peut être appellé dans une sauvegarde XML,
 *     car les CityActors d'une sauvegarde ne doivent obligatoirement prendre en 
 *     arguments que deux int, ce qui n'est pas le cas ici. 
 */

public class ChildTileAnimated extends CityActor {
	
	/** L'animation de la tuile. */
	private Animation mAnimation; 
	
	/**
	 * Constructeur.
	 */
	public ChildTileAnimated(Animation pAnimation, int pXPx, int pYPx) {
		super(pXPx, pYPx);
		setName("Tuile isométrique animée");
		
		// Positionnement :
		mWidthTile = 1;
		mHeightTile = 1;
		replaceOnGivenTile();
		
		// Visuel :
		mAnimation = pAnimation;
		
		// Ajout de ce CityActor à son graphe de scène :
		addToGraph(this);
	}
	
	/**
	 * Renvoie la texture à afficher.
	 */
	public TextureRegion getTextureRegion() {
		mTextureRegion = mAnimation.getKeyFrame(ScreenCity.gTime * 0.8f);
		return mTextureRegion;
	}
}
