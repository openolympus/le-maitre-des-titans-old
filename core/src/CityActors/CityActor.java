package CityActors;

import ApplicationConfig.MainApplicationConfig;
import CityEngine.AIManager;
import CityEngine.CityMapRenderer;
import CityEngine.SceneGraphManager;
import CityEngine.SceneGraphManager.eGraphType;
import Screens.ScreenLoading;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Un CityActor est un élément sur la carte de la cité.
 * Il étend un Actor ce qui lui permet d'avoir un comportement (actions)
 * ainsi qu'une gestion des écouteurs facilitée.
 * Il possede en plus une region texture qui est sont image rendue sur la map.
 * Les positions de l'actor (getX() et getY()) seront les positions de l'image
 * sur la map.
 * Le mDepth permet de définir la position de l'image en profondeur (z-index) sur la map.
 */

public class CityActor extends Actor {
	
	/** La texture region de ce CityActor. Une texture region contient une texture et des infos de recadrage. */
	protected TextureRegion mTextureRegion;
	
	/** La profondeur en z de cet élément */
	protected float mDepth;
	
	/** La description. Est affichée pendant une partie dans la fenetre de description (clic droit). */
	protected String mDescription;
	
	/** Type possibles. */
	public enum eFloorType { 
		Street, Wild, Water, Building
	}
	
	/** Type de ce sol. */
	protected eFloorType mFloorType;
	
	/** Longueur et hauteur en tuiles de la surface au sol cet Actor. */
	protected int mWidthTile, mHeightTile;
	
	/** L'orientation de cet Actor, en degrés de 0 à 360. A partir de midi et dans le sens horaire. */
	protected float mOrientation;

	/** Type du graphe de scène au quel appartient ce CityActor. */
	protected eGraphType mGraphType;
	
	/**
	 * Constructeur.
	 * 
	 * @param pXPx Position X sur la map exprimée en pixels 
	 * @param pYPx Position Y sur la map exprimée en pixels 
	 */
	public CityActor(int pXPx, int pYPx) {
		setName("Objet de la cité");
		setDescription("La description de cet élément n'a pas encore été écrite.");
		mGraphType = eGraphType.Grounds;
		
		// Caractéristiques :
		mFloorType = eFloorType.Wild;
		
		// Positionnement :
		setX(pXPx);
		setY(pYPx);
		setOriginX(getX());
		setOriginY(getY());
		mDepth = getY();
		
		// Visuel :
		mTextureRegion = new TextureRegion(ScreenLoading.gAssetManager.get("characters/textureParDefaut.png", Texture.class));
		mOrientation = 90.0f;
	}
	
	/** 
	 * Définis la profondeur z de cet élément. 
	 */
	public void setDepth(float pDepth) {
		mDepth = pDepth;
	}
	
	/** 
	 * Retourne la profondeur z de cet élément. 
	 */
	public float getDepth() {
		return mDepth;
	}
	
	/** 
	 * Met à jour la profondeur z de cet élément. 
	 */
	public void updateDepth() {
		mDepth = getY() - CityMapRenderer.gTileHeightPx/2;
	}
	
	/** 
	 * Définis l'orientation, en degrés de 0 à 360. A partir de midi et dans le sens horaire.
	 */
	public void setOrientation(float pOrientation) {
		mOrientation = pOrientation;
	}
	
	/** 
	 * Retourne l'orientation, en degrés de 0 à 360. A partir de midi et dans le sens horaire.
	 */
	public float getOrientation() {
		return mOrientation;
	}
	
	/** 
	 * Retourne la RegionTexture de cet élément. 
	 */
	public TextureRegion getTextureRegion() {
		return mTextureRegion;
	}
	
	/** 
	 * Définis la description cet élément. 
	 */
	public void setDescription(String pDescription) {
		mDescription = pDescription;
	}
	
	/** 
	 * Retourne la description cet élément. 
	 */
	public String getDescription() {
		return mDescription;
	}
	
	/**
	 * Ajoute au graphe de scène de type pStageType, le CityActor pStageType, 
	 * ainsi que tout ses CityActors enfants (récursivité). 
	 * Les doublons sont évités grâce au fait que LibGDX soit très bien codé.
	 * (Voir la méthode addActor() de Group)
	 * 
	 * Il y a 4 types de stages :
	 * eGraph.Grounds ----> gStageGrounds
	 * eGraph.Buildings --> gStageBuildings
	 * eGraph.Characters -> gStageCharacters
	 * eGraph.Effects ----> gStageEffects
	 * 
	 * @param pCityActor Le CityActor à ajouter au graphe.
	 * @param pStageType Le type du stage dans lequel ajouter le CityActor
	 */
	public void addToGraph(CityActor pCityActor) {
		if (CityActorGroup.class.isAssignableFrom(pCityActor.getClass())) {
			for (CityActor lChild : ((CityActorGroup) pCityActor).getAllChildren()) {
				addToGraph(lChild);
			}
		} else {
			if (CityActor.class.isAssignableFrom(pCityActor.getClass())) {
				switch (mGraphType) {
					case Grounds: SceneGraphManager.gStageGrounds.addActor(pCityActor); break;
					case Buildings: SceneGraphManager.gStageBuildings.addActor(pCityActor); break;
					case Characters: SceneGraphManager.gStageCharacters.addActor(pCityActor); break;
					default: SceneGraphManager.gStageEffects.addActor(pCityActor);
				}
			} else {
				// On essaie d'ajouter à un graphe quelque chose qui n'est ni un CityActorGroup, ni un CityActor.
			}
		}
	}
	
	/**
	 * Creer une animation à partir de la collection d'images passée en argument.
	 * 
	 * @param pTextAnim Collection des Sprites
	 * @param pAnimWidth La largeur d'une image de la collection
	 * @param pAnimHeight La hauteur d'une image de la collection
	 * @return L'animation construite avec cette collection.
	 */
	public Animation createAnimation(Texture pTexture, int pAnimWidth, int pAnimHeight) {
        TextureRegion[][] tmp = TextureRegion.split(pTexture, pAnimWidth, pAnimHeight);
        TextureRegion[] lArrayTexture = new TextureRegion[(pTexture.getWidth()/pAnimWidth) * (pTexture.getHeight()/pAnimHeight)];  // Tableau des sprites
        int index = 0;
        for (int i = 0; i < pTexture.getHeight()/pAnimHeight; i++) {
            for (int j = 0; j < pTexture.getWidth()/pAnimWidth; j++) {
                lArrayTexture[index++] = tmp[i][j]; // Dans chaque case de lArrayTexture on met une sprite de lTexture
            }
        }  
        Animation lAnim = new Animation(MainApplicationConfig.gAnimationStepTime, lArrayTexture);  
        lAnim.setPlayMode(Animation.PlayMode.LOOP);
        return lAnim; 
	}
	
	/**
	 * Place la tuile du coin du bas de ce CityActor à la position rééllement donnée.
	 */
	public void replaceOnGivenTile() {
		if (mWidthTile > 1) {
			if (mWidthTile % 2 == 0) {
				setX(getX() - (mWidthTile/2)*CityMapRenderer.gTileWidthPx + CityMapRenderer.gTileWidthPx/2);
			} else {
				setX(getX() - (mWidthTile/2)*CityMapRenderer.gTileWidthPx);
			}
		}
	}
	
	/**
	 * Fonction privée mettant récursivement à jour la map des infos sur les sols. Utilisée par updateMapGroundsInfos()
	 */	
	private void recursiveUpdateMapFloorsInfos(int pWidthTile, int pCurrentWidthTile, int pHeightTile , int pCurrentHeightTile) {
		// Position en pixels du centre de la tuile :
		Vector2 lTileCenterPx = new Vector2(getX() + CityMapRenderer.gTileWidthPx/2, getY() + CityMapRenderer.gTileHeightPx/2);
		
		// Ajout à la map des infos sur les sols :
		Vector2 lTileID =  CityMapRenderer.getTileIndexByPixel(Math.round(lTileCenterPx.x + pCurrentWidthTile * CityMapRenderer.gTileWidthPx/2), 
				                                               Math.round(lTileCenterPx.y + pCurrentHeightTile*CityMapRenderer.gTileHeightPx/2));
		AIManager.gMapFloorsInfos.put(lTileID, mFloorType);
		
		// Puis on lance la récursivité :
		if (pCurrentWidthTile < pWidthTile) {
			recursiveUpdateMapFloorsInfos(pWidthTile, pCurrentWidthTile + 1, pCurrentHeightTile, pCurrentHeightTile + 1);
		}
		
		// On continue de monter si on le doit :
		if (pCurrentHeightTile < pHeightTile ) {
			recursiveUpdateMapFloorsInfos(pWidthTile - 1, pCurrentWidthTile - 1, pHeightTile, pCurrentHeightTile + 1);
		}
	}
	
	/**
	 * Mise à jour des informations de la map des infos sur les sols.
	 */
	public void updateMapFloorInfos() {
		recursiveUpdateMapFloorsInfos(mWidthTile - 1, 0, mHeightTile - 1, 0);
	}
}
