package CityActors;

import CityEngine.AIManager;
import CityEngine.CityMapRenderer;
import CityEngine.SceneGraphManager.eGraphType;


import com.badlogic.gdx.math.Vector2;

/**
 * Un bâtiment est un CityActorGroup avec quelques attributs en plus comme
 * une case d'entrée/sortie.
 */

public class CityActorBuilding extends CityActorGroup {
	
	/** Position, relative au bâtiment, de la case d'entrée sortie. En pixels. */
	protected Vector2 mEnterExit;

	/** Le type de bâtiment où les personnages qui sortent de ce bâtiment iront. */
	protected Class<?> mTargetCharacType;
	
	/**
	 * Constructeur.
	 */
	public CityActorBuilding(int pXPx, int pYPx) {
		super(pXPx, pYPx);
		mGraphType = eGraphType.Buildings;
		mEnterExit = new Vector2(-1, -1);
		mTargetCharacType = BuildingTheatre.class;
		
		// Caractéristiques :
		mFloorType = eFloorType.Building;
	}
	
	/**
	 * Définis la case d'entrée/sortie de ce bâtiment.
	 * Recherche parmis toutes les cases du pourtour du bâtiment et 
	 * définis la premiere case route rencontré comme étant l'entrée/sortie.
	 */
	public Vector2 FindExit() {
		Vector2 lEnterExit = new Vector2(getX(), getY()); 
		for (int i = 0; i < mHeightTile; i++) {
			lEnterExit = new Vector2(getX() - i*CityMapRenderer.gTileWidthPx/2 , getY() + i*CityMapRenderer.gTileHeightPx/2);
			if (isStreet((int) lEnterExit.x, (int) lEnterExit.y)) {
				return lEnterExit;
			}
			if (isStreet((int) lEnterExit.x + (mWidthTile +1)*CityMapRenderer.gTileWidthPx/2, (int) lEnterExit.y + (mWidthTile + 1 )*CityMapRenderer.gTileHeightPx/2)) {
				return lEnterExit.add((mWidthTile +1)*CityMapRenderer.gTileWidthPx/2, (mWidthTile + 1 )*CityMapRenderer.gTileHeightPx/2);
			}
		}
		for (int i = 0; i < mWidthTile; i++) {
			lEnterExit = new Vector2(getX() + (i + 2)*CityMapRenderer.gTileWidthPx/2 , getY() + i*CityMapRenderer.gTileHeightPx/2);
			if (isStreet((int) lEnterExit.x, (int) lEnterExit.y)) {
				return lEnterExit;
			}
			if (isStreet((int) lEnterExit.x - (mHeightTile + 1)*CityMapRenderer.gTileWidthPx/2, (int) lEnterExit.y + (mHeightTile + 1)*CityMapRenderer.gTileHeightPx/2)) {
				return lEnterExit.add(-(mHeightTile + 1)*CityMapRenderer.gTileWidthPx/2, (mHeightTile + 1)*CityMapRenderer.gTileHeightPx/2);
			}			
		}
		// Valeur par défaut si on ne trouve rien.
		return new Vector2(-1, -1);
	}
	
	/** 
	 * Retourne vrai si la position de tuile donnée en argument (en pixels) correspond
	 * à une route.
	 */
	private boolean isStreet(int lXPx, int lYpx){
		Vector2 lEnterExitID = CityMapRenderer.getTileIndexByPixel(lXPx, lYpx);
		if (AIManager.gMapFloorsInfos.containsKey(lEnterExitID)) {
			return AIManager.gMapFloorsInfos.get(lEnterExitID) == eFloorType.Street;
		} else {
			return false;
		}
	}
	
	/**
	 * Retourne la position de l'entrée et de la sortie pour les personnages de ce bâtiment.
	 * En coordonnées espace scène.
	 */
	public Vector2 getEnterExit() {
		return mEnterExit;
	}
	
	/**
	 * Rend visible ou invisible l'animation du bâtiment.
	 */
	public void setAnimationVisible(boolean pVisible) {
		for (CityActor lChild : getAllChildren()) {
			if (ChildTileAnimated.class.isAssignableFrom(lChild.getClass())) {
				lChild.setVisible(pVisible);
			}
		}
	}
}
