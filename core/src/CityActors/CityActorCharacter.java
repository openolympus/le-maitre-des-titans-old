package CityActors;

import CityEngine.SceneGraphManager.eGraphType;
import Screens.ScreenCity;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Pool.Poolable;


/**
 * Un personnages est un CityActor avec quelques attributs en plus.
 */

public class CityActorCharacter extends CityActor implements Poolable {
	
	/** Animations de marche dans les 8 directions. */
	protected Animation mAnimWalkN, mAnimWalkNE, mAnimWalkE, mAnimWalkSE, mAnimWalkS, mAnimWalkSW, mAnimWalkW, mAnimWalkNW; 
	
	/** Animation de mort. */
	protected Animation mAnimDie; 

	/**
	 * Constructeur.
	 */
	public CityActorCharacter(int pXPx, int pYPx) {
		super(pXPx, pYPx);
		mGraphType = eGraphType.Characters;
	}
	
	/**
	 * Renvoie la bonne texture à afficher en fonction de l'orientation.
	 */
	public TextureRegion getTextureRegion() {
		if (337.5f <= mOrientation || mOrientation < 22.5f) {
			mTextureRegion = mAnimWalkW.getKeyFrame(ScreenCity.gTime); // O
		}
		if (22.5f <= mOrientation && mOrientation < 67.5f) {
			mTextureRegion = mAnimWalkSW.getKeyFrame(ScreenCity.gTime); // SO
		}
		if (67.5f <= mOrientation && mOrientation < 112.5f) {
			mTextureRegion = mAnimWalkS.getKeyFrame(ScreenCity.gTime); // S
		}
		if (112.5f <= mOrientation && mOrientation < 157.5f) {
			mTextureRegion = mAnimWalkSE.getKeyFrame(ScreenCity.gTime); // SE
		}
		if (157.5f <= mOrientation && mOrientation < 202.5f) {
			mTextureRegion = mAnimWalkE.getKeyFrame(ScreenCity.gTime); // E
		}
		if (202.5f <= mOrientation && mOrientation < 247.5f) {
			mTextureRegion = mAnimWalkNE.getKeyFrame(ScreenCity.gTime); // NE
		}
		if (247.5f <= mOrientation && mOrientation < 292.5f) {
			mTextureRegion = mAnimWalkN.getKeyFrame(ScreenCity.gTime); // N
		}
		if (292.5f <= mOrientation && mOrientation < 337.5f) {
			mTextureRegion = mAnimWalkNW.getKeyFrame(ScreenCity.gTime); // NO
		}
		return mTextureRegion;
	}
	
	/**
	 * Replace ce CityActor dans un état tel, qu'il puisse être réutilisé (sytème de pool).
	 */
	public void reset() {
		
	}
}
