package CityActors;

import CityEngine.SceneGraphManager.eGraphType;
import Screens.ScreenLoading;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;

/**
 * Un sol est un CityActor avec quelques attributs en plus.
 */

public class CityActorGround extends CityActor {
	
	/** Pack de textures. */
	protected TextureAtlas mGroundAtlas;

	/**
	 * Constructeur.
	 */
	public CityActorGround(int pXPx, int pYPx) {
		super(pXPx, pYPx);
		mGraphType = eGraphType.Grounds;
		
		// Pack des textures des sols :
		mGroundAtlas = ScreenLoading.gAssetManager.get("ground/ground1.atlas", TextureAtlas.class);
		
		// Caractéristiques :
		mFloorType = eFloorType.Wild;
		
		// Positionnement :
		setDepth(getDepth() + 1);
		mWidthTile = 1;
		mHeightTile = 1;
		replaceOnGivenTile();
	}
}
