package CityActors;

/**
 * Forêt. 
 * C'est un CityActorGround avec quelques attributs en plus.
 */

public class CityActorGroundForest extends CityActorGround {

	/**
	 * Constructeur.
	 */
	public CityActorGroundForest(int pXPx, int pYPx) {
		super(pXPx, pYPx);
		
		// Caractéristiques :
		mFloorType = eFloorType.Wild;
		
		// Mise à jour de la map d'infos sur les sols :
		updateMapFloorInfos();
	}
}