package CityActors;

/**
 * Une rue, un chemin, un boulevard ou une avenue. 
 * C'est un CityActorGround avec quelques attributs en plus.
 */

public class CityActorGroundStreet extends CityActorGround {

	/**
	 * Constructeur.
	 */
	public CityActorGroundStreet(int pXPx, int pYPx) {
		super(pXPx, pYPx);
		
		// Caractéristiques :
		mFloorType = eFloorType.Street;
		
		// Mise à jour de la map d'infos sur les sols :
		updateMapFloorInfos();
	}
}