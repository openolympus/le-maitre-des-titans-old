package CityActors;

import ApplicationConfig.MainApplicationConfig;
import CityEngine.CityMapRenderer;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

/**
 * Un Actor contenant un groupe de CityActors.
 * Cela permet notamment d'afficher un bâtiment de plusieurs tiles avec l'effet de depth
 * correctement ajusté à chaqu'une de ses tiles.
 * Et de manipuler toutes les positions de ses CityActors enfant en ne manipulant que la
 * position du CityActorGroup : les positions des CityActors sont
 * ajoutées à celle du CityActorGroup.
 */

public class CityActorGroup extends CityActor {
	
	/** Tableau contenant tout les CityMapActors enfants de ce groupe. */
	protected Array<CityActor> mAllChildren;

	/**
	 * Constructeur.
	 */
	public CityActorGroup(int pXPx, int pYPx) {
		super(pXPx, pYPx);
		setName("Groupe d'objets sur la carte de jeu");
		
		// Crée un tableau non ordonné d'une capacité de 16 CityMapActors, de tout les enfants. La capacité s'agrandie automatiquement si besoin est.
		mAllChildren = new Array<CityActor>(false, 16); 
	}
	
	/**
	 * Retourne le tableau de tout les CityMapActors enfants de ce groupe.
	 */
	public Array<CityActor> getAllChildren() {
		return mAllChildren;
	}
	
	/**
	 * Permet de gerer correctement le depth d'un bâtiment de plusieurs tuiles de coté.
	 * Transforme la texture de plusieurs tuiles de coté passée en argument en tuiles enfants de taille 1
	 * découpées avec les mêmes bandes du découpage de la texture de fond. 
	 * 
	 * /!\ mTextureRegion doit être définis avant l'appel de cette méthode.
	 * 
	 * Fonctionnement :
	 *  - Prend la texture représentant le bâtiment de plusieurs tuiles de coté. 
	 *  - La découpe en bandes de gTileWidthPx/2 px de large.
	 *  - Pour chaque bande :
	 * 		- Crée une ChildTile avec. 
	 *  	- Calcul son depth.
	 *  	- L'ajoute au tableau des enfants du CityActorGroup.
	 */
	public void addTilesFromTexture(TextureRegion pTextureRegion) {
		// Nombre de tranches que l'on aura sur ce bâtiment :
		int lNbrBands = (pTextureRegion.getRegionWidth()+2) / (CityMapRenderer.gTileWidthPx/2);
		
		// Pour toutes les tranches :
		for (int x = 0; x < lNbrBands; x++) {
			// On découpe :
			int lOffset = 0;
			if (x == lNbrBands-1) {
				lOffset = -2;
			}
			TextureRegion lTextReg = new TextureRegion(pTextureRegion);
			lTextReg.setRegion((CityMapRenderer.gTileWidthPx/2)*x, 0, CityMapRenderer.gTileWidthPx/2 + lOffset, pTextureRegion.getRegionHeight()); 
			
			// On créer une nouvelle tuile :
			ChildTile lTile = new ChildTile(lTextReg, Math.round(getX() + (CityMapRenderer.gTileWidthPx/2)*x), Math.round(getY()), this);
			
			// On calcul son depth :
			if ( x < lNbrBands/2) {
				lTile.mDepth += (CityMapRenderer.gTileHeightPx/2) * ((lNbrBands/2) - x - 1);
			} else {
				lTile.mDepth += (CityMapRenderer.gTileHeightPx/2) * (x - (lNbrBands/2));
			}
			
			// On l'ajout au tableau des enfants :
			mAllChildren.add(lTile);
		}
	}
	
	/**
	 * Permet de superposer une animation de plusieurs tuiles de coté sur un bâtiment.
	 * Transforme l'Animation passée en argument en tuiles animées de taille 1 découpées avec les
	 * mêmes bandes du découpage de la texture de fond.
	 * Cela permet de gérer le correctement le depth d'une Animation de plusieurs tuiles de coté que l'on 
	 * superpose à un bâtiment.
	 * 
	 * @param pAnimation L'Animation à transformer.
	 */
	public void addTilesAnimatedFromAnimation(Animation pAnimation) {
		// Nombre de tranches que l'on aura sur cette Animation :
		int lNbrBands = (mTextureRegion.getRegionWidth()+2) / (CityMapRenderer.gTileWidthPx/2);
		
		// Pour toutes les tranches :
		for (int x = 0; x < lNbrBands; x++) {
			// On créer un tableau qui stoquera les tranches pour cette itération de coupes :
			Array<TextureRegion> lBands = new Array<TextureRegion>(pAnimation.getKeyFrames().length);
			
			// Pour toutes les frames de cette Animation :
			TextureRegion[] lAnimTextureRegions = pAnimation.getKeyFrames();
			for (int k = 0; k < lAnimTextureRegions.length; k++) {
				// On utilise la frame courante pour la découpe :
				int lOffset = 0;
				if (x == lNbrBands-1) {
					lOffset = -2;
				}
				TextureRegion lBand = new TextureRegion(lAnimTextureRegions[k]);
				lBand.setRegion((CityMapRenderer.gTileWidthPx/2)*x + lAnimTextureRegions[k].getRegionX(), 
						         0, 
						         CityMapRenderer.gTileWidthPx/2 + lOffset, 
						         lAnimTextureRegions[0].getRegionHeight()); 
				
				// On stoque la bande découpée :
				lBands.add(lBand);
			}
			
			// On créer une nouvelle tuile animée à partir des bandes coupées :
	        Animation lAnimWithBands = new Animation(MainApplicationConfig.gAnimationStepTime, lBands); 
	        lAnimWithBands.setPlayMode(Animation.PlayMode.LOOP);
			ChildTileAnimated lTileAnimed = new ChildTileAnimated(lAnimWithBands, Math.round(getX() + (CityMapRenderer.gTileWidthPx/2)*x), Math.round(getY()));
			
			// Par defaut l'animation n'est pas visible. Une Action viendra plus tard la rendre visible.
			lTileAnimed.setVisible(false);
			
			// On calcul son depth :
			if ( x < lNbrBands/2) {
				lTileAnimed.mDepth += (CityMapRenderer.gTileHeightPx/2) * ((lNbrBands/2) - x - 1);
			} else {
				lTileAnimed.mDepth += (CityMapRenderer.gTileHeightPx/2) * (x - (lNbrBands/2));
			}
			
			// Et on l'ajoute aux enfants du CityActorGroup :
			mAllChildren.add(lTileAnimed);
		}
	}
	
	/**
	 * Permet de placer par exemple une statue sur un temple.
	 * Transforme la texture de plusieurs tuiles de coté passée en argument, en tuiles enfants de taille 1, 
	 * découpées avec les mêmes bandes du découpage de la texture de fond.
	 * Cela permet de gérer le correctement le depth d'un élément de plusieurs tuiles de coté que l'on 
	 * superpose à un bâtiment.
	 * /!\ mTextureRegion doit être définis avant l'appel de cette méthode.
	 * 
	 * @param pTextRegAdded L'image à superposer.
	 * @param pOffsetX Le décalage, en pixels, sur l'axe X de l'image à superposer.
	 * @param pOffsetY Le décalage, en pixels, sur l'axe Y de l'image à superposer.
	 * @param pTextRegBackground L'image sur laquelle on superpose.
	 */
	public void addTilesFromTextureExtended(TextureRegion pTextRegAdded, int pOffsetX, int pOffsetY, TextureRegion pTextRegBackground) {
		// Nombre de tranches que l'on aura sur ce bâtiment :
		int lNbrBands = (pTextRegBackground.getRegionWidth()+2) / (CityMapRenderer.gTileWidthPx/2);
		
		// Pour toutes les tranches :
		for (int x = 0; x < lNbrBands; x++) {
			// On découpe :
			int lOffset = 0;
			if (x == lNbrBands-1) {
				lOffset = -2;
			}
			TextureRegion lTextReg = new TextureRegion(pTextRegAdded);
			lTextReg.setRegion((CityMapRenderer.gTileWidthPx/2)*x - pOffsetX, -pOffsetY, CityMapRenderer.gTileWidthPx/2 + lOffset, pTextRegBackground.getRegionHeight()); 
			
			// On créer une nouvelle tuile :
			ChildTile lTile = new ChildTile(lTextReg, Math.round(getX() + (CityMapRenderer.gTileWidthPx/2)*x) - 2*CityMapRenderer.gTileWidthPx, Math.round(getY()), this);
			
			// On calcul son depth :
			if ( x < lNbrBands/2) {
				lTile.mDepth += (CityMapRenderer.gTileHeightPx/2) * ((lNbrBands/2) - x - 1) + (CityMapRenderer.gTileHeightPx/2);
			} else {
				lTile.mDepth += (CityMapRenderer.gTileHeightPx/2) * (x - (lNbrBands/2)) - (CityMapRenderer.gTileHeightPx/2);
			}
			
			// On l'ajout au tableau des enfants :
			mAllChildren.add(lTile);
		}
	}
}
