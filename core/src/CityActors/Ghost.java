package CityActors;

import CityEngine.CameraManager;
import CityEngine.CityMapRenderer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * Groupe d'ombres qui indique où l'on va construire quelque chose.
 * /!\ Attention ce CityActor ne peut pas être appellé dans une sauvegarde XML,
 *     car les CityActors d'une sauvegarde ne doivent obligatoirement prendre en 
 *     arguments que deux int, ce qui n'est pas le cas ici. 
 */

public class Ghost extends CityActorGroup {
	
	/** Type de ce qui sera créé. */
	private Class<?> mBuildType;
	
	/** Types d'ombre existants. */
	public enum eGhostChildType { 
		Building, Street, Barrage
	}
	
	/** Type des ombres affichée. */
	private eGhostChildType mGhostChildType;
	
	/**
	 * Constructeur.
	 */
	public Ghost() {
		super(0, 0);
		setName("Groupe d'ombres qui indique où l'on va construire");
		setDescription("Le groupe d'ombres qui indique où l'on va construire.");
	}
	
	/**
	 * Fonction privée ajoutant récursivement des cases représentant la surface au sol d'un batiment.
	 * Utilisée par addChild(int pWidthTile, int pHeightTile)
	 */	
	private void recursiveAddChild(int pWidthTile, int pCurrentWidthTile, int pHeightTile , int pCurrentHeightTile){
		// On colle la case actuelle et on lance une ligne
		switch (mGhostChildType) {
			case Building: 
				mAllChildren.add(new GhostChildTileBuilding(pCurrentWidthTile*CityMapRenderer.gTileWidthPx/2, pCurrentHeightTile*CityMapRenderer.gTileHeightPx/2)); break;
			case Street: 
				mAllChildren.add(new GhostChildTileStreet(pCurrentWidthTile*CityMapRenderer.gTileWidthPx/2, pCurrentHeightTile*CityMapRenderer.gTileHeightPx/2)); break;
			default: 
				mAllChildren.add(new GhostChildTileBuilding(pCurrentWidthTile*CityMapRenderer.gTileWidthPx/2, pCurrentHeightTile*CityMapRenderer.gTileHeightPx/2));
		}
		
		if (pCurrentWidthTile < pWidthTile) {
			recursiveAddChild(pWidthTile, pCurrentWidthTile + 1, pCurrentHeightTile, pCurrentHeightTile + 1);
		}
		
		// On continue de monter si on le doit :
		if (pCurrentHeightTile < pHeightTile ) {
				recursiveAddChild(pWidthTile - 1, pCurrentWidthTile - 1, pHeightTile, pCurrentHeightTile + 1);
		}
	}
	
	/**
	 * Ajoute aux enfants de ce CityActorGroup les cases représentant la surface au sol d'un batiment.
	 */
	public void addChild(int pWidthTile, int pHeightTile) {
		recursiveAddChild(pWidthTile - 1, 0, pHeightTile - 1, 0);
	}
	
	/**
	 * Replace ce CityActor à la position de la souris en se collant sur la tuile de la map la plus proche.
	 */
	public void updatePosition() {
		Vector3 lMouseScreen = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0); 
		Vector3 lMouseScene = CameraManager.gCamera.unproject(lMouseScreen);
		Vector2 lMousePosIndex = CityMapRenderer.getTileIndexByPixel(Math.round(lMouseScene.x), Math.round(lMouseScene.y));
		
		// Décalage du à l'isométrie :
		if (lMousePosIndex.y % 2 == 0) {
			setX(lMousePosIndex.x*CityMapRenderer.gTileWidthPx);
		} else {
			setX(lMousePosIndex.x*CityMapRenderer.gTileWidthPx - CityMapRenderer.gTileWidthPx/2);
		}
		setY(lMousePosIndex.y*CityMapRenderer.gTileHeightPx/2);
	}
	
	/** 
	 * Définis le type de ce qui sera créé. 
	 */
	public void setBuildType(Class<?> pBuildType) {
		mBuildType = pBuildType;
	}
	
	/** 
	 * Retourne le type de ce qui sera créé. 
	 */
	public Class<?> getTargetType() {
		return mBuildType;
	}
	
	/** 
	 * Définis le type de l'ombre affichée. 
	 */
	public void setGhostChildType(eGhostChildType pGhostChildType) {
		mGhostChildType = pGhostChildType;
	}
	
	/** 
	 * Retourne vrai si toutes les ombres enfants sont valides. 
	 */
	public boolean isValid() {
		boolean lIsValid = true;
		for (CityActor lChild : getAllChildren()) {
			lIsValid = lIsValid && ((GhostChildTile) lChild).isValid();
		}
		return lIsValid;
	}
};