package CityActors;

import CityEngine.AIManager;
import CityEngine.CameraManager;
import CityEngine.CityMapRenderer;
import Screens.ScreenLoading;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * Ombre d'une tuile qui indique où l'on va construire quelque chose (un bâtiment, une route, un barage, etc...).
 * Gère si on affiche une ombre rouge ou verte selon la validité du sol (updatePositionAndColor()).
 * /!\ Attention ce CityActor ne peut pas être appellé dans une sauvegarde XML,
 *     car les CityActors d'une sauvegarde ne doivent obligatoirement prendre en 
 *     arguments que deux int, ce qui n'est pas le cas ici. 
 */

public class GhostChildTile extends CityActor {
	
	/** Décalage relatif à la position du groupe au quel appartient cette ombre. */
	private int mOffsetX, mOffsetY;
	
	/** La texture region rouge, indiquant que la tuile n'est pas constructible. */
	protected TextureRegion mTextureRegionRed;
	
	/** La texture region verte, indiquant que la tuile est constructible. */
	protected TextureRegion mTextureRegionGreen;
	
	/** Retourne vrai si la tuile est constructible. */
	private boolean mIsValid;
	
	/**
	 * Constructeur.
	 */
	public GhostChildTile(int pOffsetX, int pOffsetY) {
		super(0, 0);
		setName("Zone où sera construit quelque chose");
		mIsValid = false;
		
		// Positionnement :
		mOffsetX = pOffsetX;
		mOffsetY = pOffsetY;
		mDepth = 1;
		
		// Visuel :
		mTextureRegionRed = new TextureRegion(ScreenLoading.gAssetManager.get("buildings/ghost_building_red.png", Texture.class));
		mTextureRegionGreen = new TextureRegion(ScreenLoading.gAssetManager.get("buildings/ghost_building_green.png", Texture.class));
	}
	
	/**
	 * Mise à jour de la position de cette ombre pour qu'elle suive bien la souris tout 
	 * en se collant à la tuile de la map la plus proche, ainsi que de la bonne couleur
	 * à afficher (rouge == invalide, vert == valide).
	 */
	public void updatePositionColorAndValidity() {
		Vector3 lMouseScreen = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0); 
		Vector3 lMouseScene = CameraManager.gCamera.unproject(lMouseScreen);
		Vector2 lMousePosIndex = CityMapRenderer.getTileIndexByPixel(Math.round(lMouseScene.x), Math.round(lMouseScene.y));
		
		// Décalage du à l'isométrie :
		if (lMousePosIndex.y % 2 == 0) {
			setX(lMousePosIndex.x*CityMapRenderer.gTileWidthPx + mOffsetX);
		} else {
			setX(lMousePosIndex.x*CityMapRenderer.gTileWidthPx - CityMapRenderer.gTileWidthPx/2 + mOffsetX);
		}
		setY(lMousePosIndex.y*CityMapRenderer.gTileHeightPx/2 + mOffsetY);
		
		// Position en pixels du centre de la tuile :
		Vector2 lTileCenterPx = new Vector2(getX() + CityMapRenderer.gTileWidthPx/2, getY() + CityMapRenderer.gTileHeightPx/2);

		// Indices corresondants au centre de la tuile :
		Vector2 lTileCenterID = CityMapRenderer.getTileIndexByPixel(Math.round(lTileCenterPx.x), Math.round(lTileCenterPx.y));
		
		// Definition de la bonne texture à afficher :
		if (AIManager.gMapFloorsInfos.containsKey(lTileCenterID)) {
			if (AIManager.gMapFloorsInfos.get(lTileCenterID) != eFloorType.Building &&
				AIManager.gMapFloorsInfos.get(lTileCenterID) != eFloorType.Street) {
				mIsValid = true;
			} else {
				mIsValid = false;
			}
		} else {
			mIsValid = false;
		}
	}
	
	/** 
	 * Retourne la RegionTexture de cet élément. 
	 */
	public TextureRegion getTextureRegion() {
		// Mise à jour de la position, de la texture et de la validité :
		updatePositionColorAndValidity();
		
		// Retourne la bonne texture :
		if (mIsValid) {
			return mTextureRegionGreen;
		} else {
			return mTextureRegionRed;
		}
	}
	
	/** 
	 * Retourne vrai si la tuile est constructible. 
	 */
	public boolean isValid() {
		return mIsValid;
	}
}
