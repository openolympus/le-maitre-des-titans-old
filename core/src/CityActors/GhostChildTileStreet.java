package CityActors;

import Screens.ScreenLoading;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Ombre d'une tuile qui indique où l'on va construire une route.
 * /!\ Attention ce CityActor ne peut pas être appellé dans une sauvegarde XML,
 *     car les CityActors d'une sauvegarde ne doivent obligatoirement prendre en 
 *     arguments que deux int, ce qui n'est pas le cas ici. 
 */

public class GhostChildTileStreet extends GhostChildTile {
	
	/**
	 * Constructeur.
	 */
	public GhostChildTileStreet(int pOffsetX, int pOffsetY) {
		super(pOffsetX, pOffsetY);
		setName("Zone où sera construit une route");
		
		// Visuel :
		mTextureRegionRed = new TextureRegion(ScreenLoading.gAssetManager.get("buildings/ghost_street_red.png", Texture.class));
		mTextureRegionGreen = new TextureRegion(ScreenLoading.gAssetManager.get("buildings/ghost_street_green.png", Texture.class));
	}
}

