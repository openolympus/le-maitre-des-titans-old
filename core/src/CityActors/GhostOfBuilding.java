package CityActors;

import CityEngine.CameraManager;
import CityEngine.CityMapRenderer;
import Screens.ScreenLoading;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * Groupe d'ombres rouge qui indique où l'on va construire un bâtiment.
 * /!\ Attention ce CityActor ne peut pas être appellé dans une sauvegarde XML,
 *     car les CityActors d'une sauvegarde ne doivent obligatoirement prendre en 
 *     arguments que deux int, ce qui n'est pas le cas ici. 
 */

public class GhostOfBuilding extends CityActorGroup {
	
	/** Type du bâtiment qui sera créé. */
	private Class<?> mBuildingType;
	
	/**
	 * Constructeur.
	 */
	public GhostOfBuilding() {
		super(0, 0);
		setName("Silouette d'un bâtiment");
		setDescription("La silouette d'un bâtiment sur le sol.");
		
		// Visuel :
		mTextureRegion = new TextureRegion(ScreenLoading.gAssetManager.get("buildings/ghost_red_of_building.png", Texture.class));
	}
	
	/**
	 * Fonction privée ajoutant récursivement les cases des ombres d'un batiment.
	 * Utilisée par addChild(int pWidthTile, int pHeightTile)
	 */	
	private void recursiveAddChild(int pWidthTile, int pCurrentWidthTile, int pHeightTile , int pCurrentHeightTile){
		// On colle la case actuelle et on lance une ligne
		mAllChildren.add(new GhostTile(pCurrentWidthTile * CityMapRenderer.gTileWidthPx / 2, pCurrentHeightTile*CityMapRenderer.gTileHeightPx / 2));
		if (pCurrentWidthTile < pWidthTile) {
			recursiveAddChild(pWidthTile, pCurrentWidthTile + 1, pCurrentHeightTile, pCurrentHeightTile + 1);
		}
		
		// On continue de monter si on le doit :
		if (pCurrentHeightTile < pHeightTile ) {
				recursiveAddChild(pWidthTile - 1, pCurrentWidthTile - 1, pHeightTile, pCurrentHeightTile + 1);
		}
	}
	
	/**
	 * Ajoute le nombre demandé de tuiles enfants ombres aux enfants de ce CityActorGroup.
	 */
	public void addChild(int pWidthTile, int pHeightTile) {
		recursiveAddChild(pWidthTile - 1, 0, pHeightTile - 1, 0);
	}
	
	
	/**
	 * Replace ce CityActor à la position de la souris et en se collant sur la tuile de la map la plus proche.
	 */
	public void updatePosition() {
		Vector3 lMouseScreen = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0); 
		Vector3 lMouseScene = CameraManager.gCamera.unproject(lMouseScreen);
		Vector2 lMousePosIndex = CityMapRenderer.getTileIndexByPixel(Math.round(lMouseScene.x), Math.round(lMouseScene.y));
		
		// Décalage du à l'isométrie :
		if (lMousePosIndex.y % 2 == 0) {
			setX(lMousePosIndex.x*CityMapRenderer.gTileWidthPx);
		} else {
			setX(lMousePosIndex.x*CityMapRenderer.gTileWidthPx - CityMapRenderer.gTileWidthPx/2);
		}
		setY(lMousePosIndex.y*CityMapRenderer.gTileHeightPx/2);
	}
	
	/** 
	 * Définis le type du bâtiment qui sera créé. 
	 */
	public void setBuildingType(Class<?> pBuildingType) {
		mBuildingType = pBuildingType;
	}
	
	/** 
	 * Retourne le type du bâtiment qui sera créé. 
	 */
	public Class<?> getBuildingType() {
		return mBuildingType;
	}
};