package CityActors;

import CityEngine.AIManager;
import CityEngine.CameraManager;
import CityEngine.CityMapRenderer;
import Screens.ScreenLoading;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * Ombres qui indique où l'on va construire un bâtiment.
 * /!\ Attention ce CityActor ne peut pas être appellé dans une sauvegarde XML,
 *     car les CityActors d'une sauvegarde ne doivent obligatoirement prendre en 
 *     arguments que deux int, ce qui n'est pas le cas ici. 
 * La particularité de ce CityActor est que l'on ne l'aoute pas aux graphes de scène.
 */

public class GhostTile extends CityActor {
	
	/** Décalage relatif à la position du groupe au quel appartient cette ombre. */
	private int mOffsetX, mOffsetY;
	
	/** La texture region rouge, indiquant que la tuile n'est pas constructible. */
	protected TextureRegion mTextureRegionRed;
	
	/** La texture region verte, indiquant que la tuile est constructible. */
	protected TextureRegion mTextureRegionGreen;
	
	/** Retourne vrai si la tuile est constructible. */
	private boolean mIsGreen;
	
	/**
	 * Constructeur.
	 */
	public GhostTile(int pOffsetX, int pOffsetY) {
		super(0, 0);
		setName("Zone où sera construit un bâtiment");
		mIsGreen = false;
		
		// Positionnement :
		mOffsetX = pOffsetX;
		mOffsetY = pOffsetY;
		mDepth = 1;
		
		// Visuel :
		mTextureRegionRed = new TextureRegion(ScreenLoading.gAssetManager.get("buildings/ghost_red_of_building.png", Texture.class));
		mTextureRegionGreen = new TextureRegion(ScreenLoading.gAssetManager.get("buildings/ghost_green_of_building.png", Texture.class));
	}
	
	/**
	 * Mise à jour de la position de cette ombre pour qu'elle suive bien la souris tout 
	 * en se collant à la tuile de la map la plus proche, ainsi que de la bonne couleur
	 * à afficher (rouge == invalide, vert == valide).
	 */
	public void updatePositionAndColor() {
		Vector3 lMouseScreen = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0); 
		Vector3 lMouseScene = CameraManager.gCamera.unproject(lMouseScreen);
		Vector2 lMousePosIndex = CityMapRenderer.getTileIndexByPixel(Math.round(lMouseScene.x), Math.round(lMouseScene.y));
		
		// Décalage du à l'isométrie :
		if (lMousePosIndex.y % 2 == 0) {
			setX(lMousePosIndex.x*CityMapRenderer.gTileWidthPx + mOffsetX);
		} else {
			setX(lMousePosIndex.x*CityMapRenderer.gTileWidthPx - CityMapRenderer.gTileWidthPx/2 + mOffsetX);
		}
		setY(lMousePosIndex.y*CityMapRenderer.gTileHeightPx/2 + mOffsetY);
		
		// Position en pixels du centre de la tuile :
		Vector2 lTileCenterPx = new Vector2(getX() + CityMapRenderer.gTileWidthPx/2, getY() + CityMapRenderer.gTileHeightPx/2);

		// Indices corresondants au centre de la tuile :
		Vector2 lTileCenterID = CityMapRenderer.getTileIndexByPixel(Math.round(lTileCenterPx.x), Math.round(lTileCenterPx.y));
		
		// Definition de la bonne texture à afficher :
		if (AIManager.gMapFloorsInfos.containsKey(lTileCenterID)) {
			if (AIManager.gMapFloorsInfos.get(lTileCenterID) != eFloorType.Building &&
				AIManager.gMapFloorsInfos.get(lTileCenterID) != eFloorType.Street) {
				mIsGreen = true;
			} else {
				mIsGreen = false;
			}
		} else {
			mIsGreen = false;
		}
	}
	
	/** 
	 * Retourne vrai si la tuile est constructible. 
	 */
	public boolean isGreen() {
		return mIsGreen;
	}
	
	/** 
	 * Retourne la RegionTexture de cet élément. 
	 */
	public TextureRegion getTextureRegion() {
		if (mIsGreen) {
			return mTextureRegionGreen;
		} else {
			return mTextureRegionRed;
		}
	}
}
