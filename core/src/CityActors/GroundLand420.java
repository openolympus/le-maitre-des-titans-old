package CityActors;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Tuile d'une route.
 */

public class GroundLand420 extends CityActorGroundStreet {
	
	/**
	 * Constructeur.
	 */
	public GroundLand420(int pXPx, int pYPx) {
		super(pXPx, pYPx);
		setName("Route");
		setDescription("Une route");
		
		// Visuel :
		mTextureRegion = new TextureRegion(mGroundAtlas.findRegion("Zeus_Land3", 420));
		
		// Ajout de ce CityActor à son graphe de scène :
		addToGraph(this);
	}
}