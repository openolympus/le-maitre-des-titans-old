package CityActors;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Tuile d'une zone forestière.
 */

public class GroundTree003 extends CityActorGroundForest {
	
	/**
	 * Constructeur.
	 */
	public GroundTree003(int pXPx, int pYPx) {
		super(pXPx, pYPx);
		setName("Zone forestière");
		setDescription("Une zone forestière blablabla ...");
		
		// Visuel :
		mTextureRegion = new TextureRegion(mGroundAtlas.findRegion("Zeus_Trees", 3));
		
		// Ajout de ce CityActor à son graphe de scène :
		addToGraph(this);
	}
}
