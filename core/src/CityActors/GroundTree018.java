package CityActors;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Tuile d'une zone forestière.
 */

public class GroundTree018 extends CityActorGroundForest {
	
	/**
	 * Constructeur.
	 */
	public GroundTree018(int pXPx, int pYPx) {
		super(pXPx, pYPx);
		setName("Zone forestière");
		setDescription("Une zone forestière blablabla ...");
		
		// Visuel :
		mTextureRegion = new TextureRegion(mGroundAtlas.findRegion("Zeus_Trees", 18));
		
		// Ajout de ce CityActor à son graphe de scène :
		addToGraph(this);
	}
}