package CityEngine;

import java.util.HashMap;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Pool;

import CityActions.AddCharacEvery;
import CityActors.ChildTile;
import CityActors.BuildingTheatre;
import CityActors.BuildingTheatreSchool;
import CityActors.CharacterActor;
import CityActors.CityActor;
import CityActors.CityActor.eFloorType;
import CityActors.CityActorBuilding;

/**
 * Gestionnaire de tous les évenements, actions, et déplacements.
 */

public class AIManager {
	
	/** La réserve de personnages acteurs. */
	public static Pool<CharacterActor> gPoolCharacterActors;
	
	/** Chercheur du chemin le plus court entre deux points. */
	public static PathFinder gPathFinder;
	
	/** Map d'informations sur les sols de la map. Permet par exemple de tester rapidement la validiter 
	 * des tuiles pour la recherche de chemins les plus courts. */
	public static HashMap<Vector2, eFloorType> gMapFloorsInfos;
	
	/**
	 * Constructeur.
	 */
	public AIManager() {
		gPoolCharacterActors = new Pool<CharacterActor>() {
		    protected CharacterActor newObject() {
		    	// Création par defaut de ce personnage :
		        return new CharacterActor(0, 0);
		    }
		};
		gPathFinder = new PathFinder();
		gMapFloorsInfos = new HashMap<Vector2, eFloorType>();
	}
	
	/**
	 * Ajoute un CityActor au fonctionnement de la partie.
	 */
	public void addToCityProcess(CityActor pCityActor) {
		// Si le CityActor est une tuile issue d'un découpage :
		if (ChildTile.class.isAssignableFrom(pCityActor.getClass())) {
			// Le bâtiment père de cette tuile :
			CityActorBuilding lFather = (CityActorBuilding) ((ChildTile) pCityActor).getFather();
			
			// Si le père de cette tuile enfant est une école de théatre :
			if (BuildingTheatreSchool.class.isAssignableFrom(lFather.getClass())) {
				// On affiche l'animation de l'école de théatre :
				lFather.setAnimationVisible(true);
				
				// On cherche le théatre le plus proche de l'école :
				CityActorBuilding lBuildingNearest = getNearestFrom(BuildingTheatre.class, lFather.getEnterExit()); 
				
				// Si le théatre le plus proche a été trouvée :
				if (lBuildingNearest != null) {
					// Action qui crée devant l'école de théatre à intervals réguliers, un personnage acteur qui ira animer le théatre :
					AddCharacEvery lActionAddEvery = Actions.action(AddCharacEvery.class);
					lActionAddEvery.setTarget(pCityActor); 						// Cet Acteur créera
					lActionAddEvery.setDelay(3.0f);								// toutes les x secondes
					lActionAddEvery.setCharacType(CharacterActor.class);		// un personnage de ce type
					lActionAddEvery.setStart(lFather.getEnterExit());			// le placera à ce point de départ
					lActionAddEvery.setEnd(lBuildingNearest.getEnterExit());	// l'envera à ce point d'arrivé
					lActionAddEvery.setBuildingToBeAnimated(lBuildingNearest);	// ce qui occasionnera l'activation de l'animation de ce bâtiment.
					lActionAddEvery.setCharacOffset(new Vector2(-29, -10));     // L'image du personnage étant décalée de ce décalage.
					
					// Affectation de l'Action à l'école de théatre :
					pCityActor.addAction(lActionAddEvery);
				}
			}
		}
	}
	
	/**
	 * Retourne le bâtiment de type pBuildingType le plus près de la position pStart.
	 */
	public CityActorBuilding getNearestFrom(Class<?> pBuildingType, Vector2 pStart) {
		CityActorBuilding lBuildingNearest = null;
		float lDistNearest = 10000000.0f;
		// Pour tout les CityActors du graphe des batiments :
		for (Actor lActor : SceneGraphManager.gStageBuildings.getActors()) {
			// Si c'est une tuile enfant :
			if (ChildTile.class.isAssignableFrom(lActor.getClass())) {
				// Si cette tuile enfant est issue d'un CityActor du type de batiment que l'on recherche :
				if (pBuildingType.isAssignableFrom(((ChildTile) lActor).getFather().getClass())) {
					// Calcul de la distance entre le point de départ et l'entré/sortie du bâtiment père de cette tuile :
					float lDist = pStart.dst(((CityActorBuilding) ((ChildTile) lActor).getFather()).getEnterExit());
					
					// Si cette entré/sortie est la plus proche :
					if (lDist < lDistNearest) {
						lDistNearest = lDist;
						lBuildingNearest = (CityActorBuilding) ((ChildTile) lActor).getFather();
					}
				}
			}
		}
		return lBuildingNearest;
	}
}
