package CityEngine;

import ApplicationConfig.MainApplicationConfig;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;

/**
 * Gestionnaire de la caméra.
 * Permet de gérer les métaphores de déplacements et de zoom de la caméra.
 * 
 * 
 * Note importante :
 * 
 * Il y a deux manières de détecter les inputs :
 * 	1)  Mettre dans une boucle de rendu :
 * 		if ( Gdx.input.isKeyPressed(Keys.LEFT) ) {
			- traitement -
		}
		Cela va vérifier à chaque frame si le clic gauche est pressé. 
	
	2)	Utiliser les méthodes de l'InputProcessor :
		public boolean keyDown(int keycode) {
			switch(keycode) {
				case Keys.LEFT:
					- traitement -
					break;
			}
		}
		Cela permet de signaler si un clic gauche à été fait, une fois.
		
	Différence majeur entre les deux :
	- Avec Gdx.input.isKeyPressed(Keys.LEFT) on detecte en continue.
	- keyDown(int keycode) signal si est déclenché, une seule fois. Ensuite il faut relacher la touche et la réapuyer.
 */

public class CameraManager implements InputProcessor {
	
	/** La caméra. */
	public static OrthographicCamera gCamera;
	
	/** Coordonnées des coins de l'écran, dans l'espace écran. */
	private Vector3 mLeftTopCornerScreen, mRightBottomCornerScreen;
	
	/** Coordonnées des coins de l'écran, dans l'espace scène. */
	public static Vector3 gLeftTopCornerScene, gRightBottomCornerScene;
	
	/**
	 * Constructeur.
	 */
	public CameraManager() {
		gCamera = new OrthographicCamera();
        gCamera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
	}

	/**
	 * Gestion des déplacements de la caméra.
	 */
	public void render() { 
		boolean lKeyBoardUsed = false;
		
		// Prochain déplacement par flèches :
		Vector3 lNextDisplacement = new Vector3(0, 0, 0);
		if (Gdx.input.isKeyPressed(Keys.LEFT)) {
			lNextDisplacement.add(-MainApplicationConfig.gPanSpeed, 0, 0);
			lKeyBoardUsed = true;
		}
		if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
			lNextDisplacement.add(MainApplicationConfig.gPanSpeed, 0, 0);
			lKeyBoardUsed = true;
		}
		if (Gdx.input.isKeyPressed(Keys.UP)) {
			lNextDisplacement.add(0, MainApplicationConfig.gPanSpeed, 0);
			lKeyBoardUsed = true;
		}
		if (Gdx.input.isKeyPressed(Keys.DOWN)) {
			lNextDisplacement.add(0, -MainApplicationConfig.gPanSpeed, 0);
			lKeyBoardUsed = true;
		} 
		
		// Prochain déplacement par la souris :
		if ((Gdx.input.getX() >= Gdx.graphics.getWidth() - MainApplicationConfig.gBordersMarge ||        // 1er octant en diagonal
			(Gdx.input.getY() < MainApplicationConfig.gBordersMarge) || 					       	     // 2eme octant en diagonal
			(Gdx.input.getX() < MainApplicationConfig.gBordersMarge) || 					        	 // 3eme octant en diagonal
			(Gdx.input.getY() >= Gdx.graphics.getHeight() - MainApplicationConfig.gBordersMarge))) {     // 4eme octant en diagonal
			if (!lKeyBoardUsed) {
				lNextDisplacement.set(((Gdx.input.getX()-(Gdx.graphics.getWidth()/2.0f))/(Gdx.graphics.getWidth()/2.0f))*MainApplicationConfig.gPanSpeed, 
						              (((Gdx.graphics.getHeight()/2.0f)-Gdx.input.getY())/(Gdx.graphics.getHeight()/2.0f))*MainApplicationConfig.gPanSpeed, 
						              0);
			}
		} 
		
		// On applique le déplacement :
		gCamera.translate(lNextDisplacement);
		
		// Coordonnées des coins de la zone de vue, dans l'espace écran :
		mLeftTopCornerScreen = new Vector3(0, 0, 0); 
		mRightBottomCornerScreen = new Vector3(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 0);
		
		// Coordonnées des coins de l'écran, dans l'espace scène :
		gLeftTopCornerScene = gCamera.unproject(mLeftTopCornerScreen);
		gRightBottomCornerScene = gCamera.unproject(mRightBottomCornerScreen);
	}
	
	/**
	 * Mise à jour de la caméra.
	 */
	public void resize(int pWidth, int pHeight) {
		gCamera.viewportWidth = pWidth;
		gCamera.viewportHeight = pHeight;
		gCamera.update();
	}
	
	/**
	 * Gestion des touches appuyées.
	 */
	public boolean keyDown(int keycode) {
		// La touche entrée replace le zoom à son niveau par defaut :
		if (keycode == Keys.ENTER) {
			resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
			MainApplicationConfig.gPanSpeed=MainApplicationConfig.gDefaultPanSpeed + 1;
		}
		return false;
	}

	/**
	 * Gestion des touches relachées.
	 */
	public boolean keyUp(int keycode) {
		return false;
	}

	/**
	 * Si la touche est actionnée.
	 */
	public boolean keyTyped(char character) {
		return false;
	}

	/**
	 * Gestion des touches appuyées sur l'écran.
	 */
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	/**
	 * Gestion des touches relachées sur l'écran.
	 */
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	/**
	 * Gestion des drags sur l'écran.
	 */
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	/**
	 * Gestion des déplacements de la souris sur l'écran.
	 */
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	/**
	 * Gestion du zoom avec la roulette de la souris.
	 * @param pAmount 
	 * -1 : zoom
	 * +1 : zoom
	 */
	public boolean scrolled(int pAmount) {
		// Prochaine zone de vue de la caméra :
		float lNextViewportWidth=gCamera.viewportWidth;
		float lNextViewportHeight=gCamera.viewportHeight;
		
		//Disjonction de cas zoom et dézoom, au cas on l'on veut repasser à une évolution en % et non en linéaire.
		if(pAmount == -1 && gCamera.viewportWidth > Gdx.graphics.getWidth() / 5 && gCamera.viewportHeight > Gdx.graphics.getHeight() / 5){
			//Zoom
			MainApplicationConfig.gPanSpeed = (int) (MainApplicationConfig.gPanSpeed - (MainApplicationConfig.gDefaultPanSpeed * MainApplicationConfig.gZoomSpeed));
			lNextViewportWidth = gCamera.viewportWidth - Math.round(MainApplicationConfig.gZoomSpeed * Gdx.graphics.getWidth());
			lNextViewportHeight = gCamera.viewportHeight - Math.round(MainApplicationConfig.gZoomSpeed * Gdx.graphics.getHeight());
		}
		
		if(pAmount==1 && gCamera.viewportWidth < Gdx.graphics.getWidth() * 3 && gCamera.viewportHeight < Gdx.graphics.getHeight() * 3){
			//Dézoom
			MainApplicationConfig.gPanSpeed = (int) (MainApplicationConfig.gPanSpeed + (MainApplicationConfig.gDefaultPanSpeed * MainApplicationConfig.gZoomSpeed));
			lNextViewportWidth = gCamera.viewportWidth + Math.round(MainApplicationConfig.gZoomSpeed * Gdx.graphics.getWidth());
			lNextViewportHeight = gCamera.viewportHeight + Math.round(MainApplicationConfig.gZoomSpeed * Gdx.graphics.getHeight());
		}		
			
		if (lNextViewportHeight != gCamera.viewportHeight) {
			// Coordonnées de la souris dans l'espace écran :
			Vector3 lMouseScreen = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0); 
				
			// Coordonnées de la souris dans l'espace scène :
			Vector3 lMouseScene = gCamera.unproject(lMouseScreen);
				
			// Coordonnées du centre de l'écran dans l'espace écran :
			Vector3 lCenterScreen = new Vector3(Gdx.graphics.getWidth()/2.0f, Gdx.graphics.getHeight()/2.0f, 0.0f);
				
			// Coordonnées du centre de l'écran dans l'espace scène :
			Vector3 lCenterScene = gCamera.unproject(lCenterScreen);
				
			// Distance entre le centre et la souris en coordonnées scène : 
			Vector3 lDiffMouseCenterScene = lMouseScene.sub(lCenterScene);
				
			// Vecteur qui permet d'ajuster afin de conserver la même place de la souris sur la carte
			Vector3 lDiffSecond = new Vector3(lDiffMouseCenterScene.x + lDiffMouseCenterScene.x*(pAmount * MainApplicationConfig.gZoomSpeed),
											  lDiffMouseCenterScene.y + lDiffMouseCenterScene.y*(pAmount * MainApplicationConfig.gZoomSpeed), 
											  lDiffMouseCenterScene.z + lDiffMouseCenterScene.z*(pAmount * MainApplicationConfig.gZoomSpeed));
				
			gCamera.translate(lDiffMouseCenterScene.sub(lDiffSecond));
			resize((int)lNextViewportWidth, (int)lNextViewportHeight);
		}
		return false;
	}
	
	/**
	 * Destructeur.
	 */
	public void dispose() {
	}
}