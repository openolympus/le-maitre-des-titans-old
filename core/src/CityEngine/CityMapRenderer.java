package CityEngine;

import CityActors.CityActor;
import CityActors.Ghost;
import Screens.ScreenLoading;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Gestionnaire de l'affichage de la carte d'une cité.
 * Se charge du rendu à l'écran.
 */

public class CityMapRenderer {
	
	/** Largeur et hauteur de la map en index. */
	public static int gMapWidthId, gMapHeightId;
	
	/** Largeur et hauteur d'une tuile en pixels. */
	public static int gTileWidthPx, gTileHeightPx;
	
	/** Typographies. */
	protected BitmapFont mFontWhite; 
	
	/** L'ombre rouge qui indique où l'on va construire un bâtiment. */
	public static Ghost gGhost;
	
	/**
	 * Constructeur.
	 */
	public CityMapRenderer() {
		gTileWidthPx = 60;
		gTileHeightPx = 30;
		gMapWidthId = 1;
		gMapHeightId = 1;
		
		// Création de l'ombre des bâtiments que l'on construira :
		gGhost = new Ghost();
		gGhost.setVisible(false);
		
		// Pour debug, typo pour écrire les depth des tuiles sur la carte :
		mFontWhite = ScreenLoading.gAssetManager.get("fonts/zeusBlanc.fnt", BitmapFont.class);
	}
	
	/**
	 * Boucle de rendu.
	 */
	public void render() {
		// Mise à jour de la camera :
		Camera camera = SceneGraphManager.gStageGrounds.getViewport().getCamera();
		camera.update();

		// Affichage des CityActors :
		SceneGraphManager.gStageGrounds.getBatch().setProjectionMatrix(CameraManager.gCamera.combined);
		SceneGraphManager.gStageGrounds.getBatch().begin();
		for (Actor lActor : SceneGraphManager.gStageInView.getActors()) {
			SceneGraphManager.gStageGrounds.getBatch().draw(((CityActor) lActor).getTextureRegion(), lActor.getX(), lActor.getY());
		}
		
		// Affichage (ou non) de l'ombre de ce que l'on souhaite construire :
		if (gGhost.isVisible()) {
			for (CityActor lChild : gGhost.getAllChildren()) {
				SceneGraphManager.gStageGrounds.getBatch().draw(lChild.getTextureRegion(), lChild.getX(), lChild.getY());
			}
		}
		
		// Pour debug, affichage du FPS :
		mFontWhite.draw(SceneGraphManager.gStageGrounds.getBatch(), "FPS : " + Gdx.graphics.getFramesPerSecond(), 0, -10);
		
		// Pour debug, affichage de la position de la souris en pixel écran et en index tuile.
		Vector3 lMouseScreen = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0); 
		Vector3 lMouseScene = CameraManager.gCamera.unproject(lMouseScreen);
		Vector2 lMousePosIndex = getTileIndexByPixel(Math.round(lMouseScene.x), Math.round(lMouseScene.y));
		mFontWhite.draw(SceneGraphManager.gStageGrounds.getBatch(),  Math.round(lMouseScene.x) + "," + Math.round(lMouseScene.y), lMouseScene.x, lMouseScene.y + 15);
		mFontWhite.draw(SceneGraphManager.gStageGrounds.getBatch(), Math.round(lMousePosIndex.x) + "," + Math.round(lMousePosIndex.y), lMouseScene.x, lMouseScene.y + 30);
		SceneGraphManager.gStageGrounds.getBatch().end();
	}
	
	/**
	 * Convertis la position en pixel d'une tile en position index.
	 * Fonction valable uniquement pour une map ayant des tiles a coordonnees positives.
	 * @param pVecX x de la tile en pixel dans le repère de la map.
	 * @param pVecY y de la tile en pixel dans le repère de la map.
	 */
	public static Vector2 getTileIndexByPixel(int pVecX, int pVecY) {
		// Résolution analytique pur avec 2 fonctions affines type y=ax+b et y=-ax+d :
		int lB,lD;
		int lX, lY;
		
		// Adaptation au fait que la tile -0.0- est centré en (0,0) :
		pVecY = pVecY - gTileHeightPx/2;
		
		// Petite dichotomie en recursive :
		lD = findTheD(-2, gMapWidthId + gMapHeightId, pVecX, pVecY);
		lB = lD-2 * (lD - Math.round(pVecY / gTileHeightPx));
		
		// Correction si necessaire :
		while (pVecX * gTileHeightPx / gTileWidthPx + lB * gTileHeightPx < pVecY) {
			lB++;
		}
		
		// Calcul magique :
		lX = (lD-lB) / 2;
		lY = lD + lB;

		return new Vector2(lX, lY-1);
	}
	
	/**
	 * Fonction privée récursive de recherche dichotomique de l'indice de fonction ax+d de 
	 * la fonction getTileIndexByPixel.
	 * @param pBondaryMin
	 * @param pBondaryMax
	 * @param pVecX
	 * @param pVecY
	 */
	private static int findTheD(int pBondaryMin, int pBondaryMax, int pVecX, int pVecY) {
		int lNewBound = (pBondaryMin + pBondaryMax) / 2;
		if (pBondaryMax - pBondaryMin == 1) {
			return pBondaryMax;
		} else if (lNewBound * gTileHeightPx - pVecX * gTileHeightPx / gTileWidthPx >= pVecY) {
			return findTheD(pBondaryMin , lNewBound, pVecX , pVecY);
		} else {
			return findTheD(lNewBound , pBondaryMax , pVecX , pVecY);
		}
	}
	
	/**
	 * Convertis la position en index d'une tile en position pixel.
	 * 
	 * @param pVecX indexX de la tile en index de tile dans le repère de la map.
	 * @param pVecY indexY de la tile en index de tile dans le repère de la map.
	 */
	public Vector2 getTilePixelByIndex(int pVecX, int pVecY) {
		return new Vector2(pVecX * gTileWidthPx, pVecY * (gTileHeightPx/2));
	}
	
	/**
	 * Destructeur.
	 */
	public void dispose() {
	}
}
