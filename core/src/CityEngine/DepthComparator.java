package CityEngine;

import java.util.Comparator;

import com.badlogic.gdx.scenes.scene2d.Actor;

import CityActors.CityActor;

/**
 * Comparateur de CityMapActor. 
 * Permet d'ordonner deux éléments de type CityMapActor en fonction de leur depth.
 */

public class DepthComparator implements Comparator<Actor> {
	
	/**
	 * Constructeur.
	 */
	public DepthComparator() {
	}
	
	/**
	 * Ordonne deux sprites selon leur profondeur.
	 */
	public int compare(Actor obj1, Actor obj2) {
		if ( ((CityActor) obj1).getDepth() < ((CityActor) obj2).getDepth() ) {
			return 1;
		} else if ( ((CityActor) obj1).getDepth() == ((CityActor) obj2).getDepth() ) {
			return 0;
		} else {
			return -1;
		}
	}
}
