package CityEngine;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;

import CityActors.CityActor.eFloorType;

import com.badlogic.gdx.math.Vector2;

/**
 * Implémentation de l'algorithme A* pour la recherche de plus courts chemins.
 */

public class PathFinder {
	
	/** Représentation d'un noeud pour l'algo. */
	public class Node {
		// Le noeud où l'on était avant d'arriver sur ce noeud :
		public Vector2 mParentPos;
		
	    // Distance pour aller du départ au noeud actuel :
		public double mStartToThis;
		
		// Distance pour aller du noeud actuel à l'arrivée :
		public double mThisToEnd;
	    
	    // Somme des deux distances précédentes :
		public double mSumDist;   
		
		// Constructeur
		public Node(Vector2 pParentPos, double pStartToThis, double pThisToEnd) {
			mParentPos = pParentPos;
			mStartToThis = pStartToThis;
			mThisToEnd = pThisToEnd;
			mSumDist = mStartToThis + mThisToEnd;
		}
	}
	
	/** Point de départ du chemin. */
	private Vector2 mStart;
	
	/** Point d'arrivé du chemin. */
	private Vector2 mEnd;
	
	/** Map des noeuds déjà visités par l'algo. */
	private HashMap<Vector2, Node> mVisitedNodes;
	
	/** Map des noeuds constituants le plus court chemin. */
	private HashMap<Vector2, Node> mPathNodes;
	
	/** Liste des positions des noeuds constituants le plus court chemin, dans l'ordre du départ à l'arrivée. */
	private LinkedList<Vector2> mOrderedPositions;
	
	/**
	 * Constructeur.
	 */
	public PathFinder() {
		mVisitedNodes = new HashMap<Vector2, Node>();
		mPathNodes = new HashMap<Vector2, Node>();
		mOrderedPositions = new LinkedList<Vector2>();
	}
	
	/**
	 * Retourne le chemin le plus court entre le départ et l'arrivée, exprimés en pixels du repère scène.
	 */
	public LinkedList<Vector2> getPath(Vector2 pStart, Vector2 pEnd) {
		// Définis le départ et l'arrivée demandé :
		mStart = pStart;
		mEnd = pEnd;
		
		// Réinitialise les listes de noeuds :
		mVisitedNodes.clear();
		mPathNodes.clear();
		mOrderedPositions.clear();
		
		// Noeud courant :
		Node lCurrentNode = new Node(null, 0, getDist(mStart, mEnd));
		
		// Position du noeud courant :
		Vector2 lCurrentPosition = mStart;
		
		// Ajout du noeud de départ aux maps :
		mVisitedNodes.put(lCurrentPosition, lCurrentNode);
		mPathNodes.put(lCurrentPosition, lCurrentNode);
		
		// Ajoute à la map des noeuds visités, les noeuds valides et voisins :
		AddValidNeighbors(lCurrentPosition);
		
		// Tant que l'arrivée n'a pas été atteinte et qu'il reste des noeuds dans la map des noeuds visités :
		while (lCurrentPosition != mEnd && !mVisitedNodes.isEmpty()) {
			// Position du meilleur noeud de la map des noeuds visités :
			lCurrentPosition = getBestFromVisitedNodes();

			// Ajoute à la map des noeuds du chemin solution ce meilleur noeud :
			mPathNodes.put(lCurrentPosition, mVisitedNodes.get(lCurrentPosition));
			
			// On le supprime de la map des noeuds à visiter :
			mVisitedNodes.remove(lCurrentPosition);
			
			// Ajoute à la map des noeuds visités, les noeuds valides et voisins :
			AddValidNeighbors(lCurrentPosition);
			
			// A la dernière itération, on ajoute le noeud d'arrivée :
			if (lCurrentPosition == mEnd) {
				Node lEndNode = new Node(lCurrentPosition, mPathNodes.get(lCurrentPosition).mSumDist, 0);
				mPathNodes.put(mEnd, lEndNode);
			}
		}
		
		// On etourne la liste des positions trouvées, après avoir été mis dans le bon ordre pour le parcours :
		return getPathOrdered();
	}
	
	/**
	 * Ajoute à la map des noeuds visités, les noeuds voisins et valides à la position passée en argument.
	 */
	public void AddValidNeighbors(Vector2 pPosition) {
		// L'algo l'ira pas chercher plus loin que ces distances par defaut :
		Node lNeighbor = new Node(pPosition, 100000000, 100000000);
		
		// Pour tous les voisins :
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				if (i != 0 && j != 0) {
					// Position du voisin courant :
					Vector2 lPosition = new Vector2(pPosition.x + i*(CityMapRenderer.gTileWidthPx/2), pPosition.y + j*(CityMapRenderer.gTileHeightPx/2));
					
					// Si ce voisin est valide :
					if (isValid(lPosition)) {
						// Si ce voisin n'est pas déjà dans la map des noeuds du chemin solution :
						if (!mPathNodes.containsKey(lPosition)) {
							// Distance pour aller du départ au noeud courant + distance entre le noeud courant et ce voisin :
							lNeighbor.mStartToThis = mPathNodes.get(pPosition).mStartToThis + getDist(pPosition, lPosition);
							
							// Distance entre ce voisin et l'arrivée :
							lNeighbor.mThisToEnd = getDist(lPosition, mEnd);
							
							// Somme des deux précédents :
							lNeighbor.mSumDist = lNeighbor.mStartToThis + lNeighbor.mThisToEnd;
							
							// La position du parent de ce voisin est la position du noeud courant :
							lNeighbor.mParentPos = pPosition;
							
							// Si la map des noeuds déjà visités contient ce voisin :
							if (mVisitedNodes.containsKey(lPosition)) {
								// On compare les couts :
								if (lNeighbor.mSumDist < mVisitedNodes.get(lPosition).mSumDist) {
									mVisitedNodes.put(lPosition, lNeighbor);
								}
							} else {
								mVisitedNodes.put(lPosition, lNeighbor);
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * Retourne la position du noeud le moins couteux parmis les noeuds déjà visités.
	 */
	public Vector2 getBestFromVisitedNodes() {
		// Position du premier noeud de la map des noeuds visités :
		Vector2 lBestNodePosition = (Vector2) mVisitedNodes.keySet().toArray()[0];
				
		// Pour toutes les positions des noeuds de la map des noeuds visités :
		for (Vector2 lPosition : mVisitedNodes.keySet()) {
			if (mVisitedNodes.get(lPosition).mSumDist < mVisitedNodes.get(lBestNodePosition).mSumDist) {
				lBestNodePosition = lPosition;
			}
		}
		
		return lBestNodePosition;
	}
	
	/**
	 * Retourne le carré de la distance euclidienne entre le point pStart et le point pEnd.
	 */
	public double getDist(Vector2 pStart, Vector2 pEnd) {
        return (pEnd.x-pStart.x)*(pEnd.x-pStart.x) + (pEnd.y-pStart.y)*(pEnd.y-pStart.y);
	}
	
	/**
	 * Retourne vrai si la position donne sur une tuile de type Street.
	 */
	public boolean isValid(Vector2 pPosition) {
		Vector2 lTileID = CityMapRenderer.getTileIndexByPixel(Math.round(pPosition.x), Math.round(pPosition.y));
		
		// Si la position demandée est bien dans la map :
		if (AIManager.gMapFloorsInfos.containsKey(lTileID)) {
			// Si la tuile correspondante est de type Street :
			if (AIManager.gMapFloorsInfos.get(lTileID) == eFloorType.Street) {
				return true;
			} else {
				// La tuile n'est pas de type Street :
				return false;
			}
		} else {
			// La position demandée n'est pas dans la map :
			return false;
		}
	}
	
	/**
	 * Retourne la liste des positions des noeuds trouvés, mis dans le bon ordre pour le parcours. 
	 */
	public LinkedList<Vector2> getPathOrdered() {
		// Si l'arrivée à été atteinte :
		if (mPathNodes.containsKey(mEnd)) {
			// Noeud courant :
			Node lNode = mPathNodes.get(mEnd);
			
			// Position courante :
			Vector2 lPosition = mEnd;
			
			// Position du parent :
			Vector2 lParentPosition = lNode.mParentPos;
		
			// Ajout de l'arrivée :
			mOrderedPositions.add(mEnd);
			
			// On remonte les parents jusqu'à atteindre le départ :
			while (lParentPosition != mStart) {
				lPosition = lParentPosition;
				mOrderedPositions.add(lPosition);
				lNode = mPathNodes.get(lNode.mParentPos);
				lParentPosition = lNode.mParentPos;
			}
			
			// Inversion de la liste :
			Collections.reverse(mOrderedPositions);
		}
		return mOrderedPositions;
	}
}
