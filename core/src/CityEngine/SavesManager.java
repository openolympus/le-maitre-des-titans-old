package CityEngine;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;

/**
 * Gestionnaire de sauvegardes.
 * Permet de charger ou de sauvegarder une partie dans un fichier sur le disque.
 */

public class SavesManager {
	
	/**
	 * Constructeur.
	 */
	public SavesManager() {
		
	}
	
	/**
	 * Charge une sauvegarde.
	 * Créer tous les éléments contenus dans le fichier XML passé en paramètre.
	 *  
	 * @param pSavePath Fichier XML de la sauvegarde.
	 */
	public static void loadSave(String pSavePath) {
		// Chargement du fichier :
		FileHandle lXMLFile = Gdx.files.internal(pSavePath);
		String lXMLString = lXMLFile.readString();
		
		// Représentation du fichier XML contenant la sauvegarde de la partie.
		Element lSave = new XmlReader().parse(lXMLString);
		
		// Renseignement de la taille de la map et de ses tuiles :
		CityMapRenderer.gTileWidthPx = lSave.getChildByName("cityMapInfos").getChildByName("sizeTile").getInt("width", 0);
		CityMapRenderer.gTileHeightPx = lSave.getChildByName("cityMapInfos").getChildByName("sizeTile").getInt("height", 0);;
		CityMapRenderer.gMapWidthId = lSave.getChildByName("cityMapInfos").getChildByName("sizeMap").getInt("width", 0);
		CityMapRenderer.gMapHeightId = lSave.getChildByName("cityMapInfos").getChildByName("sizeMap").getInt("height", 0);
		
		// Parcours des lignes de l'élément cityActors :
		XmlReader.Element lGroundAndBuildings = lSave.getChildByName("cityActors");
		for (XmlReader.Element lElementRow : lGroundAndBuildings.getChildrenByName("row")) {
			// Calcul du décalage isométrique :
			int lIsoX = 0;
			if (lElementRow.getIntAttribute("y") % 2 == 0) {
				lIsoX = CityMapRenderer.gTileWidthPx/2;
			}
			
			// Parcours de tous les CityActors de la ligne :
			for (XmlReader.Element lElementCityActor : lElementRow.getChildrenByName("cityActor")) {
				// Position du futur cityActor :
				int lPosX = lIsoX + (lElementCityActor.getIntAttribute("x") * CityMapRenderer.gTileWidthPx) - CityMapRenderer.gTileWidthPx/2;
				int lPosY = lElementRow.getIntAttribute("y") * CityMapRenderer.gTileHeightPx/2;
				try {
					// Détection du type :
					Class<?> lClassDetected = Class.forName("CityActors." + lElementCityActor.getAttribute("type"));
					Constructor<?> lConstructor = lClassDetected.getConstructor(new Class[]{int.class, int.class});
					
					// Création du cityActor correspondant :
					lConstructor.newInstance(new Object [] {lPosX, lPosY});
				} catch (ClassNotFoundException e) {
					e.printStackTrace(); // La classe n'existe pas
				}
				catch (NoSuchMethodException e) {
					e.printStackTrace(); // La classe n'a pas le constructeur recherché
			    }
				catch (InstantiationException e) {
					e.printStackTrace(); // La classe est abstract ou est une interface ou n'a pas de constructeur accessible sans paramètre
			    }
				catch (IllegalAccessException e) {
					e.printStackTrace(); // La classe n'est pas accessible
			    }
			    catch (InvocationTargetException e) {
			    	e.printStackTrace(); // Exception déclenchée si le constructeur invoqué a lui-même déclenché une exception
			    }
				catch (IllegalArgumentException e) {
					e.printStackTrace(); // Mauvais type de paramètre (Pas obligatoire d'intercepter IllegalArgumentException)
			    }
			}
		}
		// Centre la caméra au milieu de la map :
		CameraManager.gCamera.position.set((CityMapRenderer.gMapWidthId*CityMapRenderer.gTileWidthPx)/2, (CityMapRenderer.gMapHeightId*CityMapRenderer.gTileHeightPx)/4, 0);
		CameraManager.gCamera.update();
	}
	
	public static void save(String pSavePath) {
		
	}
}
