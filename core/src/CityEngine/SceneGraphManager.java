package CityEngine;

import java.util.Comparator;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Sort;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

/**
 * Gestionnaire du graph de scène.
 * Arborescence contenant tout les éléments d'une partie.
 */

public class SceneGraphManager {

	/** Le graph de scène des éléments du sol. */
	public static Stage gStageGrounds; 
	
	/** Le graph de scène des bâtiments. */
	public static Stage gStageBuildings; 
	
	/** Le graph de scène des personnages.
	 * A noter que les marchandises sont considérées comme des personnages.
	 * Car de fait, au sens des caractéristiques, un bloc de marbre 
	 * est plus proche d'un personnage que d'un bâtiment. */
	public static Stage gStageCharacters; 
	
	/** Le graph de scène des effets visuels. */
	public static Stage gStageEffects; 
	
	/** Le graph de scène des CityActors qui sont dans la zone de vue.
	 * Ce graphe permet de ne faire le tris que des depths des Actors affichés, 
	 * et non ceux de toute la map. */
	public static Stage gStageInView; 
	
	/** Comparateur des depths des éléments sur la map. */
	private DepthComparator mComparator;
	
	/** Types des graphes de scène existants. 
	 * Chaque CityActor est rangé dans un graphe de scène : gStageGrounds, gStageBuildings, gStageCharacters ou gStageEffects. */
	public enum eGraphType { 
		Grounds, Buildings, Characters, Effects
	}
	
	/**
	 * Constructeur.
	 */
	public SceneGraphManager() {
		gStageGrounds = new Stage(new ScreenViewport(CameraManager.gCamera));
		gStageBuildings = new Stage(new ScreenViewport(CameraManager.gCamera), gStageGrounds.getBatch());
		gStageCharacters = new Stage(new ScreenViewport(CameraManager.gCamera), gStageGrounds.getBatch());
		gStageEffects = new Stage(new ScreenViewport(CameraManager.gCamera), gStageGrounds.getBatch());
		gStageInView = new Stage(new ScreenViewport(CameraManager.gCamera), gStageGrounds.getBatch());
		mComparator = new DepthComparator();
	}
	
	/**
	 * Boucle de rendu.
	 */
	public void render() {
		// Joue les actions de tous les CityActors pour la frame courante :
		gStageGrounds.act();
		gStageBuildings.act();
		gStageCharacters.act();
		gStageEffects.act();
		
		// Mise à jour du tableau des CityActors visiblent dans la zone de vue :
		updateActorsInView(gStageGrounds.getActors());
		updateActorsInView(gStageBuildings.getActors());
		updateActorsInView(gStageCharacters.getActors());
		updateActorsInView(gStageEffects.getActors());
		
		// Tris (en fonction de leur depth) des CityActors visiblent dans la zone de vue :
		Sort.instance().sort(gStageInView.getActors().items, (Comparator<Actor>) mComparator, 0, gStageInView.getActors().size);
	}
	
	/**
	 * Mets à jour le tableau des CityActors visiblent avec les Actors du tableau
	 * passé en argument.
	 * Y ajoute ceux qui apparaissent à l'écran, en retire ceux qui en ont disparu.
	 *  
	 * @param pActors Tableau d'éléments dont il faut vérifier la visibilité à l'écran.
	 */
	public void updateActorsInView(Array<Actor> pActors) {
		for (Actor lActor : pActors) {
			// Si l'Actor est visible (au sens de sa variable "visible") :
			if (lActor.isVisible()) {
				// Si l'Actor est dans la zone de vue :
				if (CameraManager.gLeftTopCornerScene.x - 100 < lActor.getX() && lActor.getX() < CameraManager.gRightBottomCornerScene.x &&
					CameraManager.gRightBottomCornerScene.y - 300 < lActor.getY() && lActor.getY() < CameraManager.gLeftTopCornerScene.y + 50) {
					// Si l'Actor n'est pas dans le tableau, alors on l'y ajoute :
					if (!gStageInView.getActors().contains(lActor, false)) {
						gStageInView.getActors().add(lActor);
					}
				} else {
					// Si l'Actor n'est pas dans la zone de vue et est dans le tableau, alors on l'en enlève.
					if (gStageInView.getActors().contains(lActor, false)) {
						gStageInView.getActors().removeValue(lActor, false);
					}
				}
			} else {
				// Si l'Actor n'est pas visible (au sens de sa variable "visible") et est dans le tableau, alors on l'en enlève.
				if (gStageInView.getActors().contains(lActor, false)) {
					gStageInView.getActors().removeValue(lActor, false);
				}
			}
		}
	}
	
	/**
	 * Gestion de la vue en cas de redimensionnement de l'écran.
	 */
	public void resize(int pWidth, int pHeight) {
		gStageGrounds.getViewport().update(pWidth, pHeight, true);
	}
	
	/**
	 * Permet de libérer la mémoire lorsque l'application est détruite.
	 */
	public void dispose() {
		gStageGrounds.dispose();
	}
}
