package Screens;

import ApplicationConfig.MainApplicationConfig;
import CityEngine.AIManager;
import CityEngine.CameraManager;
import CityEngine.CityMapRenderer;
import CityEngine.SavesManager;
import CityEngine.SceneGraphManager;
import UI.CityUIManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL30;

/**
 * Écran d'une partie sur la carte de la cité.
 */

public class ScreenCity implements Screen {
	
	/** Temps permettant de faire avancer les animations. */
	public static float gTime;
	
	/** Gestionnaire de l'intelligence artificielle du jeu. */
	public static AIManager gAIManager;
	
	/** Gestionnaire de la map de la cité. */
	private CityMapRenderer mCityMapManager;  
	
	/** Gestionnaire de la caméra. */
	private CameraManager mCameraManager;
	
	/** Gestionnaire du graphe de la scène. */
	private SceneGraphManager mSceneGraphManager; 
	
	/** Gestionnaire de l'interface. */
	private CityUIManager mUIManager;
	
	/** Pour la gestion de plusieurs écouteurs d'inputs, sur la même screen. */
	private InputMultiplexer mInputMultiplexer; 
	
	/**
	 * Constructeur.
	 */
	public ScreenCity() {
		gTime = 0.0f;
		gAIManager = new AIManager();
		mCameraManager = new CameraManager();
		mSceneGraphManager = new SceneGraphManager();
		mCityMapManager = new CityMapRenderer();
		mUIManager = new CityUIManager();
		mInputMultiplexer =  new InputMultiplexer();

		// Chargement d'une sauvegarde :
		SavesManager.loadSave("saves/sav2.xml");
	}

	/**
	 * Est appellée lorsque ce screen devient le screen courant.
	 */
	public void show() {
		// Les inputs sur ces éléments seront écoutés (l'ordre est important, le premier sera "sur" le second) :
		mInputMultiplexer.addProcessor(SceneGraphManager.gStageEffects); 
		mInputMultiplexer.addProcessor(SceneGraphManager.gStageCharacters); 
		mInputMultiplexer.addProcessor(SceneGraphManager.gStageBuildings); 
		mInputMultiplexer.addProcessor(SceneGraphManager.gStageGrounds); 
		mInputMultiplexer.addProcessor(mCameraManager); 
		mInputMultiplexer.addProcessor(mUIManager); 
		mInputMultiplexer.addProcessor(mUIManager.getMiniCityMap()); 
		mInputMultiplexer.addProcessor(CityUIManager.gStageUI); 
		Gdx.input.setInputProcessor(mInputMultiplexer);
	}

	/**
	 * Est appellée lorsque la screen n'est plus la screen courante.
	 */
	public void hide() {
		// Les inputs ne sont plus écoutés :
		Gdx.input.setInputProcessor(null); 
	}
	
	/**
	 * Boucle de rendu.
	 * Affiche les éléments à l'écran.
	 */
	public void render(float delta) {
		// Système de pause :
		if (!Gdx.input.isKeyPressed(Keys.P)) {
			gTime += Gdx.graphics.getDeltaTime() * MainApplicationConfig.gAnimationsSpeed;
		
			Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
			Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
			
			// Gestion des éventuels déplacements de la caméra :
			mCameraManager.render();
			
			// Rendu da la map :
			mCityMapManager.render();
			
			// Gestion des graphes de scène :
			mSceneGraphManager.render();
			
			// Rendu de l'interface :
			mUIManager.render();
		}
	}

	/**
	 * Gestion du redimensionnement de la fenêtre.
	 */
	public void resize(int pWidth, int pHeight) {
		mCameraManager.resize(pWidth, pHeight);
		mUIManager.resize(pWidth, pHeight);
	}
	
	/**
	 * Est appellée lorsque l'application est mise en pause.
	 */
	public void pause() {
	}
	
	/**
	 * Est appellée lorsque l'application reprend (après avoir été mise en pause).
	 */
	public void resume() {
	}

	/**
	 * Destructeur.
	 */
	public void dispose() {
		mCameraManager.dispose();
		mCityMapManager.dispose();
		mSceneGraphManager.dispose();
		mUIManager.dispose();
	}

}
