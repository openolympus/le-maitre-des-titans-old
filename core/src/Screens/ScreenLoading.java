package Screens;

import Application.MainApplication;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class ScreenLoading implements Screen {
	/** Ratio de redimensionnement des images. */
	float mSplashScale;
	
	/** La caméra. */
	private OrthographicCamera mCamera;
	
	/** Le graph des éléments à afficher. */
	private Stage mStage; 
	
	/** Afficheur. */
	private Batch mBatch;
	
	/** Image des slides. */
	private Texture mSplashImg1, mSplashImg2, mSplashImg3, mSplashImg4, mSplashImg5;
	
	/** Image regroupant plusieurs images. Permet l'économie de chargements. */
	private TextureAtlas mAtlas;
	
	/** Struture d'une TextureAtlas. */
	private Skin mSkin;
	
	/** Table pour de la mise en page. */
	private Table mTable;
	
	/** Typographie. */
	private BitmapFont mFont;
	
	/** Bouton pour afficher du texte sur les slides. */
	private TextButton mTextButton;
	
	/** Le gestionnaire des assets chargés. */
	public static AssetManager gAssetManager;
	
	/** Screen du jeu. */
	private ScreenCity mScreenMainGame; 
	
	/** L'application. */
	private MainApplication mMainApplication;
	
	/**
	 * Constructeur.
	 */
	public ScreenLoading(MainApplication pMainApplication) {
		mMainApplication =  pMainApplication;
	}

	/**
	 * Est appellée lorsque la screen devient la screen courante.
	 */
	public void show() {
		// Mise en place de l'interface de cet écran :
		mSplashScale = 0.0f;
		mCamera = new OrthographicCamera();
		mCamera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		mStage = new Stage(new ScreenViewport(mCamera), mBatch);
		mBatch = new SpriteBatch();
		mSplashImg1 = new Texture(Gdx.files.internal("splash/grandes/splash1.png"));
		mSplashImg2 = new Texture(Gdx.files.internal("splash/grandes/splash2.png"));
		mSplashImg3 = new Texture(Gdx.files.internal("splash/grandes/splash3.png"));
		mSplashImg4 = new Texture(Gdx.files.internal("splash/grandes/splash4.png"));
		mSplashImg5 = new Texture(Gdx.files.internal("splash/grandes/splash5.png"));
		mAtlas = new TextureAtlas(Gdx.files.internal("ui/boutonTexte.pack"));
		mSkin = new Skin(mAtlas);
		mTable = new Table(mSkin);
		mFont = new BitmapFont(Gdx.files.internal("fonts/zeus.fnt"), false);
		TextButtonStyle style = new TextButtonStyle();
		style.up = mSkin.getDrawable("boutonTexte");
		style.font = mFont;
		mTextButton = new TextButton("Invocation des dieux", style);
		mTable.add(mTextButton).width(258).height(50); 
		mTable.setPosition(Gdx.graphics.getWidth()/2f, Gdx.graphics.getHeight()/5f); 
		mStage.addActor(mTable);
		
		// Chargement asynchrone des assets :
		gAssetManager = new AssetManager();
		
		gAssetManager.load("fonts/zeus.fnt", BitmapFont.class);
		gAssetManager.load("fonts/zeusBlanc.fnt", BitmapFont.class);
		gAssetManager.load("ui/interfaceCite.atlas", TextureAtlas.class);
		
		gAssetManager.load("characters/textureParDefaut.png", Texture.class);
		gAssetManager.load("characters/AresMarcheN.png", Texture.class);
		gAssetManager.load("characters/AresMarcheNE.png", Texture.class);
		gAssetManager.load("characters/AresMarcheE.png", Texture.class);
		gAssetManager.load("characters/AresMarcheSE.png", Texture.class);
		gAssetManager.load("characters/AresMarcheS.png", Texture.class);
		gAssetManager.load("characters/AresMarcheSO.png", Texture.class);
		gAssetManager.load("characters/AresMarcheO.png", Texture.class);
		gAssetManager.load("characters/AresMarcheNO.png", Texture.class);
		gAssetManager.load("characters/ActeurDeplaceN.png", Texture.class);
		gAssetManager.load("characters/ActeurDeplaceNE.png", Texture.class);
		gAssetManager.load("characters/ActeurDeplaceE.png", Texture.class);
		gAssetManager.load("characters/ActeurDeplaceSE.png", Texture.class);
		gAssetManager.load("characters/ActeurDeplaceS.png", Texture.class);
		gAssetManager.load("characters/ActeurDeplaceSO.png", Texture.class);
		gAssetManager.load("characters/ActeurDeplaceO.png", Texture.class);
		gAssetManager.load("characters/ActeurDeplaceNO.png", Texture.class);
		gAssetManager.load("characters/ActeurMort.png", Texture.class);
		
		gAssetManager.load("buildings/feu_deco2.png", Texture.class);
		gAssetManager.load("buildings/petite_statue_achille.png", Texture.class);
		gAssetManager.load("buildings/sanctuaire_heros.png", Texture.class);
		gAssetManager.load("buildings/entrepot_accueil.png", Texture.class);
		gAssetManager.load("buildings/entrepot_emplacement.png", Texture.class);
		gAssetManager.load("buildings/habitation_modeste.png", Texture.class);
		gAssetManager.load("buildings/bassin.png", Texture.class);
		gAssetManager.load("buildings/theatre.png", Texture.class);
		gAssetManager.load("buildings/theatreAnim1.png", Texture.class);
		gAssetManager.load("buildings/ecole_theatre.png", Texture.class);
		gAssetManager.load("buildings/ecole_theatre_animation.png", Texture.class);
		gAssetManager.load("buildings/ghost_building_red.png", Texture.class);
		gAssetManager.load("buildings/ghost_building_green.png", Texture.class);
		gAssetManager.load("buildings/ghost_street_red.png", Texture.class);
		gAssetManager.load("buildings/ghost_street_green.png", Texture.class);
		gAssetManager.load("buildings/habitation_luxe_sud.png", Texture.class);
		gAssetManager.load("buildings/habitation_luxe_ouest.png", Texture.class);
		gAssetManager.load("buildings/habitation_luxe_nord.png", Texture.class);
		gAssetManager.load("buildings/habitation_luxe_est.png", Texture.class);
		
		gAssetManager.load("effects/city_view.png", Texture.class);
		
		gAssetManager.load("ground/ground1.atlas", TextureAtlas.class);
	}

	/**
	 * Est appellée lorsque la screen n'est plus la screen courante.
	 */
	public void hide() {
		
	}
	
	/**
	 * Boucle de rendu.
	 * Affiche les éléments à l'écran.
	 */
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
		
		// Si les chargements sont finis, on passe à l'écran suivant :
		if (gAssetManager.update()) {
			mScreenMainGame = new ScreenCity();
			mMainApplication.setScreen(mScreenMainGame);
		}
		
		// Ratio des scales des images :
		if (Gdx.graphics.getWidth() > Gdx.graphics.getHeight()) {
			mSplashScale = (float) Gdx.graphics.getWidth() / (float) mSplashImg1.getWidth();
		} else {
			mSplashScale = (float) Gdx.graphics.getHeight() / (float) mSplashImg1.getHeight();
		}
		
		// Affichage des slides en fonction de l'avancement du chargement :
		mCamera.update();
		mBatch.setProjectionMatrix(mCamera.combined);
		mBatch.begin();
		if (gAssetManager.getProgress() <= 0.200f) {
			mTextButton.setText("Recrutement des héros");
			mBatch.draw(mSplashImg1, 
					    Gdx.graphics.getWidth()/2 - mSplashImg1.getWidth()/2, // the x-coordinate in screen space
					    Gdx.graphics.getHeight()/2 - mSplashImg1.getHeight()/2, // the y-coordinate in screen space
					    mSplashImg1.getWidth()/2, // the x-coordinate of the scaling and rotation origin relative to the screen space coordinates
					    mSplashImg1.getHeight()/2, // the y-coordinate of the scaling and rotation origin relative to the screen space coordinates
					    mSplashImg1.getWidth(), // width the width in pixels
					    mSplashImg1.getHeight(), // height the height in pixels
					    mSplashScale, // the scale of the rectangle around originX/originY in x
					    mSplashScale, // the scale of the rectangle around originX/originY in y
					    0, // the angle of counter clockwise rotation of the rectangle around originX/originY
					    0, // x-coordinate in texel space
					    0, // y-coordinate in texel space
					    mSplashImg1.getWidth(), // the source with in texels
					    mSplashImg1.getHeight(), // the source height in texels
					    false, // whether to flip the sprite horizontally
					    false); // whether to flip the sprite vertically
		}
		if (0.200f < gAssetManager.getProgress() && gAssetManager.getProgress() <= 0.400f) {
			mTextButton.setText("Rassemblement des montres");
			mBatch.draw(mSplashImg2, 
					    Gdx.graphics.getWidth()/2 - mSplashImg2.getWidth()/2,  Gdx.graphics.getHeight()/2 - mSplashImg2.getHeight()/2, 
					    mSplashImg2.getWidth()/2,  mSplashImg2.getHeight()/2, 
					    mSplashImg2.getWidth(), mSplashImg2.getHeight(), 
					    mSplashScale, mSplashScale, 
					    0, 
					    0, 0,
					    mSplashImg2.getWidth(), mSplashImg2.getHeight(),
					    false, false);
		}
		if (0.400f < gAssetManager.getProgress() && gAssetManager.getProgress() <= 0.600f) {
			mTextButton.setText("Dressage des moutons");
			mBatch.draw(mSplashImg3, 
					    Gdx.graphics.getWidth()/2 - mSplashImg2.getWidth()/2,  Gdx.graphics.getHeight()/2 - mSplashImg2.getHeight()/2, 
					    mSplashImg2.getWidth()/2,  mSplashImg2.getHeight()/2, 
					    mSplashImg2.getWidth(), mSplashImg2.getHeight(), 
					    mSplashScale, mSplashScale, 
					    0, 
					    0, 0,
					    mSplashImg2.getWidth(), mSplashImg2.getHeight(),
					    false, false);
		}
		if (0.600f < gAssetManager.getProgress() && gAssetManager.getProgress() <= 0.800f) {
			mTextButton.setText("Récolte des oursins");
			mBatch.draw(mSplashImg4,  
					    Gdx.graphics.getWidth()/2 - mSplashImg2.getWidth()/2,  Gdx.graphics.getHeight()/2 - mSplashImg2.getHeight()/2, 
					    mSplashImg2.getWidth()/2,  mSplashImg2.getHeight()/2, 
					    mSplashImg2.getWidth(), mSplashImg2.getHeight(), 
					    mSplashScale, mSplashScale, 
					    0, 
					    0, 0,
					    mSplashImg2.getWidth(), mSplashImg2.getHeight(),
					    false, false);
		}
		if (0.800f < gAssetManager.getProgress()) {
			mBatch.draw(mSplashImg5, 
					    Gdx.graphics.getWidth()/2 - mSplashImg2.getWidth()/2,  Gdx.graphics.getHeight()/2 - mSplashImg2.getHeight()/2, 
					    mSplashImg2.getWidth()/2,  mSplashImg2.getHeight()/2, 
					    mSplashImg2.getWidth(), mSplashImg2.getHeight(), 
					    mSplashScale, mSplashScale, 
					    0, 
					    0, 0,
					    mSplashImg2.getWidth(), mSplashImg2.getHeight(),
					    false, false);
		}
		mBatch.end();
		mStage.draw();
	}

	/**
	 * Gestion du redimensionnement de la fenêtre.
	 */
	public void resize(int pWidth, int pHeight) {
		mCamera.viewportWidth = pWidth;
		mCamera.viewportHeight = pHeight;
		mCamera.update();
		mTable.setPosition(Gdx.graphics.getWidth()/2f, Gdx.graphics.getHeight()/5f); 
		mStage.getViewport().update(pWidth, pHeight, true);
	}
	
	/**
	 * Est appellée lorsque l'application est mise en pause.
	 */
	public void pause() {

	}
	
	/**
	 * Est appellée lorsque l'application reprend (après avoir été mise en pause).
	 */
	public void resume() {

	}

	/**
	 * Destructeur.
	 */
	public void dispose() {
		gAssetManager.dispose();
		mStage.dispose();
	}
}
