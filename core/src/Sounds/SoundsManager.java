package Sounds;

import ApplicationConfig.MainApplicationConfig;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.audio.Sound;

/**
 * Gestionnaire de sons.
 * Gère tout les sons de l'application.
 */

public class SoundsManager {
	
	/** Sons de clic gauche et clic droit. Leur contenu est changé celon le contexte. */
	public static Sound gLeftClick, gRightClick; 

	public static final Sound gDefaultClick = Gdx.audio.newSound(Gdx.files.internal("sounds/clic.ogg"));
	public static final Sound gAddBuilding = Gdx.audio.newSound(Gdx.files.internal("sounds/place_building.ogg"));
	
	/**
	 * Constructeur.
	 */
	public SoundsManager() {
		gLeftClick = gDefaultClick;
		gRightClick = gDefaultClick;
	}
	
	/**
	 * Boucle de rendu.
	 * Joue les sons.
	 */
	public void render() {
		if (Gdx.input.justTouched()) {
			// Si clic gauche :
			if (Gdx.input.isButtonPressed(Buttons.LEFT)) {
				// On joue le son mis dans la variable pour le clic gauche :
				gLeftClick.play(MainApplicationConfig.gSoundVolume);
				
				// On remet le sons par defaut :
				gLeftClick = gDefaultClick;
			}
			
			// Si clic droit, on joue le son mis dans la variable pour le clic droit :
			if (Gdx.input.isButtonPressed(Buttons.RIGHT)) {
				gRightClick.play(MainApplicationConfig.gSoundVolume);
				gRightClick = gDefaultClick;
			}
		}
	}
	
	/**
	 * Destructeur.
	 */
	public void dispose() {
		gLeftClick.dispose();
		gRightClick.dispose();
		gDefaultClick.dispose();
		gAddBuilding.dispose();
	}
}
