package UI;

import CityActors.GroundLand420;
import CityActors.Ghost.eGhostChildType;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * L'interface comportant les boutons route, barrage, pelle et annulé situés sur le menu lateral.
 */

public class BuildTools extends CityUI {
	
	/** Tables qui composent cette UI. Elles permettent de mettre en page les éléments de l'interface. */
	private Table mTableRoadAndCo;
	
	/** Boutons qui composent cette UI. */
	private final TextButton mButtonRoute, mButtonBarrage, mButtonPelle, mButtonAnnuler; 

	/**
	 * Constructeur.
	 */
	public BuildTools() {
		// Création des Tables :
		mTableRoadAndCo = new Table();
		
		// Bouton route :
		mButtonRoute = createTextButton("", "route");
		addDragAndBuildListener(mButtonRoute, GroundLand420.class, eGhostChildType.Street);
		mTableRoadAndCo.add(mButtonRoute).width(35).height(37).padTop(3); 
		
		// Bouton barrage :
		mButtonBarrage = createTextButton("", "barrage");
		mButtonBarrage.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
			}
        } );
		mTableRoadAndCo.add(mButtonBarrage).width(32).height(37).padTop(3); 
		
		// Bouton pelle :
		mButtonPelle = createTextButton("", "pelle");
		mButtonPelle.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
			}
        } );
		mTableRoadAndCo.add(mButtonPelle).width(32).height(37).padTop(3); 
		
		// Bouton annuler :
		mButtonAnnuler = createTextButton("", "annuler");
		mButtonAnnuler.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
			}
        } );
		mTableRoadAndCo.add(mButtonAnnuler).width(35).height(37).padTop(3);  
		
		// Ajout des Tables à la liste mAllTables :
		mAllTables.add(mTableRoadAndCo);
	}
	
	/**
	 * Permet de redéfinir la positions de chaque élément de l'UI en prenant en compte
	 * les dimensions courantes de l'écran.
	 */
	public void createPositions() {
		mAllPositions.put(mTableRoadAndCo.hashCode(), new Vector2(Gdx.graphics.getWidth()-69, Gdx.graphics.getHeight()-452));
	}
}
