package UI;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import CityActors.CityActor;
import CityActors.Ghost.eGhostChildType;
import CityEngine.CityMapRenderer;
import Screens.ScreenLoading;
import Sounds.SoundsManager;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;

/**
 * L'interface (au sens visuel, pas au sens java) de l'écran principal lors d'une partie.
 * Ne peut pas être utilisé comme tel, est prévue pour 
 * être étendue. Voir HorizontalMenu par exemple.
 */

public class CityUI {
	
	/** Toutes les Tables qui composent l'UI. */
	protected Array<Table> mAllTables; 
	
	/** Map de toutes les positions des Tables qui composent l'UI. */
	protected Map<Integer, Vector2> mAllPositions; 
	
	/** Typographies. */
	protected BitmapFont mFontBlue, mFontWhite; 
	
	/** Image regroupant plusieurs images. Permet l'économie de chargements. */
	protected TextureAtlas mMainGameAtlas; 
	
	/** Struture d'une TextureAtlas. */
	protected Skin mCityUISkin; 
	
	/**
	 * Constructeur.
	 */
	public CityUI() {
		mAllTables = new Array<Table>();
		mAllPositions = new HashMap<Integer, Vector2>();

		// Chargement des typographie :
		mFontBlue = ScreenLoading.gAssetManager.get("fonts/zeus.fnt", BitmapFont.class);
		mFontWhite = ScreenLoading.gAssetManager.get("fonts/zeusBlanc.fnt", BitmapFont.class);
		
		// L'atlas des interfaces :
		mMainGameAtlas = ScreenLoading.gAssetManager.get("ui/interfaceCite.atlas", TextureAtlas.class);
		
		// Le skin de l'atlas :
		mCityUISkin = new Skin(mMainGameAtlas);
	}
	
	/**
	 * Retourne toutes les Tables qui composent l'UI.
	 */
	public Array<Table> getAllTables() {
		return mAllTables;
	}
	
	/**
	 * Permet de redéfinir la positions de chaque élément de l'UI en prenant en compte
	 * les dimensions courantes de l'écran.
	 */
	public void createPositions() {
		// Laisser vide
	}
	
	/**
	 * Met à jour les positions de chaque éléments de l'interface.
	 */
	public void updateAllPositions() {
		// Redéfinis les positions en prenant en compte les dimensions courantes de l'écran :
		createPositions();
		
		// Affecte ces nouvelles positions à leur Table respectives :
		for (Map.Entry<Integer, Vector2> lHashCodeAndPosition : mAllPositions.entrySet()) {
			Integer lHashCode = lHashCodeAndPosition.getKey();
			boolean lFound = false;
			for (Integer i = 0; i < mAllTables.size && !lFound; i++) {
				if (mAllTables.get(i).hashCode() == lHashCode) {
//					mAllTables.get(i).debug(); // Pour pouvoir afficher les bordures en mode debug
					mAllTables.get(i).setPosition(lHashCodeAndPosition.getValue().x, lHashCodeAndPosition.getValue().y);
					lFound = true;
				}
			}
		}
	}
	
	/**
	 * Notre propre méthode de création de bouttons.
	 * 
	 * @param pText Le texte figurant sur le bouton
	 * @param pDrawableName Le nom de l'image de fond du bouton, sans les suffixes (pas "ficher_survol.png", juste "fichier").
	 * @param pIsCheckable vrai si le bouton pourra etre coché.
	 * 
	 * @return Un bouton correctement créé.
	 */
	public TextButton createTextButton(String pText, String pDrawableName, boolean pIsCheckable) {
		String styleUp = pDrawableName;
		String styleDown = pDrawableName + "_clic";
		String styleChecked = pDrawableName + "_clic";
		String styleDisabled = pDrawableName + "_desact";
		String styleOver = pDrawableName + "_survol";
		
		// On vérifis si ces fichiers existent bien :
		try {
			mCityUISkin.getDrawable(styleUp);
		}
		catch(GdxRuntimeException e) {
			styleUp = pDrawableName;
		}
		
		try {
			mCityUISkin.getDrawable(styleDown);
		}
		catch(GdxRuntimeException e) {
			styleDown = pDrawableName;
		}
		
		try {
			mCityUISkin.getDrawable(styleChecked);
		}
		catch(GdxRuntimeException e) {
			styleChecked = pDrawableName;
		}
		
		try {
			mCityUISkin.getDrawable(styleDisabled);
		}
		catch(GdxRuntimeException e) {
			styleDisabled = pDrawableName;
		}
		
		try {
			mCityUISkin.getDrawable(styleOver);
		}
		catch(GdxRuntimeException e) {
			styleOver = pDrawableName;
		}

		TextButtonStyle style = new TextButtonStyle();
		style.up = mCityUISkin.getDrawable(styleUp);
		style.down = mCityUISkin.getDrawable(styleDown);
		if (pIsCheckable) {
			style.checked = mCityUISkin.getDrawable(styleChecked);
		}
		style.disabled = mCityUISkin.getDrawable(styleDisabled);
		style.over = mCityUISkin.getDrawable(styleOver);
		style.font = mFontBlue;
		
		TextButton button = new TextButton(pText, style);

		return button;
	}

	/**
	 * Création de bouttons.
	 * 
	 * @param pText Le texte figurant sur le bouton
	 * @param pDrawableName Le nom de l'image de fond du bouton, sans les suffixes (pas "ficher_survol.png", juste "fichier").
	 * 
	 * @return Un bouton correctement créé.
	 */
	public TextButton createTextButton(String pText, String pDrawableName) {
		return createTextButton(pText, pDrawableName, false);
	}
	
	/**
	 * Création d'un écouteur de clic sur pTargetActor.
	 * Si on clic sur cet Actor :
	 * 		Cela affiche une ombre de type pChildType et de taille pGhostWidth et pGhostHeight (en tuiles).
	 * 		Si on clic gauche sur la zone de la cité, cela créer un bâtiment de type pBuildType.
	 * 		Si on clic droit cela cache l'ombre et supprime l'écouteur de clic sur la zone de la cité.
	 */
	public void addClickAndBuildListener(Actor pTargetActor, Class<?> pBuildType, eGhostChildType pChildType, int pGhostWidth, int pGhostHeight) {
		// Redéfinition des paramètres en final :
		final Class<?> lBuildType = pBuildType;
		final eGhostChildType lChildType = pChildType;
		final int lGhostWidth = pGhostWidth;
		final int lGhostHeight = pGhostHeight;
		
		// Création d'un écouteur sur le pTargetActor :
		pTargetActor.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				// Supprime les anciennes ombres enfants :
				CityMapRenderer.gGhost.getAllChildren().clear();
				
				// Renseigne le type du bâtiment qui sera créé :
				CityMapRenderer.gGhost.setBuildType(lBuildType);
				
				// Renseigne le type de l'ombre :
				CityMapRenderer.gGhost.setGhostChildType(lChildType);
				
				// Ajoute les ombres enfants :
				CityMapRenderer.gGhost.addChild(lGhostWidth, lGhostHeight);
				
				// Affichage de l'ombre du bâtiment :
				CityMapRenderer.gGhost.setVisible(true);
				
				// Création d'un écouteur sur la zone de vue de la cité :
				CityUIManager.gCityArea.addListener(new ClickListener() {
					public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
						// Si clic gauche :
						if (button == 0) {
							// Si toutes les cases sont valides :
							if (CityMapRenderer.gGhost.isValid()) {
								// On définit le sons à jouer pour le clic :
								SoundsManager.gLeftClick = SoundsManager.gAddBuilding;
								
								// Mise à jour de la position du groupe d'ombres :
								CityMapRenderer.gGhost.updatePosition();
			
								// Création du bâtiment demandé :
								try {
									Class<?> lClassDetected = Class.forName("" + CityMapRenderer.gGhost.getTargetType().getCanonicalName());
									Constructor<?> lConstructor = lClassDetected.getConstructor(new Class[]{int.class, int.class});
									
									// Création du cityActor correspondant à la position du groupe d'ombres :
									lConstructor.newInstance(new Object [] {Math.round(CityMapRenderer.gGhost.getX()), Math.round(CityMapRenderer.gGhost.getY())});
								} catch (ClassNotFoundException e) {
									e.printStackTrace(); // La classe n'existe pas
								}
								catch (NoSuchMethodException e) {
									e.printStackTrace(); // La classe n'a pas le constructeur recherché
							    }
								catch (InstantiationException e) {
									e.printStackTrace(); // La classe est abstract ou est une interface ou n'a pas de constructeur accessible sans paramètre
							    }
								catch (IllegalAccessException e) {
									e.printStackTrace(); // La classe n'est pas accessible
							    }
							    catch (InvocationTargetException e) {
							    	e.printStackTrace(); // Exception déclenchée si le constructeur invoqué a lui-même déclenché une exception
							    }
								catch (IllegalArgumentException e) {
									e.printStackTrace(); // Mauvais type de paramètre (Pas obligatoire d'intercepter IllegalArgumentException)
							    }
							}
						}
						
						// Si clic droit :
						if (button == 1) {
							// On définit le bon sons joué pour le clic :
							SoundsManager.gLeftClick = SoundsManager.gDefaultClick;
							
							// On cache l'ombre :
							CityMapRenderer.gGhost.setVisible(false);
							
							// Supprime les ombres enfants :
							CityMapRenderer.gGhost.getAllChildren().clear();
							
							// On supprimer cet écouteur de clic :
							CityUIManager.gCityArea.removeListener(this);
						}
						return false;
					}
				});
			}
        });
	}
	
	public void addDragAndBuildListener(Actor pTargetActor, Class<?> pBuildType, eGhostChildType pChildType) {
		// Redéfinition des paramètres en final :
		final Class<?> lBuildType = pBuildType;
		final eGhostChildType lChildType = pChildType;
		// Création d'un écouteur sur le pTargetActor :
		pTargetActor.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				// Supprime les anciennes ombres enfants :
				CityMapRenderer.gGhost.getAllChildren().clear();
				
				// Renseigne le type du bâtiment qui sera créé :
				CityMapRenderer.gGhost.setBuildType(lBuildType);
				
				// Renseigne le type de l'ombre :
				CityMapRenderer.gGhost.setGhostChildType(lChildType);
				
				// Créer une ombre de 1x1 :
				CityMapRenderer.gGhost.addChild(1, 1);
				
				// Affichage de l'ombre du bâtiment :
				CityMapRenderer.gGhost.setVisible(true);
				
				// Création d'un écouteur sur la zone de vue de la cité :
				CityUIManager.gCityArea.addListener(new ClickListener() {
					float startX, startY;
					int flag=0;
					
					
					private void build(){
						buildUnit(startX,startY);
						buildUnit(CityMapRenderer.gGhost.getX(),CityMapRenderer.gGhost.getY());
					}
					
					
					private void buildUnit(float x, float y){
						// Si toutes les cases sont valides :
						if (CityMapRenderer.gGhost.isValid()) {
							// On définit le sons à jouer pour le clic :
							SoundsManager.gLeftClick = SoundsManager.gAddBuilding;
							// Mise à jour de la position du groupe d'ombres :
							CityMapRenderer.gGhost.updatePosition();
							// Création du bâtiment demandé :
							try {
								Class<?> lClassDetected = Class.forName("" + CityMapRenderer.gGhost.getTargetType().getCanonicalName());
								Constructor<?> lConstructor = lClassDetected.getConstructor(new Class[]{int.class, int.class});
								
								// Création du cityActor correspondant à la position du groupe d'ombres :
								//lConstructor.newInstance(new Object [] {Math.round(CityMapRenderer.gGhost.getX()), Math.round(CityMapRenderer.gGhost.getY())});
								lConstructor.newInstance(new Object [] {Math.round(x), Math.round(y)});

							} catch (ClassNotFoundException e) {
								e.printStackTrace(); // La classe n'existe pas
							}
							catch (NoSuchMethodException e) {
								e.printStackTrace(); // La classe n'a pas le constructeur recherché
						    }
							catch (InstantiationException e) {
								e.printStackTrace(); // La classe est abstract ou est une interface ou n'a pas de constructeur accessible sans paramètre
						    }
							catch (IllegalAccessException e) {
								e.printStackTrace(); // La classe n'est pas accessible
						    }
						    catch (InvocationTargetException e) {
						    	e.printStackTrace(); // Exception déclenchée si le constructeur invoqué a lui-même déclenché une exception
						    }
							catch (IllegalArgumentException e) {
								e.printStackTrace(); // Mauvais type de paramètre (Pas obligatoire d'intercepter IllegalArgumentException)
						    }
						}
					}
					
					public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
						//Si clic gauche
						if (button ==0) {
							CityActor a= new CityActor((int)x,(int)y){};
							startX=a.getX();
							startY=a.getY();
							flag=1;
							return true;

						}
						
						// Si clic droit :
						if (button == 1) {
							// On définit le bon sons joué pour le clic :
							SoundsManager.gLeftClick = SoundsManager.gDefaultClick;
							
							// On cache l'ombre :
							CityMapRenderer.gGhost.setVisible(false);
							
							// Supprime les ombres enfants :
							CityMapRenderer.gGhost.getAllChildren().clear();
							
							// On supprimer cet écouteur de clic :
							CityUIManager.gCityArea.removeListener(this);
							flag=0;
						}
						return false;
					}
					
					public void touchUp(InputEvent event, float x, float y, int pointer, int button){
						// Si clic gauche :
						if (button == 0 && flag==1) {
							build();
						}
					}
				});
			}
        });
	}
}
