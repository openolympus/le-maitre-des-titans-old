package UI;

import Screens.ScreenLoading;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

/**
 * Gestionnaire d'interfaces.
 * Contient tout les menus, les barres, les boutons, etc...
 * Permet d'en ajouter, de les afficher et de les mettre à jours facilement.
 */

public class CityUIManager implements InputProcessor {
	
	/** Le graph de scène des interface, contient tout les éléments d'interface. */
	public static Stage gStageUI; 
	
	/** Interface affichant des infos de debug. */
	private Debug mDebug;
	
	/** Interface affichant le menu horizontale en haut. */
	private HorizontalMenu mHorizontalMenu;
	
	/** Interface affichant le menu vertical à droite en mode game. */
	private VerticalMenuGameMode mVerticalMenuGameMode;
	
	/** Interface affichant le menu vertical à droite en mode editor. */
	private VerticalMenuEditorMode mVerticalMenuEditorMode;
	
	/** Interface comportant les deux onglets Manage et Statistiques. */
	private ManageStats mManageStats;
	
	/** Interface comportant les boutons route, barrage, pelle et annulé. */
	private BuildTools mBuildTools;
	
	/** Interface comportant les boutons objectifs, rotation et carte du monde. */
	private GoalsRotateWorldMap mGoalsRotateWorldMap;
	
	/** La mini map affichée par dessus le menu vertical à droite. */
	private MiniMap mMiniCityMap;
	
	/** Représentation de la zone où l'on voit la cité. Permet de détacter les clic dans cette zone. */
	public static TextButton gCityArea;
	
	/** Vrai si le menu latéral est en mode éditor. */
	private boolean mIsEditorMode;
	
	/**
	 * Constructeur.
	 */
	public CityUIManager() {
		gStageUI = new Stage(new ScreenViewport());
		
		// Création des éléments de l'interface :
		mDebug = new Debug();
		mHorizontalMenu = new HorizontalMenu();
		mVerticalMenuGameMode = new VerticalMenuGameMode();
		setTableVisible(mVerticalMenuGameMode, true);
		mVerticalMenuEditorMode = new VerticalMenuEditorMode();
		setTableVisible(mVerticalMenuEditorMode, false);
		mManageStats = new ManageStats();
		mBuildTools = new BuildTools();
		mGoalsRotateWorldMap = new GoalsRotateWorldMap();
		
		// Création et ajout d'un Actor transparent délimitant la zone où l'on voit la cité :
		TextButtonStyle style = new TextButtonStyle();
		style.up = new TextureRegionDrawable(new TextureRegion(ScreenLoading.gAssetManager.get("effects/city_view.png", Texture.class)));
		style.down = new TextureRegionDrawable(new TextureRegion(ScreenLoading.gAssetManager.get("effects/city_view.png", Texture.class)));
		style.checked = new TextureRegionDrawable(new TextureRegion(ScreenLoading.gAssetManager.get("effects/city_view.png", Texture.class)));
		style.disabled = new TextureRegionDrawable(new TextureRegion(ScreenLoading.gAssetManager.get("effects/city_view.png", Texture.class)));
		style.over = new TextureRegionDrawable(new TextureRegion(ScreenLoading.gAssetManager.get("effects/city_view.png", Texture.class)));
		style.font = ScreenLoading.gAssetManager.get("fonts/zeus.fnt", BitmapFont.class);
		gCityArea = new TextButton("", style);
		gCityArea.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		gStageUI.addActor(gCityArea);
		
		// Ajout de toutes les Tables au Stage du manager :
		addAllTablesToStage(mDebug);
		addAllTablesToStage(mHorizontalMenu);
		addAllTablesToStage(mVerticalMenuGameMode);
		addAllTablesToStage(mVerticalMenuEditorMode);
		addAllTablesToStage(mManageStats);
		addAllTablesToStage(mBuildTools);
		addAllTablesToStage(mGoalsRotateWorldMap);
		
		// Ajout de la mini map :
		mMiniCityMap = new MiniMap();
		
		// Par defaut l'interface est en mode game :
		mIsEditorMode = false;
	}
	
	/**
	 * Ajoute au gStageUI principal toutes les tables de l'interface donnée.
	 * 
	 * @param pUIMainGame L'interface dont les tables sont à ajouter.
	 */
	public void addAllTablesToStage(CityUI pUIMainGame) {
		for (Table lTable : pUIMainGame.getAllTables()) {
			gStageUI.addActor(lTable);
		}
	}
	
	/**
	 * Retourne la mini map affichée par dessus le menu vertical à droite.
	 */
	public MiniMap getMiniCityMap() {
		return mMiniCityMap;
	}
	
	/**
	 * Boucle de rendu.
	 * Affiche les éléments à l'écran.
	 */
	public void render() {
		// Mise à jour des positions des Tables des UI :
		mDebug.updateAllPositions();
		mHorizontalMenu.updateAllPositions();
		mVerticalMenuGameMode.updateAllPositions();
		mVerticalMenuEditorMode.updateAllPositions();
		mManageStats.updateAllPositions();
		mBuildTools.updateAllPositions();
		mGoalsRotateWorldMap.updateAllPositions();
		
		// Pour afficher les bordures des tables (debug mode) :
//		Table.drawDebug(gStageUI);
		
		// Actionne les actions de tous les Actors du gStageUI :
		gStageUI.act();
		
		// Dessine à l'écran tous les Actors du gStageUI  :
	    gStageUI.draw();
	    
	    // Affiche la mini map :
	    mMiniCityMap.render();
	}
	
	/**
	 * Gestion du redimensionnement de la fenêtre.
	 */
	public void resize(int pWidth, int pHeight) {
		gStageUI.getViewport().update(pWidth, pHeight, true);
		mMiniCityMap.resize(pWidth, pHeight);
		gCityArea.setBounds(0, 0, pWidth, pHeight);
	}
	
	public void setTableVisible(CityUI pCityUI, boolean pVisible) {
		for (Table lTable : pCityUI.getAllTables()) {
			lTable.setVisible(pVisible);
		}
	}
	
	/**
	 * Gestion des touches appuyées.
	 */
	public boolean keyDown(int keycode) {
		// Si TAB est appuyé :
		if (keycode == Keys.TAB) {
			// On passe soit en mode editor :
			if (!mIsEditorMode) {
				mIsEditorMode = true;
				setTableVisible(mVerticalMenuGameMode, false);
				setTableVisible(mVerticalMenuEditorMode, true);
			// Soit en mode game :
			} else {
				mIsEditorMode = false;
				setTableVisible(mVerticalMenuGameMode, true);
				setTableVisible(mVerticalMenuEditorMode, false);
			}
		}
		return false;
	}

	/**
	 * Gestion des touches relachées.
	 */
	public boolean keyUp(int keycode) {
		return false;
	}

	/**
	 * Si la touche est actionnée.
	 */
	public boolean keyTyped(char character) {
		return false;
	}

	/**
	 * Gestion des touches appuyées sur l'écran.
	 */
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	/**
	 * Gestion des touches relachées sur l'écran.
	 */
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	/**
	 * Gestion des drags sur l'écran.
	 */
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	/**
	 * Gestion des déplacements de la souris sur l'écran.
	 */
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	/**
	 * Gestion du zoom avec la roulette de la souris.
	 * @param pAmount 
	 * -1 : zoom
	 * +1 : zoom
	 */
	public boolean scrolled(int pAmount) {
		return false;
	}
	
	/**
	 * Destructeur.
	 */
	public void dispose() {
		gStageUI.dispose();
		mMiniCityMap.dispose();
	}
}
