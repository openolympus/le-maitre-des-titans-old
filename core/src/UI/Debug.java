package UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

/**
 * Affiche à l'écran des infos de debug.
 */

public class Debug extends CityUI {

	/** Texte de debug. */
	private Label mDebugLabel;
	
	/** Table composant cet UI. */
	private Table mDebugTable; 
	
	/**
	 * Constructeur.
	 */
	public Debug() {
		super();
		
		// Texte de debug :
		LabelStyle labelStyle = new LabelStyle();
		labelStyle.font = mFontWhite;
		mDebugLabel = new Label("Echap pour quitter \n " + 
				                "Flèches pour se déplacer \n" + 
		                        "Entrée pour replacer le zoom", labelStyle);
		mDebugTable = new Table();
		mDebugTable.add(mDebugLabel);
		
		// Ajout des Tables à la liste mAllTables :
		mAllTables.add(mDebugTable);
	}
	
	/**
	 * Permet de redéfinir la positions de chaque élément de l'UI en prenant en compte
	 * les dimensions courantes de l'écran.
	 */
	public void createPositions() {
		mAllPositions.put(mDebugTable.hashCode(), new Vector2(130, Gdx.graphics.getHeight()-65)); // Définis la position de la DebugTable
	}
}
