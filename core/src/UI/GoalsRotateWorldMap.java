package UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * L'interface comportant les boutons objectifs, rotation et carte du monde situés en bas
 * du menu lateral.
 */

public class GoalsRotateWorldMap extends CityUI {

	/** Tables qui composent cette UI. Elles permettent de mettre en page les éléments de l'interface. */
	private Table mTableGoalsAndCo;
	
	/** Boutons qui composent cette UI. */
	private final TextButton mButtonObjectifs, mButtonOrientation, mButtonMapMonde; 
				  
	/**
	 * Constructeur.
	 */
	public GoalsRotateWorldMap() {
		// Création des Tables :
		mTableGoalsAndCo = new Table();
		
		// Bouton objectifs :
		mButtonObjectifs = createTextButton("", "objectifs");
		mButtonObjectifs.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
			}
        } );
		mTableGoalsAndCo.add(mButtonObjectifs).width(33).height(33).padLeft(2);
		
		// Bouton orientation :
		mButtonOrientation = createTextButton("", "orientation");
		mButtonOrientation.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
			}
        } );
		mTableGoalsAndCo.add(mButtonOrientation).width(69).height(33).padLeft(1).padRight(1);
		
		// Bouton mapMonde :
		mButtonMapMonde = createTextButton("", "mapMonde");
		mButtonMapMonde.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
			}
        } );
		mTableGoalsAndCo.add(mButtonMapMonde).width(67).height(33);
		
		// Ajout des Tables à la liste mAllTables :
		mAllTables.add(mTableGoalsAndCo);
	}
	
	/**
	 * Permet de redéfinir la positions de chaque élément de l'UI en prenant en compte
	 * les dimensions courantes de l'écran.
	 */
	public void createPositions() {
		mAllPositions.put(mTableGoalsAndCo.hashCode(), new Vector2(Gdx.graphics.getWidth()-91, Gdx.graphics.getHeight()-581)); 
	}
}
