package UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * L'interface du menu horizontale en haut de l'écran de jeu principal.
 * Contient les boutons Fichiers, Options, Aide etc...
 */

public class HorizontalMenu extends CityUI {

	/** Toutes les Tables qui composent cette UI. */
	private Table mHorizontalMenu, 
				  mHorizontalMenu1,
				  mHorizontalMenu2,
				  mHorizontalMenu3; 
	
	/** Tous les Boutons qui composent cette UI. */
	private TextButton mHorizontalMenuButton,
					   mFichierButton, 
				       mOptionsButton, 
				       mAideButton, 
				       mTresorieButton, 
				       mCptPopulationButton, 
				       mDateButton;
	
	/**
	 * Constructeur.
	 */
	public HorizontalMenu() {
		super();
		
		// Création des Tables :
		mHorizontalMenu = new Table();
		mHorizontalMenu1 = new Table();
		mHorizontalMenu2 = new Table();
		mHorizontalMenu3 = new Table();

		// Menu horizontal (faux bouton) :
		mHorizontalMenuButton = createTextButton("", "menuHorizontalBackGround");
		mHorizontalMenu.add(mHorizontalMenuButton).width(5028).height(30); 
		
		// Bouton fichier :
		mFichierButton = createTextButton("Fichier", "vide68x22");
		mFichierButton.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
			}
        } );
		mHorizontalMenu1.add(mFichierButton).width(68).height(22);
		
		// Bouton options :
		mOptionsButton = createTextButton("Options", "vide68x22");
		mOptionsButton.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
			}
        } );
		mHorizontalMenu1.add(mOptionsButton).width(68).height(22); 
		
		// Bouton aide :
		mAideButton = createTextButton("Aide", "vide68x22");
		mAideButton.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
			}
        } );
		mHorizontalMenu1.add(mAideButton).width(68).height(22); 
		
		// Bouton trésorerie
		mTresorieButton = createTextButton("          999 999 999 999", "tresorie"); 
		mTresorieButton.getLabel().setAlignment(Align.left); // Alignement du texte
		mTresorieButton.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
			}
        } );
		mHorizontalMenu2.add(mTresorieButton).width(200).height(22); 
		
		// Bouton compte de la population :
		mCptPopulationButton = createTextButton("        53 999", "cptPopulation"); 
		mCptPopulationButton.getLabel().setAlignment(Align.left); 
		mCptPopulationButton.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
			}
        } );
		mHorizontalMenu2.add(mCptPopulationButton).width(200).height(22).padLeft(50); 
		
		// Bouton date :
		mDateButton = createTextButton("Juin 2014 apr J-C", "vide205x22");
		mDateButton.getLabel().setAlignment(Align.center);
		mDateButton.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {	
			}
        } );
		mHorizontalMenu3.add(mDateButton).width(205).height(22); 
		
		// Ajout des Tables à la liste mAllTables :
		mAllTables.add(mHorizontalMenu);
		mAllTables.add(mHorizontalMenu1);
		mAllTables.add(mHorizontalMenu2);
		mAllTables.add(mHorizontalMenu3);
	}
	
	/**
	 * Permet de redéfinir la positions de chaque élément de l'UI en prenant en compte
	 * les dimensions courantes de l'écran.
	 */
	public void createPositions() {
		mAllPositions.put(mHorizontalMenu.hashCode(), new Vector2(2514, Gdx.graphics.getHeight()-15)); 
		mAllPositions.put(mHorizontalMenu1.hashCode(), new Vector2(110, Gdx.graphics.getHeight()-11)); 
		mAllPositions.put(mHorizontalMenu2.hashCode(), new Vector2(550, Gdx.graphics.getHeight()-11)); 
		mAllPositions.put(mHorizontalMenu3.hashCode(), new Vector2(Gdx.graphics.getWidth()-283, Gdx.graphics.getHeight()-11)); 
	}
}

