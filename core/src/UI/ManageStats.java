package UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * L'interface comportant les deux onglets Manage et Statistiques situés en haut
 * du menu lateral.
 */

public class ManageStats extends CityUI {
	
	/** Tables qui composent cette lUI. Elles permettent de mettre en page les éléments de l'interface. */
	private Table mTableManageAndStats;
	
	/** Boutons qui composent cette UI. */
	private final TextButton mButtonGestion, mButtonStatistiques;

	/**
	 * Constructeur.
	 */
	public ManageStats() {

		// Création des Tables :
		mTableManageAndStats = new Table();
		
		// Bouton gestion :
		mButtonGestion = createTextButton("", "gestion");
		mButtonGestion.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
			}
        } );
		mTableManageAndStats.add(mButtonGestion).width(61).height(25); 
		
		// Bouton statistiques : 
		mButtonStatistiques = createTextButton("", "statistiques");
		mButtonStatistiques.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
			}
        } );
		mTableManageAndStats.add(mButtonStatistiques).width(61).height(25).padLeft(1); 
		
		// Ajout des Tables à la liste mAllTables :
		mAllTables.add(mTableManageAndStats);
	}
	
	/**
	 * Permet de redéfinir la positions de chaque élément de l'UI en prenant en compte
	 * les dimensions courantes de l'écran.
	 */
	public void createPositions() {
		mAllPositions.put(mTableManageAndStats.hashCode(), new Vector2(Gdx.graphics.getWidth()-74, Gdx.graphics.getHeight()-12)); 
	}
}
