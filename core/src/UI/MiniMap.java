package UI;

import CityActors.CityActor;
import CityEngine.CameraManager;
import CityEngine.CityMapRenderer;
import CityEngine.SceneGraphManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;


/**
 * Image spécifique permettant d'afficher la mini map de la cité.
 */

public class MiniMap implements InputProcessor {

	/** The managed camera. */
	private OrthographicCamera mCamera;
	
	/** Conteneur dans lequel on affiche la map. */
	private SpriteBatch mSpriteBatch;
	
	/** Largeure de la minimap en pixel. */
	private int mMiniMapWidth;
	
	/** Traceur de forme pour afficher le rectangle de vue. */
	private ShapeRenderer mShapeRenderer;
	
	/**
	 * Constructeur.
	 */
	public MiniMap() {
		mSpriteBatch = new SpriteBatch();
		mCamera = new OrthographicCamera();
		mCamera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		mMiniMapWidth = 135;
		mShapeRenderer = new ShapeRenderer();
		mShapeRenderer.setColor(1.0f, 0.0f, 0.0f, 1.0f);
	}
	
	/**
	 * Boucle de rendu.
	 * Affiche les éléments à l'écran.
	 */
	public void render() {
		// Affichage de la map :
		mSpriteBatch.setProjectionMatrix(mCamera.combined);
		mSpriteBatch.begin();
		for (Actor lActor : SceneGraphManager.gStageGrounds.getActors()) {
			mSpriteBatch.draw(((CityActor)lActor).getTextureRegion(), lActor.getX(), lActor.getY());
		}
		for (Actor lActor : SceneGraphManager.gStageBuildings.getActors()) {
			mSpriteBatch.draw(((CityActor)lActor).getTextureRegion(), lActor.getX(), lActor.getY());
		}
		for (Actor lActor : SceneGraphManager.gStageCharacters.getActors()) {
			mSpriteBatch.draw(((CityActor)lActor).getTextureRegion(), lActor.getX(), lActor.getY());
		}
		for (Actor lActor : SceneGraphManager.gStageEffects.getActors()) {
			mSpriteBatch.draw(((CityActor)lActor).getTextureRegion(), lActor.getX(), lActor.getY());
		}
		mSpriteBatch.end();

		// Traçage du rectangle :
	    mShapeRenderer.setProjectionMatrix(mCamera.combined);
		mShapeRenderer.begin(ShapeType.Line);
		float lTopLeftRectangleX = CameraManager.gLeftTopCornerScene.x;
		float lTopLeftRectangleY = CameraManager.gLeftTopCornerScene.y;
		float lBottomRightRectangleX = CameraManager.gRightBottomCornerScene.x;
		float lBottomRightRectangleY = CameraManager.gRightBottomCornerScene.y;
		if (lTopLeftRectangleX < -CityMapRenderer.gTileWidthPx/2) {
			lTopLeftRectangleX = -CityMapRenderer.gTileWidthPx/2;
		}
		if (lTopLeftRectangleX > CityMapRenderer.gMapWidthId*CityMapRenderer.gTileWidthPx) {
			lTopLeftRectangleX=CityMapRenderer.gMapWidthId*CityMapRenderer.gTileWidthPx;
		}
		if (lTopLeftRectangleY > CityMapRenderer.gMapHeightId * (CityMapRenderer.gTileHeightPx/2) + CityMapRenderer.gTileHeightPx/2) {
			lTopLeftRectangleY = CityMapRenderer.gMapHeightId * (CityMapRenderer.gTileHeightPx/2) + CityMapRenderer.gTileHeightPx/2;
		}
		if (lTopLeftRectangleY < 0) {
			lTopLeftRectangleY = 0;
		}
		if (lBottomRightRectangleX > CityMapRenderer.gMapWidthId*CityMapRenderer.gTileWidthPx) {
			lBottomRightRectangleX = CityMapRenderer.gMapWidthId*CityMapRenderer.gTileWidthPx;
		}
		if (lBottomRightRectangleY < 0) {
			lBottomRightRectangleY = 0;
		}
		float lWidthRectangle = lBottomRightRectangleX - lTopLeftRectangleX;
		float lHeightRectangle = lTopLeftRectangleY - lBottomRightRectangleY;
		if (lWidthRectangle < 0) {
			lWidthRectangle = 0;
		}
		if (lHeightRectangle < 0) {
			lHeightRectangle = 0;
		}
		mShapeRenderer.box(lTopLeftRectangleX, lTopLeftRectangleY, CameraManager.gLeftTopCornerScene.z, lWidthRectangle, -lHeightRectangle, 1);
		mShapeRenderer.end();
	}
	
	/**
	 * Gestion de la vue en cas de redimensionnement de l'écran.
	 */
	public void resize(int pScreenWidth, int pScreenHeight) {
		// Redimensionnement pour que la mini map fasse toujours mMiniMapWidth px de large.
		mCamera.viewportWidth = pScreenWidth * ((100*mMiniMapWidth)/(CityMapRenderer.gMapWidthId*CityMapRenderer.gTileWidthPx*2));
		mCamera.viewportHeight = pScreenHeight * ((100*mMiniMapWidth)/(CityMapRenderer.gMapWidthId*CityMapRenderer.gTileWidthPx*2));

		// Repositionnement de la mini map en prenant en compte le ratio :
		mCamera.position.x = -(Gdx.graphics.getWidth()/2) * ((100*mMiniMapWidth)/(CityMapRenderer.gMapWidthId*CityMapRenderer.gTileWidthPx*2)) + 920;
		mCamera.position.y = -(Gdx.graphics.getHeight()/2) * ((100*mMiniMapWidth)/(CityMapRenderer.gMapWidthId*CityMapRenderer.gTileWidthPx*2)) + 3030;
		
		// Application des modifications :
		mCamera.update();
		
	}

	/**
	 * Gestion des touches appuyées.
	 */
	public boolean keyDown(int keycode) {
		if (keycode == Keys.ENTER) {
			resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		}
		return false;
	}

	/**
	 * Gestion des touches relachées.
	 */
	public boolean keyUp(int keycode) {
		return false;
	}

	/**
	 * Si la touche est actionnée.
	 */
	public boolean keyTyped(char character) {
		return false;
	}

	/**
	 * Gestion des touches appuyées sur l'écran.
	 */
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	/**
	 * Gestion des touches relachées sur l'écran.
	 */
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	/**
	 * Gestion des drags sur l'écran.
	 */
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// Coordonnées du point touché sur l'écran, dans l'espace scène :
		Vector3 lTouchPointScene = mCamera.unproject(new Vector3(screenX, screenY, 0));
		
		// Si on clic dans le rectangle de vue :
		if (CameraManager.gLeftTopCornerScene.x < lTouchPointScene.x && lTouchPointScene.x < CameraManager.gRightBottomCornerScene.x &&
			CameraManager.gRightBottomCornerScene.y < lTouchPointScene.y && lTouchPointScene.y < CameraManager.gLeftTopCornerScene.y) {
			
			// Cela deplacera la caméra de la cité :
			CameraManager.gCamera.translate(lTouchPointScene.x - CameraManager.gCamera.position.x, lTouchPointScene.y - CameraManager.gCamera.position.y);
		}
		return false;
	}

	/**
	 * Gestion des déplacements de la souris sur l'écran.
	 */
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}
	
	/**
	 * Gestion du zoom avec la roulette de la souris.
	 */
	public boolean scrolled(int pAmount) {
		return false;
	}
	
	/**
	 * Destructeur.
	 */
	public void dispose() {
		mSpriteBatch.dispose();
	}
 }
