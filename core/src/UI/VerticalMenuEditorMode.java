package UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

/**
 * L'interface du menu lateral en mode editor.
 */

public class VerticalMenuEditorMode extends CityUI {

	/** Tables qui composent cette UI. Elles permettent de faire la mise en page. */
	private Table mTableBackGround, 
				  mTableTabs,
				  mTableTabsContents;
	
	/** Boutons qui composent cette UI. */
	private final TextButton mButtonVerticalMenuEditorMode,
						     mButtonBrush,
							 mButtonSolVide, 
    				   		 mButtonForet, 
    				   		 mButtonEau, 
    				   		 mButtonPres, 
    				   		 mButtonPoissons, 
    				   		 mButtonMines,  
    				   		 mButtonBrousailles, 
    				   		 mButtonMontagne, 
    				   		 mButtonInvasionEau, 
    				   		 mButtonInvasionSol, 
    				   		 mButtonEntreSortie, 
    				   		 mButtonAnimaux;
	
	/**
	 * Constructeur.
	 */
	public VerticalMenuEditorMode() {
		// Création des Tables :
		mTableBackGround = new Table();
		mTableTabs = new Table();
		mTableTabsContents = new Table();
		
		// Menu vertical (faux bouton) :
		mButtonVerticalMenuEditorMode = createTextButton("", "verticalMenuEditorMode");
		mTableBackGround.add(mButtonVerticalMenuEditorMode).width(186).height(3884); 
		
        // Création du groupe vertical pour les onglets :
        VerticalGroup lVerticalGroup = new VerticalGroup();
        lVerticalGroup.space(1.0f);
        
        // Création des boutons onglets :
        mButtonBrush = createTextButton("", "brush", true);
        mButtonSolVide = createTextButton("", "solVide", true);
        mButtonForet = createTextButton("", "foret", true);
        mButtonEau = createTextButton("", "eau", true);
        mButtonPres = createTextButton("", "pres", true);
        mButtonPoissons = createTextButton("", "poissons", true);
        mButtonMines = createTextButton("", "mines", true);
        mButtonBrousailles = createTextButton("", "brousailles", true);
        mButtonMontagne = createTextButton("", "montagne", true);
        mButtonInvasionEau = createTextButton("", "invasionEau", true);
        mButtonInvasionSol = createTextButton("", "invasionSol", true);
        mButtonEntreSortie = createTextButton("", "entreSortie", true);
        mButtonAnimaux = createTextButton("", "animaux", true);
		
		// Ajout des boutons au groupe vertical :
		lVerticalGroup.addActor(mButtonBrush);
		lVerticalGroup.addActor(mButtonSolVide);
		lVerticalGroup.addActor(mButtonForet);
		lVerticalGroup.addActor(mButtonEau);
		lVerticalGroup.addActor(mButtonPres);
		lVerticalGroup.addActor(mButtonPoissons);
		lVerticalGroup.addActor(mButtonMines);
		lVerticalGroup.addActor(mButtonBrousailles);
		lVerticalGroup.addActor(mButtonMontagne);
		lVerticalGroup.addActor(mButtonInvasionEau);
		lVerticalGroup.addActor(mButtonInvasionSol);
		lVerticalGroup.addActor(mButtonEntreSortie);
		lVerticalGroup.addActor(mButtonAnimaux);
		mTableTabs.add(lVerticalGroup);

        // Création du corps des onglets :
        final Table lContentBrush = new Table();
        
        final Table lContentSolVide = new Table(); 
        
        final Table lContentForet = new Table(); 
        
        final Table lContentEau = new Table(); 
        
        final Table lContentPres = new Table(); 
        
        final Table lContentPoissons = new Table(); 
        
        final Table lContentMines = new Table(); 
        
        final Table lContentBrousailles = new Table();  
        
        final Table lContentMontagne = new Table(); 
        
        final Table lContentInvasionEau = new Table(); 
        
        final Table lContentInvasionSol = new Table(); 
        
        final Table lContentEntreSortie = new Table(); 
        
        final Table lContentAnimaux = new Table(); 
        
        // Création d'une pile pour aligner tous les corps :
        Stack lStack = new Stack();
        lStack.addActor(lContentBrush);
        lStack.addActor(lContentSolVide);
        lStack.addActor(lContentForet);
        lStack.addActor(lContentEau);
        lStack.addActor(lContentPres);
        lStack.addActor(lContentPoissons);
        lStack.addActor(lContentMines);
        lStack.addActor(lContentBrousailles);
        lStack.addActor(lContentMontagne);
        lStack.addActor(lContentInvasionEau);
        lStack.addActor(lContentInvasionSol);
        lStack.addActor(lContentEntreSortie);
        lStack.addActor(lContentAnimaux);
        
        // Ajoute de la pile de corps à sa Table :
        mTableTabsContents.add(lStack).width(128).height(300);

        // Ajout d'écouteurs :
        ChangeListener lChangeListener = new ChangeListener(){
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				lContentBrush.setVisible(mButtonBrush.isChecked());
				lContentSolVide.setVisible(mButtonSolVide.isChecked());
				lContentForet.setVisible(mButtonForet.isChecked());
				lContentEau.setVisible(mButtonEau.isChecked());
				lContentPres.setVisible(mButtonPres.isChecked());
				lContentPres.setVisible(mButtonPoissons.isChecked());
				lContentPoissons.setVisible(mButtonMines.isChecked());
				lContentMines.setVisible(mButtonBrousailles.isChecked());
				lContentBrousailles.setVisible(mButtonMontagne.isChecked());
				lContentMontagne.setVisible(mButtonInvasionEau.isChecked());
				lContentInvasionSol.setVisible(mButtonInvasionSol.isChecked());
				lContentInvasionEau.setVisible(mButtonEntreSortie.isChecked());
				lContentInvasionEau.setVisible(mButtonAnimaux.isChecked());
			}
        };
        
        // Ajout de l'écouteur aux onglets :
        mButtonBrush.addListener(lChangeListener);
        mButtonSolVide.addListener(lChangeListener);
		mButtonForet.addListener(lChangeListener);
		mButtonEau.addListener(lChangeListener);
		mButtonPres.addListener(lChangeListener);
		mButtonPoissons.addListener(lChangeListener);
		mButtonMines.addListener(lChangeListener);
		mButtonBrousailles.addListener(lChangeListener);
		mButtonMontagne.addListener(lChangeListener);
		mButtonInvasionEau.addListener(lChangeListener);
		mButtonInvasionSol.addListener(lChangeListener);
		mButtonEntreSortie.addListener(lChangeListener);
		mButtonAnimaux.addListener(lChangeListener);

		// Création du groupe de boutons, pour qu'il n'y en est qu'un seul à la fois de cliqué :
        ButtonGroup lButtonGroup = new ButtonGroup();
        lButtonGroup.setMinCheckCount(1);
        lButtonGroup.setMaxCheckCount(1);
        lButtonGroup.add(mButtonBrush, mButtonSolVide, mButtonForet, mButtonEau, mButtonPres, mButtonPoissons, mButtonMines, mButtonBrousailles,
		                 mButtonMontagne, mButtonInvasionEau, mButtonInvasionSol, mButtonEntreSortie, mButtonAnimaux);
		
		// Ajout des Tables à la liste mAllTables :
		mAllTables.add(mTableBackGround);
		mAllTables.add(mTableTabs);
		mAllTables.add(mTableTabsContents);
	}
	
	/**
	 * Permet de redéfinir la positions de chaque élément de l'UI en prenant en compte
	 * les dimensions courantes de l'écran.
	 */
	public void createPositions() {
		mAllPositions.put(mTableBackGround.hashCode(),  new Vector2(Gdx.graphics.getWidth()-93, Gdx.graphics.getHeight()-1942)); 
		mAllPositions.put(mTableTabs.hashCode(), new Vector2(Gdx.graphics.getWidth()-158, Gdx.graphics.getHeight()-288)); 
		mAllPositions.put(mTableTabsContents.hashCode(), new Vector2(Gdx.graphics.getWidth()-69, Gdx.graphics.getHeight()-190));
	}
}
