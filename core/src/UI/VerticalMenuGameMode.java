package UI;

import CityActors.BuildingHouseLuxurious;
import CityActors.BuildingHouseModest;
import CityActors.BuildingTheatreSchool;
import CityActors.Ghost.eGhostChildType;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * L'interface du menu lateral en mode game.
 */

public class VerticalMenuGameMode extends CityUI {

	/** Tables qui composent cette UI. Elles permettent de mettre en page les éléments de l'interface. */
	private Table mTableBackGround, 
				  mTableNbrMsgs, 
				  mTableTabs,
				  mTableTabsContents;
	
	/** Boutons qui composent cette UI. */
	private final TextButton mButtonPopulation,
							 mButtonAdministration, 
    				   		 mButtonAgriculture, 
    				   		 mButtonArmee, 
    				   		 mButtonCulture, 
    				   		 mButtonDistribution, 
    				   		 mButtonEsthetique,  
    				   		 mButtonHygiene, 
    				   		 mButtonIndustrie, 
    				   		 mButtonVerticalMenuGameMode, 
    				   		 mButtonMythologie, 
    				   		 mButtonNbrMsg, 
    				   		 mButtonVueGenerale,
    				   		 mButtonMaisonModeste,
    				   		 mButtonMaisonLuxe,
    				   		 mButtonTheatre; 
	
	/**
	 * Constructeur.
	 */
	public VerticalMenuGameMode() {
		// Création des Tables :
		mTableBackGround = new Table();
		mTableNbrMsgs = new Table();
		mTableTabs = new Table();
		mTableTabsContents = new Table();
		
		// Menu vertical (faux bouton) :
		mButtonVerticalMenuGameMode = createTextButton("", "verticalMenuGameMode");
		mTableBackGround.add(mButtonVerticalMenuGameMode).width(186).height(3884); 
		
        // Création du groupe vertical pour les onglets :
        VerticalGroup lVerticalGroup = new VerticalGroup();
        lVerticalGroup.space(1.0f);
        
        // Création des boutons onglets :
        mButtonPopulation = createTextButton("", "population", true);
		mButtonAgriculture = createTextButton("", "agriculture", true);
		mButtonIndustrie = createTextButton("", "industrie", true);
		mButtonDistribution = createTextButton("", "distribution", true);
		mButtonHygiene = createTextButton("", "hygiene", true);
		mButtonAdministration = createTextButton("", "administration", true);
		mButtonCulture = createTextButton("", "culture", true);
		mButtonMythologie = createTextButton("", "mythologie", true);
		mButtonArmee = createTextButton("", "armee", true);
		mButtonEsthetique = createTextButton("", "esthetique", true);
		mButtonVueGenerale = createTextButton("", "vueGenerale", true);
		
		// Ajout des boutons au groupe vertical :
		lVerticalGroup.addActor(mButtonPopulation);
		lVerticalGroup.addActor(mButtonAgriculture);
		lVerticalGroup.addActor(mButtonIndustrie);
		lVerticalGroup.addActor(mButtonDistribution);
		lVerticalGroup.addActor(mButtonHygiene);
		lVerticalGroup.addActor(mButtonAdministration);
		lVerticalGroup.addActor(mButtonCulture);
		lVerticalGroup.addActor(mButtonMythologie);
		lVerticalGroup.addActor(mButtonArmee);
		lVerticalGroup.addActor(mButtonEsthetique);
		lVerticalGroup.addActor(mButtonVueGenerale);
		mTableTabs.add(lVerticalGroup);

        // Création du corps des onglets :
        final Table lPopulationContent = new Table();
        mButtonMaisonModeste = createTextButton("", "maisonModeste");
        addClickAndBuildListener(mButtonMaisonModeste, BuildingHouseModest.class, eGhostChildType.Building, 2, 2);
        lPopulationContent.add(mButtonMaisonModeste).width(58).height(42);
        mButtonMaisonLuxe = createTextButton("", "maisonLuxueuse");
        addClickAndBuildListener(mButtonMaisonLuxe, BuildingHouseLuxurious.class, eGhostChildType.Building, 4, 4);
        lPopulationContent.add(mButtonMaisonLuxe).width(58).height(42).padLeft(5);
        
        final Table lAgricultureContent = new Table(); 
        
        final Table lIndustrieContent = new Table(); 
        
        final Table lDistributionContent = new Table(); 
        
        final Table lHygieneContent = new Table(); 
        
        final Table lAdministrationContent = new Table(); 
        
        final Table lCultureContent = new Table(); 
        mButtonTheatre = createTextButton("", "theatre");
        addClickAndBuildListener(mButtonTheatre, BuildingTheatreSchool.class, eGhostChildType.Building, 3, 3);
        lCultureContent.add(mButtonTheatre).width(58).height(42);
        
        final Table lMythologiContent = new Table();  
        
        final Table lArmeeContent = new Table(); 
        
        final Table lEsthetiqueContent = new Table(); 
        
        final Table lVueGeneraleContent = new Table(); 
        
        // Création d'une pile pour aligner tous les corps :
        Stack lStack = new Stack();
        lStack.addActor(lPopulationContent);
        lStack.addActor(lAgricultureContent);
        lStack.addActor(lIndustrieContent);
        lStack.addActor(lDistributionContent);
        lStack.addActor(lHygieneContent);
        lStack.addActor(lAdministrationContent);
        lStack.addActor(lCultureContent);
        lStack.addActor(lMythologiContent);
        lStack.addActor(lArmeeContent);
        lStack.addActor(lEsthetiqueContent);
        lStack.addActor(lVueGeneraleContent);
        
        // Ajoute de la pile de corps à sa Table :
        mTableTabsContents.add(lStack).width(128).height(300);

        // Ajout d'écouteurs :
        ChangeListener lChangeListener = new ChangeListener(){
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				lPopulationContent.setVisible(mButtonPopulation.isChecked());
				lAgricultureContent.setVisible(mButtonAgriculture.isChecked());
				lIndustrieContent.setVisible(mButtonIndustrie.isChecked());
				lDistributionContent.setVisible(mButtonDistribution.isChecked());
				lHygieneContent.setVisible(mButtonHygiene.isChecked());
				lAdministrationContent.setVisible(mButtonAdministration.isChecked());
				lCultureContent.setVisible(mButtonCulture.isChecked());
				lMythologiContent.setVisible(mButtonMythologie.isChecked());
				lArmeeContent.setVisible(mButtonArmee.isChecked());
				lEsthetiqueContent.setVisible(mButtonEsthetique.isChecked());
				lVueGeneraleContent.setVisible(mButtonVueGenerale.isChecked());
			}
        };
        
        // Ajout de l'écouteur aux onglets :
        mButtonPopulation.addListener(lChangeListener);
        mButtonAgriculture.addListener(lChangeListener);
		mButtonIndustrie.addListener(lChangeListener);
		mButtonDistribution.addListener(lChangeListener);
		mButtonHygiene.addListener(lChangeListener);
		mButtonAdministration.addListener(lChangeListener);
		mButtonCulture.addListener(lChangeListener);
		mButtonMythologie.addListener(lChangeListener);
		mButtonArmee.addListener(lChangeListener);
		mButtonEsthetique.addListener(lChangeListener);
		mButtonVueGenerale.addListener(lChangeListener);

		// Création du groupe de boutons, pour qu'il n'y en est qu'un seul à la fois de cliqué :
        ButtonGroup lButtonGroup = new ButtonGroup();
        lButtonGroup.setMinCheckCount(1);
        lButtonGroup.setMaxCheckCount(1);
        lButtonGroup.add(mButtonPopulation, mButtonAgriculture, mButtonIndustrie, mButtonDistribution, mButtonHygiene, mButtonAdministration, mButtonCulture,
		                 mButtonMythologie, mButtonArmee, mButtonEsthetique, mButtonVueGenerale);
		
		// Bouton nbrMsg :
		mButtonNbrMsg = createTextButton("", "nbrMsg");
		mButtonNbrMsg.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
			}
        } );
		mTableNbrMsgs.add(mButtonNbrMsg).width(33).height(33).padTop(499).padLeft(134);
		
		// Ajout des Tables à la liste mAllTables :
		mAllTables.add(mTableBackGround);
		mAllTables.add(mTableNbrMsgs);
		mAllTables.add(mTableTabs);
		mAllTables.add(mTableTabsContents);
	}
	
	/**
	 * Permet de redéfinir la positions de chaque élément de l'UI en prenant en compte
	 * les dimensions courantes de l'écran.
	 */
	public void createPositions() {
		mAllPositions.put(mTableBackGround.hashCode(),  new Vector2(Gdx.graphics.getWidth()-93, Gdx.graphics.getHeight()-1942)); 
		mAllPositions.put(mTableTabs.hashCode(), new Vector2(Gdx.graphics.getWidth()-158, Gdx.graphics.getHeight()-247)); 
		mAllPositions.put(mTableTabsContents.hashCode(), new Vector2(Gdx.graphics.getWidth()-69, Gdx.graphics.getHeight()-190));
		mAllPositions.put(mTableNbrMsgs.hashCode(), new Vector2(Gdx.graphics.getWidth()-91, Gdx.graphics.getHeight()-247));  
	}
}
