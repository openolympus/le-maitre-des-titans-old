package leMaitreDesTitans.desktop;

import Application.MainApplication;
import ApplicationConfig.MainApplicationConfig;

import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {
	public static void main (String[] arg) {
		// Creation de l'application :
		MainApplication mainApplication = new MainApplication();
		
		// Définition de la configuration de l'application:
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = MainApplicationConfig.gApplicationName;
		config.width = MainApplicationConfig.gScreenWidth;
		config.height = MainApplicationConfig.gScreenHeight;
		config.addIcon(MainApplicationConfig.gFaviconPath, FileType.Internal);
		config.fullscreen = MainApplicationConfig.gIsFullScreen;
		config.vSyncEnabled = MainApplicationConfig.gIsUsedVSync;
		
		// Lancement de l'application avec la configuration :
		new LwjglApplication(mainApplication, config);
	}
}
